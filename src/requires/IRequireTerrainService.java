package requires;

import services.ITerrainJeuxService;

public interface IRequireTerrainService {
	void bind(ITerrainJeuxService terrain);
}
