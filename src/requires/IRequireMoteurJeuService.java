package requires;

import services.IMoteurJeuService;

public interface IRequireMoteurJeuService {
	void bind(IMoteurJeuService moteurJeu);
}
