package requires;

import java.util.List;

import services.IRouteService;

public interface IRequireRouteService {
	public void bind_route(List<IRouteService> route);
}
