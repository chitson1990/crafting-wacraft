package implementation;

import java.awt.Point;

import enums.OBJECTS;
import enums.RACE;
import services.IMineService;

public class Mine implements IMineService {

	private int largeur;
	private int hauteur;
	private int orRestant;
	private int abandonComtpeur;
	private Point position;
	private RACE etatAppaternance;
	
	@Override
	public int largeur() {
		return largeur;
	}

	@Override
	public int hauteur() {
		return hauteur;
	}

	@Override
	public int orRestant() {
		return this.orRestant;
	}


	@Override
	public boolean estLaminee() {
		return orRestant<=0;
	}

	@Override
	public int abandonCompteur() {
		return abandonComtpeur;
	}

	@Override
	public void init(int largeur, int hauteur, Point position, RACE race) {
		this.largeur= largeur;
		this.hauteur= hauteur;
		abandonComtpeur =51;
		orRestant =51;
		this.position= position;
		etatAppaternance = race;
	}

	@Override
	public void retrait(int s) {
		orRestant-=s;
	}

	@Override
	public void acceuil() {
		abandonComtpeur=0;
	}

	@Override
	public void abandoned() {
		abandonComtpeur++;
	}

	@Override
	public boolean estAbandonnee() {
		return abandonComtpeur==51;
	}

	@Override
	public boolean peutAcceuil() {
		return abandonComtpeur==0;
	}

	@Override
	public OBJECTS myType() {
		return OBJECTS.MINE;
	}
	@Override
	public int getX() {
		return position.x;
	}

	@Override
	public int getY() {
		return position.y;
	}

	@Override
	public void setX(int x) {
		position.x = x;
	}

	@Override
	public void setY(int y) {
		position.y = y;
	}
	
	@Override
	public RACE appartientA() {
		return etatAppaternance;
	}
}
