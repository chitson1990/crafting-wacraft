package implementation;

import java.awt.Point;

import enums.OBJECTS;
import enums.RACE;
import services.IVillageoisService;

public class Villageois implements IVillageoisService {
	private RACE race;
	private int largeur;
	private int pointDeVie;
	private double vitesse;
	private int force;
	private int hauteur;
	private int quantiteOr;
	Point position;
	private int compteurPasJeuRestant;

	@Override
	public void init(String race, int largeur, int hauteur, int force,
			double vitesse, int pointsVie, Point position) {
		this.race = RACE.valueOf(race);
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.force = force;
		this.vitesse = vitesse;
		this.pointDeVie = pointsVie;
		quantiteOr = 0;
		this.position = position;
		compteurPasJeuRestant = -1;
	}

	@Override
	public void retrait(int s) {
		pointDeVie -= s;
	}

	@Override
	public RACE race() {
		return race;
	}

	@Override
	public int largeur() {
		return largeur;
	}

	@Override
	public int hauteur() {
		return hauteur;
	}

	@Override
	public int force() {
		return force;
	}

	@Override
	public double vitesse() {
		return vitesse;
	}

	@Override
	public int pointDeVie() {
		return pointDeVie;
	}

	@Override
	public int quantiteOr() {
		return quantiteOr;
	}

	@Override
	public boolean estMort() {
		return pointDeVie <= 0;
	}

	@Override
	public OBJECTS myType() {
		return OBJECTS.VILLAGEOIS;
	}

	@Override
	public int getX() {
		return position.x;
	}

	@Override
	public int getY() {
		return position.y;
	}

	@Override
	public void setX(int x) {
		position.x = x;
	}

	@Override
	public void setY(int y) {
		position.y = y;
	}

	@Override
	public int resteDansLaMinePourPasDeJeu() {
		return compteurPasJeuRestant;
	}

	@Override
	public boolean estEnTrainDeMiner() {
		return resteDansLaMinePourPasDeJeu() > 0;
	}

	@Override
	public void entrerMine() {
		compteurPasJeuRestant = 16;
	}

	@Override
	public void miner() {
		compteurPasJeuRestant--;
	}

	@Override
	public void sortirMine() {
		compteurPasJeuRestant = 0;
	}

	@Override
	public void remplirPoches(int i) {
		quantiteOr+=i;
	}

	@Override
	public void viderPoches() {
		quantiteOr=0;
	}
}
