package implementation;

import java.awt.Point;

import enums.OBJECTS;
import services.IMurrailleService;

public class Muraille implements IMurrailleService {

	int hauteur;
	int largeur;
	Point position;
	int pointVie;

	@Override
	public int hauteur() {
		return hauteur;
	}

	@Override
	public int largeur() {
		return largeur;
	}

	@Override
	public boolean passAt(int x, int y) {

		for (int i = 0; i < largeur; i++) {
			if (x == (getX() + i)) {
				for (int j = 0; j < hauteur; j++) {
					if (y == (getY() + j)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void init(Point position, int largeur, int hauteur, int pointVie) {
		this.position = position;
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.pointVie = pointVie;
	}

	@Override
	public OBJECTS myType() {
		return OBJECTS.MURAILLE;
	}

	@Override
	public int getX() {
		return position.x;
	}

	@Override
	public int getY() {
		return position.y;
	}

	@Override
	public void setX(int x) {
		position.x = x;
	}

	@Override
	public void setY(int y) {
		position.y = y;
	}

	@Override
	public int pointDeVie() {
		return this.pointVie;
	}

	@Override
	public boolean estDetruit() {
		return pointVie <= 0;
	}

	@Override
	public void retrait(int s) {
		pointVie -= s;
	}

}
