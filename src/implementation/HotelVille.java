package implementation;

import java.awt.Point;

import enums.OBJECTS;
import enums.RACE;
import services.IHotelVilleService;

public class HotelVille implements IHotelVilleService {
	private int largeur;
	private int hauteur;
	private int quantiteDor;
	private int abandonComtpeur;
	private boolean estAbandonnee;
	private Point position;
	private RACE etatAppaternance;
	
	
	@Override
	public int largeur() {
		return largeur;
	}

	@Override
	public int hauteur() {
		return hauteur;
	}

	@Override
	public int quantityDor() {
		return this.quantiteDor;
	}

	@Override
	public boolean estAbandonnee() {
		return estAbandonnee;
	}

	@Override
	public boolean estLaminee() {
		return quantiteDor<=0;
	}

	@Override
	public int abandonCompteur() {
		return abandonComtpeur;
	}

	@Override
	public void init(int largeur, int hauteur, Point position, RACE race) {
		this.largeur = largeur;
		this.hauteur=hauteur;
		this.quantiteDor = 16;
		estAbandonnee=true;
		this.position = position;
		etatAppaternance = race;
	}

	@Override
	public void depot(int s) {
		quantiteDor+=s;
		estAbandonnee=false;
	}

	@Override
	public OBJECTS myType() {
		return OBJECTS.HOTELVILLE;
	}
	@Override
	public int getX() {
		return position.x;
	}

	@Override
	public int getY() {
		return position.y;
	}

	@Override
	public void setX(int x) {
		position.x = x;
	}

	@Override
	public void setY(int y) {
		position.y = y;
	}

	@Override
	public RACE appartientA() {
		return etatAppaternance;
	}

}
