package implementation;

import java.awt.Point;

import enums.OBJECTS;
import services.IRouteService;

public class Route implements IRouteService {

	int hauteur;
	int largeur;
	Point position;

	@Override
	public int hauteur() {
		return hauteur;
	}

	@Override
	public int largeur() {
		return largeur;
	}

	@Override
	public boolean passAt(int x, int y) {

		for (int i = 0; i < largeur; i++) {
			if (x == (getX() + i)) {
				for (int j = 0; j < hauteur; j++) {
					if (y == (getY() + j)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void init(Point position, int largeur, int hauteur) {
		this.position = position;
		this.largeur = largeur;
		this.hauteur = hauteur;
	}

	@Override
	public OBJECTS myType() {
		return OBJECTS.ROUTE;
	}

	@Override
	public int getX() {
		return position.x;
	}

	@Override
	public int getY() {
		return position.y;
	}

	@Override
	public void setX(int x) {
		position.x=x;
	}

	@Override
	public void setY(int y) {
		position.y=y;
	}

}
