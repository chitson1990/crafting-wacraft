package implementation;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import enums.OBJECTS;
import services.IHotelVilleService;
import services.IMineService;
import services.IMurrailleService;
import services.IMyObjectService;
import services.IRouteService;
import services.ITerrainJeuxService;

public class Terrain implements ITerrainJeuxService {
	private int largeur;
	private int hauteur;
	private IHotelVilleService[] hotels;
	private List<IMineService>mines;
	private List<IMurrailleService> murailles;
	private List<IRouteService> routes;
	private OBJECTS[][] matrices;
	private int nbMurailles=0;
	Set<Integer> numeroMurailles;
	Set<Integer> numeroMines;
	
	@Override
	public void bind_hotel(IHotelVilleService[] hotel) {
		hotels = hotel;
		setHotelPositionInTheMatrices();
	}

	// TODO
	private void setHotelPositionInTheMatrices() {
		for (IHotelVilleService _hotel : this.hotels) {
			setObjectAt(_hotel.getX(), _hotel.getY(), _hotel);
		}
	}

	@Override
	public void bind_mine(List<IMineService> mine) {
		mines = mine;
		for(int i=0; i < mine.size(); i++) {
			numeroMines.add(i);
		}
		setMinesPositionInTheMatrices();
	}

	// TODO
	private void setMinesPositionInTheMatrices() {
		for (IMineService _mine : this.mines) {
			setObjectAt(_mine.getX(), _mine.getY(), _mine);
		}
	}

	@Override
	public void bind_murraile(List<IMurrailleService> muraille) {
		murailles = muraille;
		nbMurailles=muraille.size();
		for(int i=0; i< nbMurailles; i++) {
			numeroMurailles.add(i);
		}
		setMuraillesPositionInTheMatrices();
	}

	private void setMuraillesPositionInTheMatrices() {
		for (IMurrailleService _muraille:this.murailles) {
			setObjectAt(_muraille.getX(), _muraille.getY(), _muraille);
		}
	}

	@Override
	public void bind_route(List<IRouteService> route) {
		this.routes = route;
		setRoutesPositionInTheMatrices();
	}

	private void setRoutesPositionInTheMatrices() {
		for (IRouteService _route : this.routes) {
			setObjectAt(_route.getX(), _route.getY(), _route);
		}
	}

	@Override
	public int largeur() {
		return largeur;
	}

	@Override
	public int hauteur() {
		return hauteur;
	}

	@Override
	public void init(int largeur, int hauteur) {
		numeroMurailles =new HashSet<Integer>();
		numeroMines = new HashSet<Integer>();
		this.largeur = largeur;
		this.hauteur = hauteur;
		matrices = new OBJECTS[largeur][hauteur];
		for (int i = 0; i < largeur; i++) {
			for (int j = 0; j < hauteur; j++) {
				matrices[i][j] = OBJECTS.RIEN;
			}
		}
		// TODO more here
	}

	@Override
	public OBJECTS getObjectAt(int x, int y) {
		return matrices[x][y];
	}

	@Override
	public void setObjectAt(int x, int y, IMyObjectService object) {
		switch (getObjectType(object)) {
		case RIEN:
			matrices[x][y] = OBJECTS.RIEN;
			break;
		default:
			fillMeInTheMatrices(object);
		}
	}

	private void fillMeInTheMatrices(IMyObjectService object) {
		int x = object.getX();
		int y = object.getY();
		for (int i = 0; i < object.largeur(); i++) {
			for (int j = 0; j < object.hauteur(); j++) {
				if (getObjectAt(x + i, y + j) == OBJECTS.RIEN) {
					matrices[x + i][y + j] = object.myType();
				}
			}
		}
	}
	
	public static OBJECTS getObjectType(IMyObjectService object) {
		if (object == null) {
			return OBJECTS.RIEN;
		} else {
			return object.myType();
		}
	}

	@Override
	public void setObjectAt(int x, int y, OBJECTS type) {
		matrices[x][y]=type;
	}

	@Override
	public IMurrailleService getMuraille(int id) {
		return murailles.get(id);
	}

	@Override
	public void detruireMuraille(int id) {
		IMurrailleService _muraille = murailles.get(id);
		murailles.remove(id);
		numeroMurailles.remove(id);
	    removeMuralsFromMatrices(_muraille);
	}
	
	private void removeMuralsFromMatrices(IMurrailleService murals) {
		int height = murals.hauteur();
		int width = murals.largeur();
		int x = murals.getX();
		int y = murals.getY();
		
		for(int i=0; i < width; i++) {
			for (int j=0; j < height ; j++) {
				setObjectAt(x+i, y+j, OBJECTS.RIEN);
			}
		}
	}

	@Override
	public int nbMurailles() {
		return numeroMurailles.size();
	}

	@Override
	public IMineService getMine(int numero) {
		return mines.get(numero);
	}

	@Override
	public Set<Integer> numerosMurailles() {
		return numeroMurailles;
	}

	@Override
	public Set<Integer> numerosMines() {
		return numeroMines;
	}
}
