package implementation;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import enums.COMMAND;
import enums.OBJECTS;
import enums.RESULTAT;
import services.IHotelVilleService;
import services.IMineService;
import services.IMoteurJeuService;
import services.IMurrailleService;
import services.ITerrainJeuxService;
import services.IVillageoisService;

public class MoteurJeu implements IMoteurJeuService {

	private int maxPasJeu;
	private int pasJeuCourant;
	private int compteurVillageois;
	private Map<Integer, IVillageoisService> villageois;
	private IHotelVilleService hotelVille;
	private ITerrainJeuxService terrain;

	@Override
	public void bind(ITerrainJeuxService terrain) {
		this.terrain = terrain;
	}

	@Override
	public void bind(IVillageoisService villageois) {

		if(terrain == null)
			throw new Error("Vous devez binder le terrain avant de binder un Villageois");

		if(this.villageois == null)
			this.villageois = new HashMap<Integer, IVillageoisService>();

		this.villageois.put(compteurVillageois, villageois);

		int xVillageois = positionVillageoisX(compteurVillageois);
		int yVillageois = positionVillageoisY(compteurVillageois);

		for(int i = xVillageois; i < xVillageois + villageois.largeur(); i ++) {
			for(int j = yVillageois; j < yVillageois + villageois.hauteur(); j++) {
				OBJECTS previousObject = terrain.getObjectAt(i, j);

				if(previousObject == OBJECTS.ROUTE)
					terrain.setObjectAt(i, j, OBJECTS.VILLAGEOISSURROUTE);
				else terrain.setObjectAt(i, j, OBJECTS.VILLAGEOIS);
			}
		}
		compteurVillageois++;
	}

	@Override
	public void bind_hotel(IHotelVilleService[] hotel) {
		hotelVille = hotel[0];
	}

	@Override
	public int largeurTerrain() {
		return terrain.largeur();
	}

	@Override
	public int hauteurTerrain() {
		return terrain.hauteur();
	}

	@Override
	public int maxPasJeu() {
		return maxPasJeu;
	}

	@Override
	public int pasJeuCourant() {
		return pasJeuCourant;
	}

	@Override
	public boolean estFini() {
		return pasJeuCourant == maxPasJeu;
	}

	@Override
	public RESULTAT resultatFinal() {
		if(hotelVille.quantityDor() >= 1664)
			return RESULTAT.GAGNER;
		return RESULTAT.PERDU;
	}

	@Override
	public ITerrainJeuxService terrain() {
		return terrain;
	}

	@Override
	public Set<Integer> numerosVillageois() {
		return villageois.keySet();
	}

	@Override
	public IVillageoisService getVillageois(int numero) {
		return villageois.get(numero);
	}

	@Override
	public int positionVillageoisX(int numero) {
		return villageois.get(numero).getX();
	}

	@Override
	public int positionVillageoisY(int numero) {
		return villageois.get(numero).getY();
	}

	@Override
	public Set<Integer> numerosMines() {
		return terrain.numerosMines();
	}

	@Override
	public IMineService getMine(int numero) {
		return terrain.getMine(numero);
	}

	@Override
	public int positionMineX(int numero) {
		return terrain.getMine(numero).getX();
	}

	@Override
	public int positionMineY(int numero) {
		return terrain.getMine(numero).getY();
	}

	@Override
	public Set<Integer> numerosMurailles() {
		return terrain.numerosMurailles();
	}

	@Override
	public IMurrailleService getMuraille(int numeroMuraille) {
		return terrain.getMuraille(numeroMuraille);
	}

	@Override
	public int positionMurailleX(int numeroMuraille) {
		return terrain.getMuraille(numeroMuraille).getX();
	}

	@Override
	public int positionMurailleY(int numeroMuraille) {
		return terrain.getMuraille(numeroMuraille).getY();
	}

	@Override
	public IHotelVilleService hotelDeVille() {
		return hotelVille;
	}

	@Override
	public int positionHotelVilleX() {
		return hotelVille.getX();
	}

	@Override
	public int positionHotelVilleY() {
		return hotelVille.getY();
	}

	@Override
	public boolean peutEntrerMine(int numeroVillageois, int numeroMine) {
		IMineService mine = getMine(numeroMine);
		IVillageoisService villageois = getVillageois(numeroVillageois);

		if(mine.estAbandonnee() || mine.appartientA() == villageois.race()) {
			int xVillageois = positionVillageoisX(numeroVillageois);
			int yVillageois = positionVillageoisY(numeroVillageois);
			int xMine = positionMineX(numeroMine) - 51;
			int yMine = positionMineY(numeroMine) - 51;

			if((xVillageois >= xMine + mine.largeur() + 102)
					|| (xVillageois + villageois.largeur() <= xMine)
					|| (yVillageois >= yMine + mine.hauteur() + 102)
					|| (yVillageois + villageois.hauteur() <= yMine))
				return false;

			return true;
		}
		else return false;
	}

	@Override
	public boolean peutEntrerHotelVille(int numeroVillageois) {
		IHotelVilleService hotelVille = hotelDeVille();
		IVillageoisService villageois = getVillageois(numeroVillageois);

		if(hotelVille.estAbandonnee() || hotelVille.appartientA() == villageois.race()) {
			int xVillageois = positionVillageoisX(numeroVillageois);
			int yVillageois = positionVillageoisY(numeroVillageois);
			int xHotelVille = positionHotelVilleX() - 51;
			int yHotelVille = positionHotelVilleY() - 51;

			if((xVillageois >= xHotelVille + hotelVille.largeur() + 102)
					|| (xVillageois + villageois.largeur() <= xHotelVille)
					|| (yVillageois >= yHotelVille + hotelVille.hauteur() + 102)
					|| (yVillageois + villageois.hauteur() <= yHotelVille))
				return false;

			return true;
		}
		else return false;
	}

	@Override
	public boolean peutAttaquerMuraille(int numeroVillageois, int numeroMuraille) {
		int xVillageois = positionVillageoisX(numeroVillageois);
		int yVillageois = positionVillageoisY(numeroVillageois);
		int xMuraille = positionMurailleX(numeroMuraille) - 51;
		int yMuraille = positionMurailleY(numeroMuraille) - 51;
		IVillageoisService villageois = getVillageois(numeroVillageois);
		IMurrailleService muraille = getMuraille(numeroMuraille);

		if((xVillageois >= xMuraille + muraille.largeur() + 102)
				|| (xVillageois + villageois.largeur() <= xMuraille)
				|| (yVillageois >= yMuraille + muraille.hauteur() + 102)
				|| (yVillageois + villageois.hauteur() <= yMuraille))
			return false;

		return true;
	}

	@Override
	public void init(int maxPas) {
		this.maxPasJeu = maxPas;
		pasJeuCourant = 0;
		compteurVillageois = 0;

		if(terrain == null)
			throw new Error("Vous devez binder le terrain avant l'appel à init");

		if(villageois == null)
			throw new Error("Vous devez binder au moins un Villageois avant l'appel à init");
	}

	@Override
	public void pasJeu(COMMAND commande, int numVillageois, int argument) {
		IVillageoisService villageois = getVillageois(numVillageois);

		// 0: on incrémente le pas de jeu courant
		pasJeuCourant += 1;

		// 1: on traite la commande de l'utilisateur
		switch(commande) {
		case DEPLACER:
			int angle = argument;

			// déplacement droite
			if((0 <= angle && angle < 45) || (315 < angle && angle <= 360)) {
				if(estSurRoute(numVillageois)) {
					if(estAtteignable(numVillageois, angle, 2, 0)) {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, 2, 0);
						villageois.setX(villageois.getX() + 2);
					}
					else {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, 1, 0);
						villageois.setX(villageois.getX() + 1);
					}
				}
				else {
					setCasesDepart(numVillageois);
					setCasesArrivee(numVillageois, 1, 0);
					villageois.setX(villageois.getX() + 1);
				}
			}
			// déplacement diagonale haut droite
			else if(angle == 45) {
				if(estSurRoute(numVillageois)) {
					if(estAtteignable(numVillageois, angle, 2, -2)) {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, 2, -2);
						villageois.setX(villageois.getX() + 2);
						villageois.setY(villageois.getY() - 2);
					}
					else {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, 1, -1);
						villageois.setX(villageois.getX() + 1);
						villageois.setY(villageois.getY() - 1);
					}
				}
				else {
					setCasesDepart(numVillageois);
					setCasesArrivee(numVillageois, 1, -1);
					villageois.setX(villageois.getX() + 1);
					villageois.setY(villageois.getY() - 1);
				}

			}
			// déplacement haut
			else if(45 < angle && angle < 135) {
				if(estSurRoute(numVillageois)) {
					if(estAtteignable(numVillageois, angle, 0, -2)) {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, 0, -2);
						villageois.setY(villageois.getY() - 2);
					}
					else {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, 0, -1);
						villageois.setY(villageois.getY() - 1);
					}
				}
				else {
					setCasesDepart(numVillageois);
					setCasesArrivee(numVillageois, 0, -1);
					villageois.setY(villageois.getY() - 1);
				}

			}
			// déplacement diagonale haut gauche
			else if(angle == 135) {
				if(estSurRoute(numVillageois)) {
					if(estAtteignable(numVillageois, angle, -2, -2)) {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, -2, -2);
						villageois.setX(villageois.getX() - 2);
						villageois.setY(villageois.getY() - 2);
					}
					else {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, -1, -1);
						villageois.setX(villageois.getX() - 1);
						villageois.setY(villageois.getY() - 1);
					}
				}
				else {
					setCasesDepart(numVillageois);
					setCasesArrivee(numVillageois, -1, -1);
					villageois.setX(villageois.getX() - 1);
					villageois.setY(villageois.getY() - 1);
				}

			}
			// déplacement gauche
			else if(135 < angle && angle < 225) {
				if(estSurRoute(numVillageois)) {
					if(estAtteignable(numVillageois, angle, -2, 0)) {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, -2, 0);
						villageois.setX(villageois.getX() - 2);
					}
					else {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, -1, 0);
						villageois.setX(villageois.getX() - 1);
					}
				}
				else {
					setCasesDepart(numVillageois);
					setCasesArrivee(numVillageois, -1, 0);
					villageois.setX(villageois.getX() - 1);
				}

			}
			// déplacement diagonale bas gauche
			else if(angle == 225) {
				if(estSurRoute(numVillageois)) {
					if(estAtteignable(numVillageois, angle, -2, 2)) {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, -2, 2);
						villageois.setX(villageois.getX() - 2);
						villageois.setY(villageois.getY() + 2);
					}
					else {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, -1, 1);
						villageois.setX(villageois.getX() - 1);
						villageois.setY(villageois.getY() + 1);
					}
				}
				else {
					setCasesDepart(numVillageois);
					setCasesArrivee(numVillageois, -1, 1);
					villageois.setX(villageois.getX() - 1);
					villageois.setY(villageois.getY() + 1);
				}

			}
			// déplacement bas
			else if(225 < angle && angle < 315) {
				if(estSurRoute(numVillageois)) {
					if(estAtteignable(numVillageois, angle, 0, 2)) {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, 0, 2);
						villageois.setY(villageois.getY() + 2);
					}
					else {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, 0, 1);
						villageois.setY(villageois.getY() + 1);
					}
				}
				else {
					setCasesDepart(numVillageois);
					setCasesArrivee(numVillageois, 0, 1);
					villageois.setY(villageois.getY() + 1);
				}

			}
			// déplacement diagonale bas droite
			else if(angle == 315) {
				if(estSurRoute(numVillageois)) {
					if(estAtteignable(numVillageois, angle, 2, 2)) {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, 2, 2);
						villageois.setX(villageois.getX() + 2);
						villageois.setY(villageois.getY() + 2);
					}
					else {
						setCasesDepart(numVillageois);
						setCasesArrivee(numVillageois, 1, 1);
						villageois.setX(villageois.getX() + 1);
						villageois.setY(villageois.getY() + 1);
					}
				}
				else {
					setCasesDepart(numVillageois);
					setCasesArrivee(numVillageois, 1, 1);
					villageois.setX(villageois.getX() + 1);
					villageois.setY(villageois.getY() + 1);
				}

			}
			break;

		case ENTRERHOTELVILLE:
			hotelDeVille().depot(villageois.quantiteOr());
			villageois.viderPoches();
			break;

		case ENTRERMINE:
			int numeroMine = argument;
			villageois.remplirPoches(1);
			villageois.entrerMine();

			IMineService mine = getMine(numeroMine);
			mine.retrait(1);
			mine.acceuil();
			break;

		case ATTAQUERMURAILLE:
			int numeroMuraille = argument;

			IMurrailleService muraille = getMuraille(numeroMuraille);
			muraille.retrait(villageois.force());
			if(muraille.estDetruit())
				terrain.detruireMuraille(numeroMuraille);

			break;

		case RIEN:
			// pasJeu vide
			break;

		default:
			// on ne devrait jamais arriver là
			break;
		}

		// 2: traitements

		// on s'occupe des Mines
		if(commande == COMMAND.ENTRERMINE) {
			for(int numeroMine : numerosMines())
				if(numeroMine != argument)
					getMine(numeroMine).abandoned();
		}
		else {
			for(int numeroMine : numerosMines())
				if(!getMine(numeroMine).estAbandonnee())
					getMine(numeroMine).abandoned();
		}

		// on s'occupe des Villageois en train de miner
		for(int i : numerosVillageois()) {
			IVillageoisService miner = getVillageois(i);

			if(miner.estEnTrainDeMiner())
				miner.miner();
		}

	}

	/**
	 * Renvoit true si le Villageois associé au numéro passé en paramètre est sur une Route et sinon false.
	 * 
	 * @param numVillageois
	 * @return
	 */
	private boolean estSurRoute(int numeroVillageois) {
		for(int i = positionVillageoisX(numeroVillageois); i < getVillageois(numeroVillageois).largeur(); i++) {
			for(int j = positionVillageoisY(numeroVillageois); j < getVillageois(numeroVillageois).hauteur(); j++) {
				if(terrain().getObjectAt(i, j) == OBJECTS.VILLAGEOISSURROUTE)
					return true;
			}
		}

		return false;	}

	private boolean estAtteignable(int numeroVillageois, int angle, int xOption, int yOption) {
		int currentX = positionVillageoisX(numeroVillageois);
		int currentY = positionVillageoisY(numeroVillageois);
		int nextX = positionVillageoisX(numeroVillageois) + xOption;
		int nextY = positionVillageoisY(numeroVillageois) + yOption;
		int largeur = getVillageois(numeroVillageois).largeur();
		int hauteur = getVillageois(numeroVillageois).hauteur();

		// déplacement droite
		if((0 <= angle && angle < 45) || (315 < angle && angle <= 360)) {
			for(int i = currentY; i < currentY + hauteur; i ++) {
				OBJECTS object = terrain().getObjectAt(nextX + largeur, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement diagonale haut droite
		else if(angle == 45) {
			for(int i = nextX; i < nextX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}

			for(int i = nextY; i < nextY + hauteur; i ++) {
				OBJECTS object = terrain().getObjectAt(nextX + largeur, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement haut
		else if(45 < angle && angle < 135) {
			for(int i = currentX; i < currentX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement diagonale haut gauche
		else if(angle == 135) {
			for(int i = nextX; i < nextX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}

			for(int i = nextY; i < nextY + hauteur; i++) {
				OBJECTS object = terrain().getObjectAt(nextX, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement gauche
		else if(135 < angle && angle < 225) {
			for(int i = currentY; i < currentY + hauteur; i++) {
				OBJECTS object = terrain().getObjectAt(nextX, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement diagonale bas gauche
		else if(angle == 225) {
			for(int i = nextY; i < nextY + hauteur; i++) {
				OBJECTS object = terrain().getObjectAt(nextX, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}

			for(int i = nextX; i < nextX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY + hauteur);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement bas
		else if(225 < angle && angle < 315) {
			for(int i = currentX; i < currentX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement diagonale bas droite
		else if(angle == 315) {
			for(int i = nextX; i < nextX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY + hauteur);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}

			for(int i = nextY; i < nextY + hauteur; i++) {
				OBJECTS object = terrain().getObjectAt(nextX + largeur, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}

		return true;
	}

	/**
	 * Modifie la case située aux coordonnées (positionVillageoisX(numeroVillageois), positionVillageoisY(numeroVillageois)) après le déplacement du Villageois associé au numéro passé en paramètre.<br>
	 * Si la case était à OBJECTS.VILLAGEOISSURROUTE, elle devient OBJECTS.ROUTE et OBJECTS.RIEN sinon.
	 * 
	 * @param numeroVillageois
	 * @param xOption
	 * @param yOption
	 */
	private void setCasesDepart(int numeroVillageois) {
		int currentX = positionVillageoisX(numeroVillageois);
		int currentY = positionVillageoisY(numeroVillageois);

		for(int i = currentX; i < currentX + getVillageois(numeroVillageois).largeur(); i++) {
			for(int j = currentY; j < currentY + getVillageois(numeroVillageois).hauteur(); j++) {

				OBJECTS object = terrain.getObjectAt(i, j);

				if(object == OBJECTS.VILLAGEOISSURROUTE) {
					terrain.setObjectAt(i, j, OBJECTS.ROUTE);
				}
				else if (object == OBJECTS.VILLAGEOIS){
					terrain.setObjectAt(i, j, OBJECTS.RIEN);
				}
			}
		}
	}

	/**
	 * Modifie la case située aux coordonnées (positionVillageoisX(numeroVillageois) + xOption, positionVillageoisY(numeroVillageois) + yOption) après le déplacement du Villageois associé au numéro passé en paramètre.<br>
	 * Si la case était à OBJECTS.ROUTE, elle devient OBJECTS.VILLAGEOISSURROUTE et OBJECTS.VILLAGEOIS sinon.
	 * 
	 * @param numeroVillageois
	 * @param xOption
	 * @param yOption
	 */
	private void setCasesArrivee(int numeroVillageois, int xOption, int yOption) {
		int nextX = positionVillageoisX(numeroVillageois) + xOption;
		int nextY = positionVillageoisY(numeroVillageois) + yOption;

		for(int i = nextX; i < nextX + getVillageois(numeroVillageois).largeur(); i++) {
			for(int j = nextY; j < nextY + getVillageois(numeroVillageois).hauteur(); j++) {

				OBJECTS object = terrain.getObjectAt(i, j);

				if(object == OBJECTS.ROUTE) {
					terrain.setObjectAt(i, j, OBJECTS.VILLAGEOISSURROUTE);
				}
				else if (object == OBJECTS.RIEN){
					terrain.setObjectAt(i, j, OBJECTS.VILLAGEOIS);
				}
			}
		}
	}

}
