package tests.abstracts;

import static org.junit.Assert.assertEquals;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import services.IMurrailleService;

public abstract class AbstractMurailleTests {

	protected IMurrailleService muraille;

	@Before
	public abstract void before();

	@After
	public void after() {
		muraille = null;
	}

	@Test
	public void test_init() {
		int x = 2;
		int y = 3;

		int largeur = 5;
		int hauteur = 3;
		// Condition initiale
		// Aucune

		// Operations init
		// LO = init(POint::init(x,y),largeur,hauteur)
		muraille.init(new Point(x, y), largeur, hauteur, 8);

		// ORACLE
		// getX(LO)=x
		// getY(LO)=y
		// largeur(LO)=largeur
		// hauteur(LO)=hauteur

		// RAPPORT

		assertEquals(" Test life after init: Expected" + 8 + " found "
				+ muraille.pointDeVie(), 8, muraille.pointDeVie());
		assertEquals(
				"Tests y position: expected " + y + "  : found"
						+ muraille.getY(), y, muraille.getY());
		assertEquals(
				"Tests x position: expected " + x + "  : found"
						+ muraille.getX(), x, muraille.getX());

		assertEquals("Tests largeur: expected " + largeur + "  : found"
				+ muraille.largeur(), largeur, muraille.largeur());
		assertEquals("Tests largeur: expected " + hauteur + "  : found"
				+ muraille.hauteur(), hauteur, muraille.hauteur());
	}

	@Test
	public void testSetX() {
		int x = 2;
		int y = 3;

		int largeur = 5;
		int hauteur = 3;
		// Condition initiale
		// LO = LO = init(POint::init(x,y),largeur,hauteur)
		muraille.init(new Point(x, y), largeur, hauteur, 5);

		int x_expected = 6;
		// Operations
		// L1 = setX(LO, x_expected)

		muraille.setX(x_expected);

		// ORACLE
		// L1.getX() = x_expected

		// RAPPORT
		assertEquals("Tests x position: expected " + x_expected + "  : found"
				+ muraille.getX(), x_expected, muraille.getX());
	}

	@Test
	public void testSetY() {
		int x = 2;
		int y = 3;

		int largeur = 5;
		int hauteur = 3;
		// Condition initiale
		// LO = LO = init(POint::init(x,y),largeur,hauteur)
		muraille.init(new Point(x, y), largeur, hauteur, 2);

		int y_expected = 6;
		// Operations
		// L1 = setY(LO, y_expected)

		muraille.setY(y_expected);
		// ORACLE
		// L1.getY() = y_expected

		// RAPPORT
		assertEquals("Tests y position: expected " + y_expected + "  : found"
				+ muraille.getY(), y_expected, muraille.getY());
	}
	
	@Test
	public void test_retrait() {
		int x = 2;
		int y = 3;

		int largeur = 5;
		int hauteur = 3;
		int pointsVie =2;
		// Condition initiale
		// LO = LO = init(POint::init(x,y),largeur,hauteur)
		muraille.init(new Point(x, y), largeur, hauteur, pointsVie);

		// Operations
		// L1 = Muraille:retrait(1)
		muraille.retrait(1);

		// ORACLE
		// L1.pointsDeVie() == L0.pointsDeVie()-1

		// RAPPORT
		assertEquals("retrait, points de vie attendu " + (pointsVie - 1)
				+ " trouver " + muraille.pointDeVie(), pointsVie - 1,
				muraille.pointDeVie());
	}
}
