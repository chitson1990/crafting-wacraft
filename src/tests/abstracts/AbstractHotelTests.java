package tests.abstracts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enums.RACE;
import errors.PreconditionError;
import services.IHotelVilleService;

//TODO
public abstract class AbstractHotelTests {

	protected IHotelVilleService hotel;

	@After
	public void after() {
		hotel = null;
	}

	@Before
	public abstract void before();

	@Test
	public void test_init() {
		// condition initiale
		// acune

		// operations
		// init

		int largeur = 31, hauteur = 11;

		RACE race = RACE.HUMAIN;
		Point position = new Point(largeur,hauteur);
		hotel.init(largeur, hauteur,position,race);

		// Oracle
		// hotel.largeur()=largeur
		// hotel.hauteur()=hauteur
		// hotel.quantiteDor =16
		// hotel.getX() = largeur
		// hotel.getY() = hauteur

		// Rapport
		assertEquals(
				"init, Hotel de ville largeur pas le meme que celui passer",
				true, hotel.largeur() == largeur);
		assertEquals(
				"init, Hotel de ville hauteur pas le meme que celui passer",
				true, hotel.hauteur() == hauteur);
	}

	
	@Test(expected=PreconditionError.class)
	public void test_init_Erro() {
		// condition initiale
		// acune

		// operations
		// init

		int largeur = 30, hauteur = 11;

		RACE race = RACE.HUMAIN;
		Point position = new Point(largeur,hauteur);
		hotel.init(largeur, hauteur,position,race);

		// Oracle
		// Message d'erreur

		// Rapport
		assertEquals(
				"init, Hotel de ville largeur pas le meme que celui passer",
				true, hotel.largeur() == largeur);
		assertEquals(
				"init, Hotel de ville hauteur pas le meme que celui passer",
				true, hotel.hauteur() == hauteur);
	}

	
	@Test
	public void test_depot() {
		// condition initiale init
		// L0 = init(55,55)
		

		RACE race = RACE.ORC;
		
		hotel.init(55, 55, new Point(55,55),race);

		// Operations
		// L1 = hotel.depot(3)

		hotel.depot(3);

		int expected = 19;
		// Oracle
		// L1.quantiteDor = L1.quantiteDor + 3

		// RAPPORT
		assertEquals("depot, expected " + expected + " quantite d'or actuelle "
				+ hotel.quantityDor(), expected, hotel.quantityDor());
	}

	@Test(expected = PreconditionError.class)
	public void test_depot_2() {
		// condition initial
		// L0 = init(55,56)

		RACE race = RACE.ORC;
		hotel.init(55, 56, new Point(55,55),race);

		// Operations
		// L1 = hotel.depot(3)

		hotel.depot(-3);

		int expected = 19;
		// Oracle
		// HotelVilleContract doit jeter PreConditionError

		// RAPPORT
		assertEquals("depot, expected " + expected + " quantite d'or actuelle "
				+ hotel.quantityDor(), expected, hotel.quantityDor());
	}

	@Test
	public void test_abandoned_1() {
		// condition initiale
		// aucune

		// Operation
		// init
		// L0 = init(21,31)


		RACE race = RACE.HUMAIN;
		hotel.init(21, 31, new Point(55,22),race);

		// Oracle
		// HotelVille::estAbandone(LO) = true

		// RAPPORT
		assertTrue("Hotel doit etre abandonee a ce point",
				hotel.estAbandonnee());
	}

	@Test
	public void test_abandoned_2() {
		// condition initiale
		// LO = inti(21.31)


		RACE race = RACE.ORC;
		hotel.init(21, 31, new Point(55,55),race);

		// Operation
		// depot(3)
		// L1 = HotelVille:depot(LO,3)

		hotel.depot(3);

		// Oracle
		// HotelVille::estAbandone(LO) = false

		// RAPPORT
		assertFalse("Hotel ne doit pas etre abandonee a ce point",
				hotel.estAbandonnee());
	}
	
	

	@Test
	public void testSetX() {
		// condition initiale
		// LO = Mine:init(3.3)
		int x = 56;
		int y = 23;
		Point position = new Point(x, y);
		hotel.init(3, 3, position, RACE.HUMAIN);
		int x_expected = 6;
		// Operations
		// L1 = setX(LO, x_expected)

		hotel.setX(x_expected);

		// ORACLE
		// L1.getX() = x_expected

		// RAPPORT
		assertEquals("Tests x position: expected " + x_expected + "  : found"
				+ hotel.getX(), x_expected, hotel.getX());
	}

	@Test
	public void testSetY() {
		// condition initiale
		// LO = Mine:init(3.3)
		int x = 56;
		int y = 23;
		Point position = new Point(x, y);
		hotel.init(3, 3, position, RACE.HUMAIN);
		int y_expected = 6;
		// Operations
		// L1 = setY(LO, y_expected)

		hotel.setY(y_expected);
		// ORACLE
		// L1.getY() = y_expected

		// RAPPORT
		assertEquals("Tests y position: expected " + y_expected + "  : found"
				+ hotel.getY(), y_expected, hotel.getY());
	}
}
