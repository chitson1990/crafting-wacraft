package tests.abstracts;

import static org.junit.Assert.assertEquals;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import services.IRouteService;

//TODO
public abstract class AbstractRouteTests {

	protected IRouteService route;
	
	@Before
	public abstract void before();
	
	@After 
	public void after() {
		route = null;
	}
	
	@Test
	public void test_init() {
		int x = 2;
		int y = 3;
		
		int largeur =5;
		int hauteur = 3;
		// Condition initiale 
		// Aucune
		
		// Operations init
		// LO = init(POint::init(x,y),largeur,hauteur)
		route.init(new Point(x,y), largeur, hauteur);
		
		// ORACLE
		// getX(LO)=x
		// getY(LO)=y
		// largeur(LO)=largeur
		// hauteur(LO)=hauteur
		
		// RAPPORT
		

		assertEquals("Tests y position: expected " + y + "  : found" + route.getY(),y,route.getY());
		assertEquals("Tests x position: expected " + x + "  : found" + route.getX(),x,route.getX());

		assertEquals("Tests largeur: expected " + largeur + "  : found" + route.largeur(),largeur ,route.largeur());
		assertEquals("Tests largeur: expected " + hauteur + "  : found" + route.hauteur(),hauteur,route.hauteur());
	}
	
	@Test
	public void testSetX() {
		int x = 2;
		int y = 3;
		
		int largeur =5;
		int hauteur = 3;
		// Condition initiale
		// LO = LO = init(POint::init(x,y),largeur,hauteur)
		route.init(new Point(x,y), largeur, hauteur);
		
		int x_expected =6;
		// Operations
		// L1 = setX(LO, x_expected)
		
		route.setX(x_expected);
		
		//ORACLE
		// L1.getX() = x_expected
		
		//RAPPORT
		assertEquals("Tests x position: expected " + x_expected + "  : found" + route.getX(), x_expected, route.getX());
	}
	
	
	@Test
	public void testSetY() {
		int x = 2;
		int y = 3;
		
		int largeur =5;
		int hauteur = 3;
		// Condition initiale
		// LO = LO = init(POint::init(x,y),largeur,hauteur)
		route.init(new Point(x,y), largeur, hauteur);
		
		int y_expected =6;
		// Operations
		// L1 = setY(LO, y_expected)
		
		route.setY(y_expected);
		//ORACLE
		// L1.getY() = y_expected
		
		//RAPPORT
		assertEquals("Tests y position: expected " + y_expected + "  : found" + route.getY(), y_expected, route.getY());
	}
}
