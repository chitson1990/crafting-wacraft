package tests.abstracts;

import static org.junit.Assert.assertEquals;

import java.awt.Point;

import implementation.Muraille;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enums.OBJECTS;
import services.IMurrailleService;
import services.ITerrainJeuxService;

//TODO
public abstract class AbstractTerrainTests {

	protected ITerrainJeuxService terrain;

	@Before
	public abstract void before();

	@After
	public void after() {
		terrain = null;
	}

	@Test
	public void test_init() {
		int hauteur = 15;
		int largeur = 13;
		// condition initiale : aucune

		// opération
		// init
		terrain.init(largeur, hauteur);

		// ORACLE
		// pas de message d'erreur

		// RAPPORT
		assertEquals("init, hauteur n'est pas correcte", true,
				terrain.hauteur() == hauteur);
		assertEquals("init, largeur n'est pas correcte ", true,
				terrain.largeur() == largeur);
		assertEquals(" numeroMurailles must be empty", 0, terrain.numerosMurailles().size());

	}

	@Test
	public void testSetObjectAt() {
		int hauteur = 15;
		int largeur = 13;
		// condition initiale
		// LO= init (13,15)
		terrain.init(largeur, hauteur);
		// opération
		// L1 = setObjectAt(LO,5,5, MINE)

		terrain.setObjectAt(5, 5, OBJECTS.MINE);

		// ORACLE
		// getObjectAt(L1,5,5) =MINE

		// RAPPORT

		assertEquals("getObjectAt(5,5) expected " + OBJECTS.MINE.toString()
				+ "but found " + terrain.getObjectAt(5, 5).toString(),
				OBJECTS.MINE, terrain.getObjectAt(5, 5));

	}
	
	@Test
	public void testSetObjectAt_1() {
		int TerrainHauteur = 15;
		int Terrainlargeur = 13;
		// condition initiale
		// LO= init (13,15)
		terrain.init(Terrainlargeur,TerrainHauteur);
		// opération
		// L1 = setObjectAt(LO,5,5, MINE)

		int x =5;
		int y=5;
		IMurrailleService service = new Muraille();
		service.init(new Point(x,y), 3, 5,3);
		terrain.setObjectAt(x, y, service);

		// ORACLE
		// getObjectAt(L1,5,5) =MINE

		// RAPPORT

		for (int i =0 ; i < service.largeur() ; i++) {
			for (int j =0 ; j < service.hauteur() ; j++) {
				assertEquals("getObjectAt(" + (x+i)+", " + (y+j)+ ") expected " + service.myType().toString()
						+ "but found " + terrain.getObjectAt(x+i, y+j).toString(),
						service.myType(), terrain.getObjectAt(x+i, y+j));
			}
		}
	}
}
