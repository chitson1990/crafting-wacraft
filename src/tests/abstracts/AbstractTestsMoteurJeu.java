package tests.abstracts;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enums.COMMAND;
import services.IMoteurJeuService;

public abstract class AbstractTestsMoteurJeu {

	protected IMoteurJeuService moteur;

	@Before
	public abstract void setUp();
	
	@After
	public void after() {
		moteur = null;
	}

	@Test
	public void testInit() {
		int expectedMaxPasJeu = 3600;
		int expectedPasJeuCourant = 0;
		
		// conditions initiales: aucunes

		// opérations
		moteur.init(expectedMaxPasJeu);

		int actualMaxPasJeu = moteur.maxPasJeu();
		int actualPasJeuCourant = moteur.pasJeuCourant();

		// oracle
		assertEquals("Expected " + expectedMaxPasJeu + " but was " + actualMaxPasJeu, expectedMaxPasJeu, actualMaxPasJeu);
		assertEquals("Expected " + expectedPasJeuCourant + " but was " + actualPasJeuCourant, expectedPasJeuCourant, actualPasJeuCourant);

		for(int numerosVillageois : moteur.numerosVillageois()) {
			assertTrue("Le villageois " + numerosVillageois + " n'est pas à moins de 51 pixels de l'hotel de ville.", moteur.peutEntrerHotelVille(numerosVillageois));
		}

		assertFalse("Le jeu est terminé juste après l'init.", moteur.estFini());

	}

	@Test
	public void testLargeurTerrain() {
		int expectedLargeur = 600;
		
		// conditions initiales
		moteur.init(3600);

		//opérations
		int actualLargeur = moteur.largeurTerrain();

		// oracle
		assertEquals("La largeur du terrain n'est pas celle escomptée", expectedLargeur, actualLargeur);
	}

	@Test
	public void testHauteurTerrain() {
		int expectedHauteur = 400;
		
		// conditions initiales
		moteur.init(3600);

		// opérations
		int actualHauteur = moteur.hauteurTerrain();

		// oracle
		assertEquals("La hauteur du terrain n'est pas celle escomptée", expectedHauteur, actualHauteur);
	}

	@Test
	public void testMaxPasJeu() {
		int expectedMaxPasJeu = 3600;
		
		// conditions initiales
		moteur.init(expectedMaxPasJeu);

		// opérations
		int actualMaxPasJeu = moteur.maxPasJeu();

		// oracle
		assertEquals("Le nombre max de pasJeu n'est pas celui escompté", expectedMaxPasJeu, actualMaxPasJeu);
	}

	@Test
	public void testPasJeuCourantAfterInit() {
		int expectedPasJeuCourant = 0;

		// conditions initiales
		moteur.init(3600);

		// opérations
		int actualPasJeuCourant = moteur.pasJeuCourant();

		// oracle
		assertEquals("Le pasJeu courant après init() n'est pas celui escompté", expectedPasJeuCourant, actualPasJeuCourant);
	}

	@Test
	public void testPeutEntrerHotelVilleAfterInit() {
		
		// conditions initiales
		moteur.init(3600);

		// oracle
		for(int numerosVillageois : moteur.numerosVillageois()) {
			assertTrue("Le villageois " + numerosVillageois + " n'est pas à moins de 51 pixels de l'hotel de ville.", moteur.peutEntrerHotelVille(numerosVillageois));
		}
	}

	@Test
	public void testEstFiniAfterInit() {
		
		// conditions initiales
		moteur.init(3600);

		// opérations: aucunes
		
		// oracle
		assertFalse("Le jeu est terminé juste après l'init.", moteur.estFini());
	}

	@Test
	public void testEstFini() {
		
		// conditions initiales
		moteur.init(0);
		
		// opérations: aucunes
		
		// oracle
		assertTrue("Le jeu n'est pas terminé.", moteur.estFini());
	}
	
	@Test
	public void testAvancementPasJeu() {
		
		// conditions initiales
		moteur.init(3600);
		
		// opérations
		moteur.pasJeu(COMMAND.RIEN, 0, 0);
		
		// oracle
		assertEquals("La pas de jeu n'a pas correctement progressé", 1, moteur.pasJeuCourant());
	}
	
	@Test
	public void testPasJeuDeplacementDroite() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 1, 0);
				
		// oracle
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement vers la droite", 7, moteur.positionVillageoisX(1));
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement vers la droite", 6, moteur.positionVillageoisY(1));
	}
	
	@Test
	public void testPasJeuDeplacementDiagonaleHauteDroite() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 1, 45);
				
		// oracle
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement en diagonale haute droite", 7, moteur.positionVillageoisX(1));
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement en diagonale haute droite", 5, moteur.positionVillageoisY(1));
	}
	
	@Test
	public void testPasJeuDeplacementHaut() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 1, 90);
				
		// oracle
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement vers le haut", 6, moteur.positionVillageoisX(1));
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement vers le haut", 5, moteur.positionVillageoisY(1));
	}
	
	@Test
	public void testPasJeuDeplacementDiagonaleHauteGauche() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 1, 135);
				
		// oracle
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement en diagonale haute gauche", 5, moteur.positionVillageoisX(1));
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement en diagonale haute gauche", 5, moteur.positionVillageoisY(1));
	}
	
	@Test
	public void testPasJeuDeplacementGauche() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 1, 180);
				
		// oracle
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement vers la gauche", 5, moteur.positionVillageoisX(1));
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement vers la gauche", 6, moteur.positionVillageoisY(1));
	}
	
	@Test
	public void testPasJeuDeplacementDiagonaleBasseGauche() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 1, 225);
				
		// oracle
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement en diagonale basse gauche", 5, moteur.positionVillageoisX(1));
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement en diagonale basse gauche", 7, moteur.positionVillageoisY(1));
	}
	
	@Test
	public void testPasJeuDeplacementBas() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 1, 270);
				
		// oracle
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement vers le bas", 6, moteur.positionVillageoisX(1));
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement vers le bas", 7, moteur.positionVillageoisY(1));
	}
	
	@Test
	public void testPasJeuDeplacementDiagonaleBasseDroite() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 1, 315);
				
		// oracle
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement en diagonale basse droite", 7, moteur.positionVillageoisX(1));
		assertEquals("Le villageois 1 ne s'est pas déplacé correctement en diagonale basse droite", 7, moteur.positionVillageoisY(1));
	}
	
	@Test
	public void testPasJeuDeplacementDroiteSurRoute() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 0, 0);
				
		// oracle
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement vers la droite", 8, moteur.positionVillageoisX(0));
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement vers la droite", 6, moteur.positionVillageoisY(0));
	}
	
	@Test
	public void testPasJeuDeplacementDiagonaleHauteDroiteSurRoute() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 0, 45);
				
		// oracle
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement en diagonale haute droite", 8, moteur.positionVillageoisX(0));
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement en diagonale haute droite", 0, moteur.positionVillageoisY(0));
	}
	
	@Test
	public void testPasJeuDeplacementHautSurRoute() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 0, 90);
				
		// oracle
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement vers le haut", 6, moteur.positionVillageoisX(0));
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement vers le haut", 0, moteur.positionVillageoisY(0));
	}
	
	@Test
	public void testPasJeuDeplacementDiagonaleHauteGaucheSurRoute() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 0, 135);
				
		// oracle
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement en diagonale haute gauche", 4, moteur.positionVillageoisX(0));
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement en diagonale haute gauche", 0, moteur.positionVillageoisY(0));
	}
	
	@Test
	public void testPasJeuDeplacementGaucheSurRoute() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 0, 180);
				
		// oracle
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement vers la gauche", 4, moteur.positionVillageoisX(0));
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement vers la gauche", 2, moteur.positionVillageoisY(0));
	}
	
	@Test
	public void testPasJeuDeplacementDiagonaleBasseGaucheSurRoute() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 0, 225);
				
		// oracle
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement en diagonale basse gauche", 5, moteur.positionVillageoisX(0));
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement en diagonale basse gauche", 3, moteur.positionVillageoisY(0));
	}
	
	@Test
	public void testPasJeuDeplacementBasSurRoute() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 0, 270);
				
		// oracle
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement vers le bas", 6, moteur.positionVillageoisX(0));
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement vers le bas", 3, moteur.positionVillageoisY(0));
	}
	
	@Test
	public void testPasJeuDeplacementDiagonaleBasseDroiteSurRoute() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.DEPLACER, 0, 315);
				
		// oracle
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement en diagonale basse droite", 7, moteur.positionVillageoisX(0));
		assertEquals("Le villageois 0 ne s'est pas déplacé correctement en diagonale basse droite", 3, moteur.positionVillageoisY(0));
	}
	
	@Test
	public void testPasJeuAttaquerMuraille() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.ATTAQUERMURAILLE, 1, 0);
				
		// oracle
		assertEquals("La muraille 0 n'a pas été correctement attaquée", 1, moteur.getMuraille(0).pointDeVie());
	}
	
	@Test
	public void testPasJeuDetruireMuraille() {
		// conditions initiales
		moteur.init(3600);
				
		// opérations
		moteur.pasJeu(COMMAND.ATTAQUERMURAILLE, 0, 0);
				
		// oracle
		assertEquals("La muraille 0 n'a pas été correctement détruite", 0, moteur.numerosMurailles().size());
	}
	
	@Test
	public void testMiner() {
		// conditions initiales
		moteur.init(3600);
		
		int preOrMine = moteur.getMine(0).orRestant();
				
		// opérations
		moteur.pasJeu(COMMAND.ENTRERMINE, 1, 0);
		for(int i = 0; i < 16; i++)
			moteur.pasJeu(COMMAND.RIEN, 0, 0);
				
		// oracle
		assertEquals("La mine n'a pas été correctement minée", preOrMine - 1, moteur.getMine(0).orRestant());
		assertEquals("Le Villageois n'a pas correctement miné", 1, moteur.getVillageois(1).quantiteOr());
	}

}
