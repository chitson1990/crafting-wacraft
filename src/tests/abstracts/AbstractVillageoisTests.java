package tests.abstracts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enums.RACE;
import services.IVillageoisService;

//TODO add more tests
public abstract class AbstractVillageoisTests {

	protected IVillageoisService villageois;

	@Before
	public abstract void before();

	@After
	public void after() {
		villageois = null;
	}

	@Test
	public void test_init() {
		// condition initiale aucune
		int largeur = 15;
		int hauteur = 13;
		final String race = "ORC";
		int force = 5;
		int vitesse = 5;
		int pointsVie = 3;
		Point position = new Point(largeur, hauteur);
		// operations init
		// LO =nit(race, largeur, hauteur, force, vitesse, pointsVie,position);
		villageois.init(race, largeur, hauteur, force, vitesse, pointsVie,
				position);

		// Oracle
		/**
		 *  estMort(LO) = false
		 *  race(LO) = race
		 *  largeur(LO) = largeur
		 *  hauteur(LO) = hauteur
		 *  force(LO) = force
		 *  pointDeVie(LO) = pointsVie
		 *  vitesse(LO) = vitesse
		 *  quantiteOr(LO) =0
		 */

		// RAPPORT
		assertTrue("init, villageois est pas mort", !villageois.estMort());
		assertEquals("init, race de villageois n'est pas correct ", true,
				villageois.race() == RACE.valueOf(race));
		assertEquals("init, largeur de villageois n'est pas correct ", true,
				villageois.largeur() == largeur);
		assertEquals("init, hauteur de villageois n'est pas correct ", true,
				villageois.hauteur() == hauteur);
		assertEquals("init, force de villageois n'est pas correct ", true,
				villageois.force() == force);

		assertEquals("init, vitesse de villageois n'est pas correct ",
				true, villageois.vitesse() == vitesse);
		assertEquals("init, pointsDeVie de villageois n'est pas correct ",
				true, villageois.pointDeVie() == pointsVie);
		assertEquals("gold quantity after init", true, villageois.quantiteOr()==0);
	}

	@Test
	public void test_retrait() {
		// condition initiale
		// LO = init(race, largeur, hauteur, force, vitesse, pointsVie);
		int largeur = 111;
		int hauteur = 13;
		final String race = "ORC";
		int force = 5;
		int vitesse = 5;
		int pointsVie = 3;

		Point position = new Point(largeur, hauteur);
		villageois.init(race, largeur, hauteur, force, vitesse, pointsVie,
				position);

		// Operations
		// L1 = Villageois:retrait(1)
		villageois.retrait(1);

		// ORACLE
		// L1.pointsDeVie() == L0.pointsDeVie()-1

		// RAPPORT
		assertEquals("retrait, points de vie attendu " + (pointsVie - 1)
				+ " trouver " + villageois.pointDeVie(), pointsVie - 1,
				villageois.pointDeVie());
	}

	@Test
	public void testEntrerMine() {
		// condition initiale
		// LO = init(race, largeur, hauteur, force, vitesse, pointsVie);
		int largeur = 19;
		int hauteur = 17;
		final String race = "ORC";
		int force = 5;
		int vitesse = 5;
		int pointsVie = 3;

		Point position = new Point(largeur, hauteur);
		villageois.init(race, largeur, hauteur, force, vitesse, pointsVie,
				position);

		// Operation
		// L1 = entrerMine(LO)

		villageois.entrerMine();

		// Oracle
		// resteDansLaMinePourPasDeJeu(L1) =51

		// RAPPORT
		assertEquals(
				" test enterMine Expected " + 16 + " Found "
						+ villageois.resteDansLaMinePourPasDeJeu(), 16,
				villageois.resteDansLaMinePourPasDeJeu());
	}

	@Test
	public void testSortirMine() {
		// condition initiale
		// LO = init(race, largeur, hauteur, force, vitesse, pointsVie);
		// L1 = entrerMine(LO)
		int largeur = 11;
		int hauteur = 13;
		final String race = "ORC";
		int force = 5;
		int vitesse = 5;
		int pointsVie = 3;

		Point position = new Point(largeur, hauteur);
		villageois.init(race, largeur, hauteur, force, vitesse, pointsVie,
				position);
		villageois.entrerMine();
		// Operation
		// L2 = sortirMine(L1)

		villageois.sortirMine();

		// Oracle
		// resteDansLaMinePourPasDeJeu(L2) =0

		// RAPPORT
		assertEquals(
				" test sortirMine Expected " + 0 + " Found "
						+ villageois.resteDansLaMinePourPasDeJeu(), 0,
				villageois.resteDansLaMinePourPasDeJeu());
	}

	@Test
	public void testSetX() {
		// condition initiale
		// LO = init(race, largeur, hauteur, force, vitesse, pointsVie);
		int largeur = 11;
		int hauteur = 11;
		final String race = "ORC";
		int force = 5;
		int vitesse = 5;
		int pointsVie = 3;

		Point position = new Point(largeur, hauteur);
		villageois.init(race, largeur, hauteur, force, vitesse, pointsVie,
				position);

		int x_expected = 6;
		// Operations
		// L1 = setX(LO, x_expected)

		villageois.setX(x_expected);
		// ORACLE
		// L1.getX() = x_expected

		// RAPPORT
		assertEquals("Tests x position: expected " + x_expected + "  : found"
				+ villageois.getX(), x_expected, villageois.getX());
	}

	@Test
	public void testSetY() {
		// condition initiale
		// LO = init(race, largeur, hauteur, force, vitesse, pointsVie);
		int largeur = 11;
		int hauteur = 11;
		final String race = "ORC";
		int force = 5;
		int vitesse = 5;
		int pointsVie = 3;

		Point position = new Point(largeur, hauteur);
		villageois.init(race, largeur, hauteur, force, vitesse, pointsVie,
				position);

		int y_expected = 6;
		// Operations
		// L1 = setY(LO, y_expected)

		villageois.setY(y_expected);
		// ORACLE
		// L1.getY() = y_expected

		// RAPPORT
		assertEquals("Tests y position: expected " + y_expected + "  : found"
				+ villageois.getY(), y_expected, villageois.getY());
	}

	@Test
	public void testRemplier() {
		// condition initiale
		// LO = init(race, largeur, hauteur, force, vitesse, pointsVie);
		int largeur = 11;
		int hauteur = 11;
		final String race = "ORC";
		int force = 5;
		int vitesse = 5;
		int pointsVie = 3;

		Point position = new Point(largeur, hauteur);
		villageois.init(race, largeur, hauteur, force, vitesse, pointsVie,
				position);

		int initial = villageois.quantiteOr();

		int toAdd = 3;

		int expected = initial + toAdd;

		// Operations
		// L1 = remplierPoches(LO,3)

		villageois.remplirPoches(toAdd);

		// ORACLE
		// quantitDor(L1) = expected

		assertEquals(" Quantite d'or expected " + expected + " found"
				+ villageois.quantiteOr(), expected, villageois.quantiteOr());
	}

	@Test
	public void testViderPoches() {
		// condition initiale
		// LO = init(race, largeur, hauteur, force, vitesse, pointsVie);
		// L1 = remplierPoches(LO,3)

		int largeur = 11;
		int hauteur = 11;
		final String race = "ORC";
		int force = 5;
		int vitesse = 5;
		int pointsVie = 3;

		Point position = new Point(largeur, hauteur);
		villageois.init(race, largeur, hauteur, force, vitesse, pointsVie,
				position);
		int toAdd = 3;

		villageois.remplirPoches(toAdd);

		// Operations
		// L2= viderPoches(L1)
		villageois.viderPoches();

		int expected = 0;
		// ORACLE
		// quantitDor(L2) = 0

		assertEquals(" Quantite d'or expected " + expected + " found"
				+ villageois.quantiteOr(), expected, villageois.quantiteOr());
	}
}
