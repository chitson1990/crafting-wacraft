package tests.abstracts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enums.RACE;
import errors.PreconditionError;
import services.IMineService;

//TODO
public abstract class AbstractMineTests {

	protected IMineService mine;

	@Before
	public abstract void before();

	@After
	public void after() {
		mine = null;
	}

	@Test
	public void test_init() {

		int largeur = 3;
		int hauteur = 3;

		Point position = new Point(largeur, hauteur);
		// condition initiale
		// aucune

		// Operations
		// LO = Mine:init(largeur, hauteur)
		mine.init(largeur, hauteur, position, RACE.HUMAIN);

		// Oracle
		// Pas d'erreur envoyer et
		// Mine:largeur(LO) = largeur
		// Mine:hauteur(LO) = hauteur
		// Mine:getX(LO) = position.x
		// Mine.getY(LO) = position.y

		// RAPPORT
		assertEquals(" Should be" + largeur + " But found" + mine.largeur(),
				largeur, mine.largeur());
		assertEquals(" Should be" + largeur + " But found" + mine.largeur(),
				largeur, mine.largeur());
		assertEquals(" RACE should be ", RACE.HUMAIN, mine.appartientA());
		assertEquals("Tests y position: expected " + position.y + "  : found"
				+ mine.getY(), position.y, mine.getY());
		assertEquals("Tests x position: expected " + position.x + "  : found"
				+ mine.getX(), position.x, mine.getX());
	}

	@Test(expected = PreconditionError.class)
	public void test_init_2() {

		int largeur = -2;
		int hauteur = 3;

		int x = 12;
		int y = 23;
		Point position = new Point(x, y);

		// condition initiale
		// aucune

		// Operations
		// LO = Mine:init(largeur, hauteur)

		mine.init(largeur, hauteur, position, RACE.HUMAIN);

		// Oracle
		// Precondition erreur envoyer.

		// RAPPORT
	} // Mine et MineBugged echoue le test mais MineContract le passe.

	@Test
	public void test_retrait() {
		// condition initiale
		// LO = Mine:init(3.3)
		int x = 56;
		int y = 23;
		Point position = new Point(x, y);
		mine.init(3, 3, position, RACE.HUMAIN);

		// Operations
		// L1 = Mine.retrait(LO,1);
		mine.retrait(1);

		// ORACLE
		// orRestant(L1) = 50

		assertEquals("Une unite d'or extracted. Expected" + 50 + " Found"
				+ mine.orRestant(), 50, mine.orRestant());
	}

	@Test
	public void test_estLamine() {
		// condition initiale
		// LO = Mine:init(3.3)
		int x = 56;
		int y = 23;
		Point position = new Point(x, y);
		mine.init(3, 3, position, RACE.ORC);

		// Operations
		// L1 = Mine.retrait(LO,51);
		mine.retrait(51);

		// Oracle
		// Mine est laminer

		// RAPPORT

		assertEquals("Mine should be exhausted", true, mine.estLaminee());
	}

	@Test
	public void test_estLamine_1() {
		// condition initiale
		// LO = Mine:init(3.3)

		int y = 63;
		int x = 41;
		Point position = new Point(x, y);
		mine.init(3, 3, position, RACE.HUMAIN);

		// Operations
		// L1 = Mine.retrait(LO,51);
		mine.retrait(21);

		// Oracle
		// Mine est laminer

		// RAPPORT

		assertEquals("Mine should not be exhausted", false, mine.estLaminee());
	}

	@Test
	public void test_estAbandonee() {
		// condition initiale
		// LO = Mine:init(3,3)
		// L1 = Mine::accueil(LO)
		int y = 63;
		int x = 41;
		Point position = new Point(x, y);
		mine.init(3, 3, position, RACE.HUMAIN);

		mine.acceuil();

		// Operations
		// L2= Mine:acceuil(L1);

		for (int i = 0; i < 5; i++)
			mine.abandoned();

		// Oracle
		// Mine pas abandonner

		// RAPORT
		assertEquals("Mine should be not abandoned at this stage", false,
				mine.estAbandonnee());
	}

	@Test
	public void test_estAbandonee_2() {
		// condition initiale
		// LO = Mine:init(3,3)

		int y = 63;
		int x = 41;
		Point position = new Point(x, y);
		mine.init(3, 3, position, RACE.ORC);

		mine.acceuil();
		// Operations
		/**
		 * pour i =1 a 51 L(i+1)= Mine:abandon(L(i));
		 */

		for (int i = 0; i < 51; i++)
			mine.abandoned();

		// Oracle
		// Mine abandonner at this stage

		// RAPORT
		assertTrue("Mine should be abandoned at this stage",
				mine.estAbandonnee());
	}

	@Test
	public void testSetX() {
		// condition initiale
		// LO = Mine:init(3.3)
		int x = 56;
		int y = 23;
		Point position = new Point(x, y);
		mine.init(3, 3, position, RACE.HUMAIN);
		int x_expected = 6;
		// Operations
		// L1 = setX(LO, x_expected)

		mine.setX(x_expected);

		// ORACLE
		// L1.getX() = x_expected

		// RAPPORT
		assertEquals("Tests x position: expected " + x_expected + "  : found"
				+ mine.getX(), x_expected, mine.getX());
	}

	@Test
	public void testSetY() {
		// condition initiale
		// LO = Mine:init(3.3)
		int x = 56;
		int y = 23;
		Point position = new Point(x, y);
		mine.init(3, 3, position, RACE.HUMAIN);
		int y_expected = 6;
		// Operations
		// L1 = setY(LO, y_expected)

		mine.setY(y_expected);
		// ORACLE
		// L1.getY() = y_expected

		// RAPPORT
		assertEquals("Tests y position: expected " + y_expected + "  : found"
				+ mine.getY(), y_expected, mine.getY());
	}
}
