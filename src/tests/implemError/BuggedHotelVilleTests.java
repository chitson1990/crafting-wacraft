package tests.implemError;

import tests.abstracts.AbstractHotelTests;
import bugged_implementation.HotelVilleBugged;

public class BuggedHotelVilleTests extends AbstractHotelTests {

	@Override
	public void before() {
		hotel = new HotelVilleBugged();
	}

}
