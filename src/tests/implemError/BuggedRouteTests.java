package tests.implemError;

import tests.abstracts.AbstractRouteTests;
import bugged_implementation.RouteBugged;

public class BuggedRouteTests extends AbstractRouteTests {

	@Override
	public void before() {
		route = new RouteBugged();
	}

}
