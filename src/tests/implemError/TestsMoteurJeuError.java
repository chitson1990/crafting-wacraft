package tests.implemError;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;

import enums.RACE;
import bugged_implementation.HotelVilleBugged;
import bugged_implementation.MineBugged;
import bugged_implementation.MoteurJeuBugged;
import bugged_implementation.MurailleBugged;
import bugged_implementation.RouteBugged;
import bugged_implementation.TerrainBugged;
import bugged_implementation.VillageoisBugged;
import services.IHotelVilleService;
import services.IMineService;
import services.IMurrailleService;
import services.IRouteService;
import services.ITerrainJeuxService;
import services.IVillageoisService;
import tests.abstracts.AbstractTestsMoteurJeu;

public class TestsMoteurJeuError extends AbstractTestsMoteurJeu {

	@Before
	public void setUp() {
		IHotelVilleService hotelVille = new HotelVilleBugged();
		IVillageoisService villageois0 = new VillageoisBugged();
		IVillageoisService villageois1 = new VillageoisBugged();
		IMurrailleService muraille = new MurailleBugged();
		IMineService mine = new MineBugged();
		IRouteService route  = new RouteBugged();
		ITerrainJeuxService terrain = new TerrainBugged();
		moteur = new MoteurJeuBugged();
		
		hotelVille.init(5, 7, new Point(11, 2), RACE.ORC);
		villageois0.init("ORC", 3, 3, 2, 1, 10, new Point(6, 2));
		villageois1.init("ORC", 3, 3, 2, 1, 10, new Point(6, 6));
		muraille.init(new Point(5, 10), 5, 1, 2);
		mine.init(3, 3, new Point(1, 5), RACE.ORC);
		route.init(new Point(4, 3), 5, 1);
		terrain.init(600, 400);
		
		terrain.bind_hotel(new IHotelVilleService[]{hotelVille});
		terrain.bind_murraile(new ArrayList<IMurrailleService>(Arrays.asList(muraille)));
		terrain.bind_mine(new ArrayList<IMineService>(Arrays.asList(mine)));
		terrain.bind_route(new ArrayList<IRouteService>(Arrays.asList(route)));
		moteur.bind(terrain);
		moteur.bind(villageois0);
		moteur.bind(villageois1);
		moteur.bind_hotel(new IHotelVilleService[]{hotelVille});
	}

}
