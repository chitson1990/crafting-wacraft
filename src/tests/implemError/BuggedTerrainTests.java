package tests.implemError;

import tests.abstracts.AbstractTerrainTests;
import bugged_implementation.TerrainBugged;

public class BuggedTerrainTests extends AbstractTerrainTests {
	@Override
	public void before() {
		terrain = new TerrainBugged();
	}
}
