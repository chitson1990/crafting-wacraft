package tests.implemError;

import tests.abstracts.AbstractMineTests;
import bugged_implementation.MineBugged;

public class BuggedMineTests extends AbstractMineTests {

	@Override
	public void before() {
		mine =new MineBugged();
	}

}
