package tests.implemError;

import tests.abstracts.AbstractMurailleTests;
import bugged_implementation.MurailleBugged;

public class BuggedMurailleTests extends AbstractMurailleTests {

	@Override
	public void before() {
		muraille = new MurailleBugged();
	}

}
