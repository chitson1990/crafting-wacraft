package tests.implemError;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	BuggedMineTests.class,
	BuggedHotelVilleTests.class,
	BuggedMurailleTests.class,
	BuggedRouteTests.class,
	BuggedTerrainTests.class,
	BuggedVillageoisTests.class,
	TestsMoteurJeuError.class})

public class TestsAllError {}
