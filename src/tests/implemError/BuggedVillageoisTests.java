package tests.implemError;

import tests.abstracts.AbstractVillageoisTests;
import bugged_implementation.VillageoisBugged;

public class BuggedVillageoisTests extends AbstractVillageoisTests {

	@Override
	public void before() {
		villageois = new VillageoisBugged();
	}

}
