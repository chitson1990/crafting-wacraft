package tests.contracts;

import contracts.RouteContract;
import implementation.Route;
import tests.abstracts.AbstractRouteTests;

public class TestRouteContract extends AbstractRouteTests {

	@Override
	public void before() {
		route = new RouteContract(new Route());
	}

}
