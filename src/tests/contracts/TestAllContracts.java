package tests.contracts;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;



@RunWith(Suite.class)
@SuiteClasses({
	TestVillageoisContract.class,
	TestMineContract.class,
	TestHotelVilleContract.class,
	TestMurailleContract.class,
	TestRouteContract.class,
	TestTerrainContract.class,
	TestsMoteurJeuContract.class})

public class TestAllContracts {}
