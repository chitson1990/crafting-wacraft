package tests.contracts;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;

import implementation.HotelVille;
import implementation.Mine;
import implementation.MoteurJeu;
import implementation.Muraille;
import implementation.Route;
import implementation.Terrain;
import implementation.Villageois;

import org.junit.Before;

import contracts.HotelVilleContract;
import contracts.MineContract;
import contracts.MoteurJeuContract;
import contracts.MurailleContract;
import contracts.RouteContract;
import contracts.TerrainContract;
import contracts.VillageoisContract;
import enums.RACE;
import services.IHotelVilleService;
import services.IMineService;
import services.IMurrailleService;
import services.IRouteService;
import services.ITerrainJeuxService;
import services.IVillageoisService;
import tests.abstracts.AbstractTestsMoteurJeu;

public class TestsMoteurJeuContract extends AbstractTestsMoteurJeu{

	@Before
	public void setUp() {
		IHotelVilleService hotelVille = new HotelVilleContract(new HotelVille());
		IVillageoisService villageois0 = new VillageoisContract(new Villageois());
		IVillageoisService villageois1 = new VillageoisContract(new Villageois());
		IMurrailleService muraille = new MurailleContract(new Muraille());
		IMineService mine = new MineContract(new Mine());
		IRouteService route  = new RouteContract(new Route());
		ITerrainJeuxService terrain = new TerrainContract(new Terrain());
		moteur = new MoteurJeuContract(new MoteurJeu());
		
		hotelVille.init(5, 7, new Point(11, 2), RACE.ORC);
		villageois0.init("ORC", 3, 3, 2, 1, 10, new Point(6, 2));
		villageois1.init("ORC", 3, 3, 2, 1, 10, new Point(6, 6));
		muraille.init(new Point(5, 10), 5, 1, 2);
		mine.init(3, 3, new Point(1, 5), RACE.ORC);
		route.init(new Point(4, 3), 5, 1);
		terrain.init(600, 400);
		
		terrain.bind_hotel(new IHotelVilleService[]{hotelVille});
		terrain.bind_murraile(new ArrayList<IMurrailleService>(Arrays.asList(muraille)));
		terrain.bind_mine(new ArrayList<IMineService>(Arrays.asList(mine)));
		terrain.bind_route(new ArrayList<IRouteService>(Arrays.asList(route)));
		moteur.bind(terrain);
		moteur.bind(villageois0);
		moteur.bind(villageois1);
		moteur.bind_hotel(new IHotelVilleService[]{hotelVille});
	}

}
