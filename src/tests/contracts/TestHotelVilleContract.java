package tests.contracts;

import implementation.HotelVille;
import contracts.HotelVilleContract;
import tests.abstracts.AbstractHotelTests;

public class TestHotelVilleContract extends AbstractHotelTests {

	@Override
	public void before() {
		hotel = new HotelVilleContract(new HotelVille());
	}

}
