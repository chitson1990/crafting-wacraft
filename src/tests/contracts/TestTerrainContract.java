package tests.contracts;

import implementation.Terrain;
import contracts.TerrainContract;
import tests.abstracts.AbstractTerrainTests;

public class TestTerrainContract extends AbstractTerrainTests {

	@Override
	public void before() {
		terrain = new TerrainContract(new Terrain());
	}

}
