package tests.contracts;

import implementation.Mine;
import contracts.MineContract;
import tests.abstracts.AbstractMineTests;

public class TestMineContract extends AbstractMineTests {

	@Override
	public void before() {
		mine = new MineContract(new Mine());
	}

}
