package tests.implem;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	MineTests.class,
	HotelVilleTests.class,
	VillageoisTests.class,
	RouteTests.class,
	MurailleTests.class,
	TerrainTests.class,
	TestsMoteurJeuImpl.class})

public class TestsAllImpl {}
