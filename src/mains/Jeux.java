package mains;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import enums.COMMAND;
import enums.RACE;
import implementation.HotelVille;
import implementation.Mine;
import implementation.MoteurJeu;
import implementation.Muraille;
import implementation.Route;
import implementation.Terrain;
import implementation.Villageois;
import services.IHotelVilleService;
import services.IMineService;
import services.IMoteurJeuService;
import services.IMurrailleService;
import services.IRouteService;
import services.ITerrainJeuxService;
import services.IVillageoisService;

public class Jeux {

	public static void main(String[] args) {

		ITerrainJeuxService terrain = new Terrain();
		terrain.init(455, 357);

		IHotelVilleService firstHotel = new HotelVille();
		IHotelVilleService secondHotel = new HotelVille();
		firstHotel.init(35, 35, new Point(1, 1), RACE.HUMAIN);
		secondHotel.init(35, 35, new Point(301, 157), RACE.ORC);

		IHotelVilleService hotel[] = new IHotelVilleService[]{ firstHotel, secondHotel };
		terrain.bind_hotel(hotel);

		IMineService firstMine = new Mine();
		IMineService secondMine = new Mine();
		IMineService thirdMine = new Mine();
		IMineService fourthMine = new Mine();

		firstMine.init(11, 13, new Point(30, 15), RACE.HUMAIN);

		fourthMine.init(15, 17, new Point(150, 135), RACE.HUMAIN);

		secondMine.init(9, 31, new Point(1, 320), RACE.ORC);

		thirdMine.init(23, 21, new Point(400, 20), RACE.ORC);

		List<IMineService> mines = new ArrayList<IMineService>();

		mines.add(firstMine);
		mines.add(secondMine);
		mines.add(thirdMine);
		mines.add(fourthMine);

		terrain.bind_mine(mines);

		IRouteService firstRoute = new Route();
		IRouteService secondRoute = new Route();

		IMurrailleService firstMuraille = new Muraille();
		IMurrailleService secondMuraille = new Muraille();

		firstRoute.init(new Point(200, 80), 60, 2);
		secondRoute.init(new Point(300, 300), 60, 1);

		firstMuraille.init(new Point(65, 30), 2, 65, 60);
		secondMuraille.init(new Point(1, 330), 30, 12, 63);

		List<IRouteService> routes = new ArrayList<IRouteService>();

		routes.add(firstRoute);
		routes.add(secondRoute);
		terrain.bind_route(routes);

		List<IMurrailleService> murailles = new ArrayList<IMurrailleService>();
		murailles.add(firstMuraille);
		murailles.add(secondMuraille);
		terrain.bind_murraile(murailles);

		IVillageoisService first = new Villageois();
		first.init("ORC", 3, 3, 4, 4.5, 10, new Point(1, 1));
		IVillageoisService second = new Villageois();
		second.init("HUMAIN", 3, 3, 4, 5.5, 10, new Point(53, 11));

		IMoteurJeuService engine = new MoteurJeu();
		engine.bind(terrain);
		engine.bind_hotel(hotel);
		engine.bind(first);

		IMoteurJeuService engineSecond = new MoteurJeu();
		engineSecond.bind(terrain);
		engineSecond.bind_hotel(hotel);
		engineSecond.bind(second);

		engine.init(25656);
		engineSecond.init(25656);
		Random random = new Random();
		int chooseEngine;

		COMMAND command[] = { COMMAND.DEPLACER, COMMAND.ENTRERMINE,
				COMMAND.ENTRERHOTELVILLE, COMMAND.ATTAQUERMURAILLE,
				COMMAND.RIEN };
		COMMAND toDoCommand;
		int argument = 0;
		while (!engine.estFini() && !engineSecond.estFini()) {
			chooseEngine = random.nextInt(2);
			toDoCommand = command[random.nextInt(command.length -1)];
			
			switch(toDoCommand) {
				case ATTAQUERMURAILLE:
					argument = random.nextInt(engine.numerosMurailles().size());
					break;
					
				case DEPLACER:
					argument = random.nextInt(361);
					break;
					
				case ENTRERHOTELVILLE:
					break;
					
				case ENTRERMINE:
					argument = random.nextInt(engine.numerosMines().size());
					break;
					
				case RIEN:
					break;
			}
			
			if (chooseEngine == 1) {
				engine.pasJeu(toDoCommand, 0, argument);
			} else {
				engine.pasJeu(toDoCommand, 0, argument);
			}
		}

	}

}
