package services;

import enums.OBJECTS;

public interface IMyObjectService {
	/**
	 * Observators
	 */
	
	/**
	 *  Largeur
	 * @return int 
	 */
	public int largeur();
	/**
	 *	 Hauteur
	 * @return int
	 */
	public int hauteur();
	
	/**
	 * 
	 * @return OBJECT
	 */
	
	public OBJECTS myType();
	
	//getX : [IMyObjectService] -> int
	//getY : [IMyObjectService] -> int
	
	public int getX();
	public int getY();
	
	/**
	 * Operators
	 */
	//setX : [IMyObjectService] x int -> [IMyObjectService]
	//		pre setX(S, x) require x > 0
	//	setY : [IMyObjectService] x int -> [IMyObjectService]
	//	pre setY(S, y) require y > 0
	
	public void setX(int x);
	/*	pre setX(S, x) require x > 0
	 *	post
			getX(setX(S, x)) = x;
			// Reste invariable
			*/
	
	public void setY(int y);
	/*  pre setY(S, y) require y > 0
	 *	post
		getY(setY(S, y)) = y
		//Reste invariables	
 	 */
}
