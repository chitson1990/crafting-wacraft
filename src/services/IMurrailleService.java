package services;

import java.awt.Point;

//TODO
public interface IMurrailleService extends IMyObjectService{

	/**
	 * Observators
	 */
	
	
	/**
	 * Points de vie d'une muraille
	 * @return int
	 */
	public int pointDeVie();
	
	/**
	 * Tester si une muraille est detruit
	 * @return int
	 */
	public boolean estDetruit();
	
	/**
	 * Longeur de la Muraillle qu'on envisage construire
	 * @return int
	 */
	@Override
	public int hauteur();
	
	/**
	 * La longeur reel de la Muraille apres sa construction
	 * @return int
	 */
	@Override
	public int largeur();
	
	/**
	 * Une Muraille passe par un tel point.<br>
	 * pre:<br>
	 * 		pre passAt(x,y) requires x>0 ^ y>0 <br>
	 * @param x
	 * @param y
	 * @return boolean
	 */
	
	public boolean passAt(int x, int y);
	
	/**
	 * Constructors
	 */
	
	/**
	 * pre:<br>
	 * 		pre init(position, Length, hauteur, int Vie) requires Vie > 0 ^position <> null ^ largeur >0 ^ hauteur>0 <br>
	 * post:<br>
	 * 		getX(init(p,l,h,vie)) = p.x
	 * 		getY(init(p,l,h,vie)) = p.y
	 * 		hauteur(init(p,l,h,vie))=h; <br>
	 * 		longuer(init(p,l,h,vie))=l; <br>
	 * 		pointVie(init(p,l,h,vie)) = vie;
	 * @param maxLength
	 */
	public void init (Point position, int largeur, int hauteur, int pointVie);

	
	/**
	*  pre: <br>
	*		pre retrait(Muraille,s) require !estDetruit(Muraille) && s>0 <br>
	*<br>
	*  post: <br>
	*  		pointsDeVie(retrait(Muraille,s))=pointsDeVie(Muraille) -s <br>
	*  		getX(retrait(Muraille,s)) = getX(Muraille) <br>
	*  		getY(retrait(Muraille,s)) = getY(Muraille) <br>
	**/
	void retrait (int s) ;

	/**
	 * pre:<br> 
	 *		pre setX(M,x) require x>0 <br>
	 *<br>
	 * Post:<br>
	 * 		pointDeVie(setY(M))=pointDeVie(M) <br>
	 * 		getX(setX(M,x)) = x <br>
	 * 		getY(setX(M,x)) = getY(M) <br>
	 * @param x
	 */
	@Override
	public void setX(int x); 
	
	
	/**
	 * pre:<br> 
	 *		pre setY(M,y) require y>0 <br>
	 *<br>
	 * Post:<br>
	 * 		pointDeVie(setY(M))=pointDeVie(M) <br>
	 * 		getX(setY(M,y)) = getX(M) <br>
	 * 		getY(sety(M,y)) = y <br>
	 * @param y
	 */
	@Override
	public void setY(int y); 
	
	
	/**
	 * Invariant:<br>
	 * 		estDetruit(M) = (min)pointsDeVie(M) ≤0	<br>
	 * */
}
