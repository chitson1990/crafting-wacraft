package services;

import java.awt.Point;

import enums.RACE;

public interface IVillageoisService extends IMyObjectService {

	/**
	 * Observators
	 */
	/**
	 * la race d'un villageois
	 * @return RACE
	 */
	public RACE race();
	
	/**
	 * La largeur d'un villageois
	 * @return int
	 */
	@Override
	public int  largeur();
	
	/**
	 * L'hauteur d'un villageois
	 * @return int
	 */
	@Override
	public int  hauteur(); 
	
	/**
	 * La force d'un villageois
	 * @return int
	 */
	public int  force(); 
	/**
	 * La vitesse d'un villageois
	 * @return int
	 */
	public double  vitesse (); 
	
	/**
	 * Points de vie d'un villageois
	 * @return int
	 */
	public int pointDeVie();
	
	/**
	 * Quantite d'or que porter un villageois
	 * @return int
	 */
	public int quantiteOr (); 
	
	/**
	 * Tester si un villageois est mort
	 * @return int
	 */
	public boolean estMort();
	
	/**
	 * Observator qui nous indique combine de pas de jeu il reste pour l'utilisateur qui est en train de miner
	 * @return int
	 */
	public int resteDansLaMinePourPasDeJeu();
	/**
	 * Observator pour savoir si le villageois est en train de miner
	 * @return boolean
	 */
	
	public boolean estEnTrainDeMiner(); 
	/**
	 * Constructors
	 */
	
	/**
	 * pre <br>
	 * 		init : String x int x int x int x double x int x Point -> [Villageois] <br>
	 *			pre init(race,largeur,hauteur,force,vitesse,pointsVie, pos) require pos.x > 0 ^ pos. >0 ^race <> "" && largeur%2=1 && hauteur%2=1 <br> 
	 * <br>
	 * <br>
	 * post <br>
	 * 		race(init(s,l,h,f,v,p,pos))=r <br>
	 *		largeur(init(s,l,h,f,v,p,pos))=l <br>
	 *		hauteur(init(s,l,h,f,v,p,pos))=h	<br>
	 *		force(init(s,l,h,f,v,p,pos))=f <br>
	 *		vitesse(init(s,l,h,f,v,p,pos))=v <br>
	 *		pointsDeVie(init(s,l,h,f,v,p,pos))=p <br>
	 *		quantiteOr(init(s,l,h,f,v,p,pos))=0 <br>
	 *		getX(init(s,l,h,f,v,p,pos)) = pos.x <br>
	 *		getY(init(s,l,h,f,v,p,pos)) = pos.y <br>
	 *		resteDansLaMinePourPasDeJeu(init(s,l,h,f,v,p,pos))=-1<br>
	 * @param race
	 * @param largeur
	 * @param hauteur
	 * @param force
	 * @param vitesse
	 * @param pointsVie
	 * @param position TODO
	 **/
	
	void init(String race,int largeur,int hauteur,int force,double vitesse,int pointsVie, Point position);

	/**
	 * Operators
	 */
	
	/**
	*  pre: <br>
	*		pre retrait(Villageois,s) require !estMort(Villageois) && s>0 <br>
	*<br>
	*  post: <br>
	*  		pointsDeVie(retrait(Villageois,s))=pointsDeVie(Villageois) -s <br>
	*		quantiteOr(retrait(Villageois,s))=quantiteOr(Villageois)<br>
	*		getX(retrait(Villageois,s)) = getX(Villageois); <br>
	*		getY(retrait(Villageois,s)) = getY(Villageois) <br>
	*		resteDansLaMinePourPasDeJeu(retrait(Villageois,s)) = resteDansLaMinePourPasDeJeu(Villageois)
	**/
	void retrait (int s) ;

	/**
	 * 	pre setX(V, x) require x > 0 <br>
	 *	
	 *<br>
	 *post:<br>
	 *		getX(setX(V, x)) = x; <br>
	 *		getY(setX(V, x)) = getY(V) <br>
	 *		pointsDeVie(setX(V, x))= pointsDeVie(V) <br>
	 *		quantiteOr (setX(V, x))=quantiteOr (V)	<br>
	 *		resteDansLaMinePourPasDeJeu(setX(V, x)) = resteDansLaMinePourPasDeJeu(V)
	 */
	
	@Override
	public void setX(int x);
	
	
	/**
	 * 	pre setY(V, y) require y > 0<br>
	 *	
	 *<br>
	 *post:<br>
	 *		getX(setY(V, y)) = getX(V);<br>
	 *		getY(setY(V, y)) = y<br>
	 *		pointsDeVie(setY(V, y))= pointsDeVie(V)<br>
	 *		quantiteOr (setY(V, y))=quantiteOr (V)	<br>
	 *		resteDansLaMinePourPasDeJeu(setY(V, y)) = resteDansLaMinePourPasDeJeu(V)<br>
	 */
	
	
	@Override
	public void setY(int y);


	/**
	 * pre:<br>
	 * 		 pre entrerMine(V) requires !estEnTrainDeMiner(V); <br>
	 * post:<br>
	 * 		resteDansLaMinePourPasDeJeu(entrerMine(V)) = 51<br>	
	 *		getX(entrerMine(V)) = getX(V)<br>
	 *		getY(entrerMine(V)) = getY(V)<br>
	 *		pointsDeVie(entrerMine(V))= pointsDeVie(V)<br>
	 *		quantiteOr (entrerMine(V))=quantiteOr (V)	<br>
	 */
	public void entrerMine();
	

	/**
	 * pre:<br>
	 * 		 pre sortirMine(V) requires estEnTrainDeMiner(V); <br>
	 * post:<br>
	 * 		resteDansLaMinePourPasDeJeu(sortirMine(V)) = 0<br>	
	 *		getX(sortirMine(V)) = getX(V)<br>
	 *		getY(sortirMine(V)) = getY(V)<br>
	 *		pointsDeVie(sortirMine(V))= pointsDeVie(V)<br>
	 *		quantiteOr (sortirMine(V))=quantiteOr (V)	<br>
	 */
	public void sortirMine();
	
	/**
	 * pre:<br>
	 * 		 pre miner(V) requires estEnTrainDeMiner(V); <br>
	 * post:<br>
	 * 		resteDansLaMinePourPasDeJeu(miner(V)) = resteDansLaMinePourPasDeJeu(V)-1<br>	
	 *		getX(miner(V)) = getX(V)<br>
	 *		getY(miner(V)) = getY(V)<br>
	 *		pointsDeVie(miner(V))= pointsDeVie(V)<br>
	 *		quantiteOr (miner(V))=quantiteOr (V)	<br>
	 */
	
	public void miner();

	/**
	 * pre:<br>
	 * 		pre remplirPoches(i) requires i >0<br>
	 * post:
	 * 		resteDansLaMinePourPasDeJeu(remplirPoches(V,i)) = resteDansLaMinePourPasDeJeu(V)<br>	
	 *		getX(remplirPoches(V,i)) = getX(V)<br>
	 *		getY(remplirPoches(V,i)) = getY(V)<br>
	 *		pointsDeVie(remplirPoches(V,i))= pointsDeVie(V)<br>
	 *		quantiteOr (remplirPoches(V,i))=quantiteOr(V)+i	<br>	
	 * @param i
	 */
	public void remplirPoches(int i);

	/**
	 * pre:
	 * 		pre viderPoches(V) requires quantiteOr(V)>0<br>
	 * post:<br>
	 * 		resteDansLaMinePourPasDeJeu(viderPoches(V)) = resteDansLaMinePourPasDeJeu(V)<br>	
	 *		getX(viderPoches(V)) = getX(V)<br>
	 *		getY(viderPoches(V)) = getY(V)<br>
	 *		pointsDeVie(viderPoches(V))= pointsDeVie(V)<br>
	 *		quantiteOr (viderPoches(V))=0	<br>	
	 */
	public void viderPoches();

	
	/**
	 * Invariants
	 */
	
	/**
	 * estMort(V) = (min)pointsDeVie(V) ≤0	<br> 
	 * estEnTrainDeMiner(V) = (min) resteDansLaMinePourPasDeJeu(V) > 0; 
	 */
}
