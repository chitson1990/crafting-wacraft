package services;

import java.util.Set;

import enums.COMMAND;
import enums.RESULTAT;
import requires.IRequireHotelVilleService;
import requires.IRequireTerrainService;
import requires.IRequireVillageoisService;

public interface IMoteurJeuService extends IRequireTerrainService, IRequireVillageoisService, IRequireHotelVilleService{
	
	// Observators
	
	/**
	 * const observator<br>
	 * 
	 * @return
	 */
	int largeurTerrain();
	
	/**
	 * const observator<br>
	 * 
	 * @return
	 */
	int hauteurTerrain();
	
	/**
	 * const observator<br>
	 * 
	 * @return
	 */
	int maxPasJeu();
	
	int pasJeuCourant();
	
	boolean estFini();
	
	/**
	 * \pre:<br>
	 * estFini(M)<br>
	 * 
	 * @return
	 */
	RESULTAT resultatFinal();
	
	/**
	 * const observator<br>
	 * 
	 * @return
	 */
	ITerrainJeuxService terrain();
	
	/**
	 * const observator<br>
	 * 
	 * @return
	 */
	Set<Integer> numerosVillageois();
	
	/**
	 * \pre:<br>
	 * numero ∈ numerosVillageois(M)<br>
	 * 
	 * @param i
	 * @return
	 */
	IVillageoisService getVillageois(int numero);
	
	/**
	 * \pre:<br>
	 * numero ∈ numerosVillageois(M)<br>
	 * 
	 * @param numero
	 * @return
	 */
	int positionVillageoisX(int numero);
	
	/**
	 * \pre:<br>
	 * numero ∈ numerosVillageois(M)<br>
	 * 
	 * @param numero
	 * @return
	 */
	int positionVillageoisY(int numero);
	
	Set<Integer> numerosMines();
	
	/**
	 * \pre:<br>
	 * numero ∈ numerosMines(M)<br>
	 * 
	 * @param numero
	 * @return
	 */
	IMineService getMine(int numero);
	
	/**
	 * \pre:<br>
	 * numero ∈ numerosMines(M)<br>
	 * 
	 * @param numero
	 * @return
	 */
	int positionMineX(int numero);
	
	/**
	 * \pre:<br>
	 * numero ∈ numerosMines(M)<br>
	 * 
	 * @param numero
	 * @return
	 */
	int positionMineY(int numero);
	
	/**
	 * const observator
	 * 
	 * @return
	 */
	Set<Integer> numerosMurailles();
	
	/**
	 * \pre:<br>
	 * numeroMuraille ∈ numerosMurailles(M)
	 * 
	 * @param numeroMuraille
	 * @return
	 */
	IMurrailleService getMuraille(int numeroMuraille);
	
	/**
	 * \pre:<br>
	 * numeroMuraille ∈ numerosMurailles(M)
	 * 
	 * @param numeroMuraille
	 * @return
	 */
	int positionMurailleX(int numeroMuraille);
	
	/**
	 * \pre:<br>
	 * numeroMuraille ∈ numerosMurailles(M)
	 * 
	 * @param numeroMuraille
	 * @return
	 */
	int positionMurailleY(int numeroMuraille);
	
	IHotelVilleService hotelDeVille();
	
	/**
	 * const observator<br>
	 * 
	 * @return
	 */
	int positionHotelVilleX();
	
	/**
	 * const observator<br>
	 * 
	 * @return
	 */
	int positionHotelVilleY();
	
	/**
	 * \pre:<br>
	 * numeroVillageois ∈ numerosVillageois(M) ∧ numeroMine ∈ numerosMines(M)<br>
	 * 
	 * @param numeroVillageois
	 * @param numeroMine
	 * @return
	 */
	boolean peutEntrerMine(int numeroVillageois, int numeroMine);
	
	/**
	 * \pre:<br>
	 * numeroVillageois ∈ numerosVillageois(M)<br>
	 * 
	 * @param numeroVillageois
	 * @return
	 */
	boolean peutEntrerHotelVille(int numeroVillageois);
	
	/**
	 * \pre:<br>
	 * numeroVillageois ∈ numerosVillageois(M) ∧ numeroMuraille ∈ numerosMurailles(M)<br>
	 * 
	 * @param numeroVilageois
	 * @param numeroMuraille
	 * @return
	 */
	boolean peutAttaquerMuraille(int numeroVilageois, int numeroMuraille);

	// Constructors
	
	/**
	 * \pre:<br>
	 * maxPasJeu >= 0<br><br>
	 * 
	 * \post:<br>
	 * maxPasJeu(init(maxPasJeu)) == maxPasJeu<br>
	 * si maxPasJeu == 0 alors estFini(init(maxPasJeu))<br>
	 * sinon !estFini(init(maxPasJeu))<br>
	 * 
	 * @param largeur
	 * @param hauteur
	 * @param maxPasJeu
	 */
	void init(int maxPasJeu);

	// Operators
	
	/**
	 * \pre:<br>
	 * pre pasJeu(M, commande, numVillageois, argument) require
			!estFini(M)
			si commande = DEPLACER alors
				numVillageois ∈ numerosVillageois(M) && 0 <= argument <= 360

				// déplacement droite
				si 0 <= argument < 45 || 315 < argument <= 360 alors
					positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) < largeur(M)
					
					si il exite i ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
					
						si positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois) + 2 > largeur(M)
							∀i ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
								ITerrainJeuService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 1, i) == RIEN || ROUTE
						
						sinon ∀i ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
									ITerrainJeuService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 1, i) == RIEN || ROUTE
								&& ∀i ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
										ITerrainJeuService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 2, i) == RIEN || ROUTE
					
					sinon ∀i ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
								ITerrainJeuService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 1, i) == RIEN || ROUTE

				// déplacement diagonale haute droite
				si arg = 45 alors
					positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) < largeur(M) && positionVillageoisY(M, numVillageois) > 0
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						si positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 2 > largeur(M) || positionVillageoisY(M, numVillageois) - 2 < 0
							∀i ∈ [positionVillageoisX(M, numVillageois) + 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois) + 1]
								ITerrainJeuService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 1) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisY(M, numVillageois) - 1...positionVillageoisY(M, numVillageois) - 1 + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
								ITerrainJeuService::getObjectAt(terrain(M), positionVillageoisX(numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 1, j) == RIEN || ROUTE
							
						sinon ∀i ∈ [positionVillageoisX(M, numVillageois) + 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois) + 1]
								ITerrainJeuService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 1) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisY(M, numVillageois) - 1...positionVillageoisY(M, numVillageois) - 1 + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
								ITerrainJeuService::getObjectAt(terrain(M), positionVillageoisX(numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 1, j) == RIEN || ROUTE
							
							&& ∀i ∈ [positionVillageoisX(M, numVillageois) + 2...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois) + 2]
								ITerrainJeuService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 2) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisY(M, numVillageois) - 2...positionVillageoisY(M, numVillageois) - 2 + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
								ITerrainJeuService::getObjectAt(terrain(M), positionVillageoisX(numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 2, j) == RIEN || ROUTE
					
					sinon
						∀i ∈ [positionVillageoisX(M, numVillageois) + 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois) + 1]
							ITerrainJeuService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 1) == RIEN || ROUTE
						&& ∀j ∈ [positionVillageoisY(M, numVillageois) - 1...positionVillageoisY(M, numVillageois) - 1 + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
							ITerrainJeuService::getObjectAt(terrain(M), positionVillageoisX(numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 1, j) == RIEN || ROUTE

				// déplacement haut
				si 45 < arg < 135 alors
					positionVillageoisY(M, numVillageois) > 0
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						si positionVillageoisY(M, numVillageois) - 2 < 0
							∀i ∈ [positionVillageoisX(numVillageois)...IVillageoisService::largeur(getVillageois(M, numVillageois))]
								ITerrainJeuService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 1) == RIEN || ROUTE
						
						sinon
							∀i ∈ [positionVillageoisX(numVillageois)...IVillageoisService::largeur(getVillageois(M, numVillageois))]
								ITerrainJeuService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 1) == RIEN || ROUTE
							&& ∀i ∈ [positionVillageoisX(numVillageois)...IVillageoisService::largeur(getVillageois(M, numVillageois))]
								ITerrainJeuService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 2) == RIEN || ROUTE
					
					sinon
						∀i ∈ [positionVillageoisX(numVillageois)...IVillageoisService::largeur(getVillageois(M, numVillageois))]
							ITerrainJeuService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 1) == RIEN || ROUTE

				// déplacement diagonale haute gauche
				si arg = 135
					positionVillageoisY(M, numVillageois) > 0 && positionVillageoisX(M, numVillageois) > 0
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois))]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						si positionVillageoisY(M, numVillageois) - 2 < 0 || positionVillageoisX(M, numVillageois) - 2 < 0
							∀i ∈ [positionVillageoisX(M, numVillageois) - 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) - 1]
								ITerrainJeuxService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 1) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisY(M, numVillageois) - 1...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) - 1]
								ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 1, j) == RIEN || ROUTE
						
						sinon
							∀i ∈ [positionVillageoisX(M, numVillageois) - 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) - 1]
								ITerrainJeuxService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 1) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisY(M, numVillageois) - 1...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) - 1]
								ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 1, j) == RIEN || ROUTE
							&& ∀i ∈ [positionVillageoisX(M, numVillageois) - 2...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) - 2]
								ITerrainJeuxService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 2) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisY(M, numVillageois) - 2...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) - 2]
								ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 2, j) == RIEN || ROUTE
					
					sinon
						∀i ∈ [positionVillageoisX(M, numVillageois) - 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) - 1]
							ITerrainJeuxService::getObjectAt(terrain(M), i, positionVillageoisY(M, numVillageois) - 1) == RIEN || ROUTE
						&& ∀j ∈ [positionVillageoisY(M, numVillageois) - 1...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) - 1]
							ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 1, j) == RIEN || ROUTE

				// déplacement gauche
				si 135 < arg < 225 
					positionVillageoisX(M, numVillageois) > 0
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois))]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						si positionVillageoisX(M, numVillageois) - 2 < 0
							∀i ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
								ITerrainJeuxService::getObjectAt(positionVillageoisX(M, numVillageois) - 1, i) == RIEN || ROUTE
						
						sinon
							∀i ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
								ITerrainJeuxService::getObjectAt(positionVillageoisX(M, numVillageois) - 1, i) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
								ITerrainJeuxService::getObjectAt(positionVillageoisX(M, numVillageois) - 2, j) == RIEN || ROUTE
					
					sinon
						∀i ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
							ITerrainJeuxService::getObjectAt(positionVillageoisX(M, numVillageois) - 1, i) == RIEN || ROUTE
				
				// déplacement diagonale bas gauche
				si arg = 225
					positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) < ITerrainJeuxService::hauteur(terrain(M)) && positionVillageoisX(M, numVillageois) > 0
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois))]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						si positionVillageoisX(M, numVillageois) - 2 < 0 || positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) + 2 > ITerrainJeuxService::hauteur(terrain(M)) alors
							∀i ∈ [positionVillageoisY(M, numVillageois) - 1...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) - 1]
								ITerrainJeuxService::getObjectAt(positionVillageoisX(M, numVillageois) - 1, i) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisX(M, numVillageois) - 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) - 1]
								ITerrainJeuxService::getObjectAt(j, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) + 1) == RIEN || ROUTE
						
						sinon
							∀i ∈ [positionVillageoisY(M, numVillageois) - 1...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) - 1]
								ITerrainJeuxService::getObjectAt(positionVillageoisX(M, numVillageois) - 1, i) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisX(M, numVillageois) - 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) - 1]
								ITerrainJeuxService::getObjectAt(j, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) + 1) == RIEN || ROUTE
							&& ∀i ∈ [positionVillageoisY(M, numVillageois) - 2...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) - 2]
								ITerrainJeuxService::getObjectAt(positionVillageoisX(M, numVillageois) - 2, i) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisX(M, numVillageois) - 2...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) - 2]
								ITerrainJeuxService::getObjectAt(j, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) + 2) == RIEN || ROUTE
					
					sinon
						∀i ∈ [positionVillageoisY(M, numVillageois) - 1...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) - 1]
							ITerrainJeuxService::getObjectAt(positionVillageoisX(M, numVillageois) - 1, i) == RIEN || ROUTE
						&& ∀j ∈ [positionVillageoisX(M, numVillageois) - 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) - 1]
							ITerrainJeuxService::getObjectAt(j, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) + 1) == RIEN || ROUTE
				
				// déplacement bas
				si 225 < arg < 315
					positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) < ITerrainJeuxService::hauteur(terrain(M))
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois))]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						si positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois) + 2 > ITerrainJeuxService::hauteur(terrain(M)) alors
							∀i ∈ [positionVillageoisX(M, numVillageois)...[positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois))]
								ITerrainJeuxService::getObjectAt(i, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) + 1) == RIEN || ROUTE
						
						sinon
							∀i ∈ [positionVillageoisX(M, numVillageois)...[positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois))]
								ITerrainJeuxService::getObjectAt(i, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) + 1) == RIEN || ROUTE
							&& ∀i ∈ [positionVillageoisX(M, numVillageois)...[positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois))]
								ITerrainJeuxService::getObjectAt(i, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) + 2) == RIEN || ROUTE
					
					sinon
						∀i ∈ [positionVillageoisX(M, numVillageois)...[positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois))]
							ITerrainJeuxService::getObjectAt(i, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) + 1) == RIEN || ROUTE
					
				// déplacement diagonale bas droite
				si arg = 315
					positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)) < ITerrainJeuxService::hauteur(terrain(M)) && positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) < largeur(M)
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois))]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois))]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						si positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois) + 2 > ITerrainJeuxService::hauteur(terrain(M)) || positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois) + 2 > largeur(M) alors
							∀i ∈ [positionVillageoisX(M, numVillageois) + 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 1]
								ITerrainJeuxService::getObjectAt(i, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois) + 1) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisY(M, numVillageois) + 1...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois) + 1]
								ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 1, j) == RIEN || ROUTE
						
						sinon
							∀i ∈ [positionVillageoisX(M, numVillageois) + 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 1]
								ITerrainJeuxService::getObjectAt(i, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois) + 1) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisY(M, numVillageois) + 1...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois) + 1]
								ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 1, j) == RIEN || ROUTE
							&& ∀i ∈ [positionVillageoisX(M, numVillageois) + 2...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 2]
								ITerrainJeuxService::getObjectAt(i, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois) + 2) == RIEN || ROUTE
							&& ∀j ∈ [positionVillageoisY(M, numVillageois) + 2...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois) + 2]
								ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 2, j) == RIEN || ROUTE
					
					sinon
						∀i ∈ [positionVillageoisX(M, numVillageois) + 1...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 1]
							ITerrainJeuxService::getObjectAt(i, positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois) + 1) == RIEN || ROUTE
						&& ∀j ∈ [positionVillageoisY(M, numVillageois) + 1...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois) + 1]
							ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)) + 1, j) == RIEN || ROUTE

			si commande = ENTRERMINE
				alors numVillageois ∈ numerosVillageois(M) && argument ∈ numerosMines(M) && peutEntrerMine(M, numVillageois, argument) && !IMineService::estLaminee(getMine(M, argument))
			
			si commande = ENTRERHOTELVILLE
				alors numVillageois ∈ numerosVillageois(M) && argument ∈ numerosMines(M) && peutEntrerHotelVille(M, numVillageois, argument)
			
			si commande = ATTAQUERMURAILLE
				alors numVillageois ∈ numerosVillageois(M) && argument ∈ numerosMurailles(M) && peutAttaquerMuraille(M, numVillageois, argument)
				
	 * \post:<br>
	 * 	pasJeuCourant(pasJeu(M, c, numVillageois, arg)) = pasJeuCourant(M) + 1
			
		// les Villageois qui étaient en train de miner ont continuer leur minage		
		∀i ∈ numerosVillageois(M)
			si IVillageoisService::isMining(getVillageois(M, i))
				IVillageoisService::compteurMinage(getVillageois(pasJeu(M, c, numVillageois, arg), i)) = IVillageoisService::compteurMinage(getVillageois(M, i)) + 1
		
		si c = DEPLACER
			// les positions des Villageois, sauf celui passé en paramètre, sont inchangées
			∀i ∈ numerosVillageois(M) \ numVillageois
				positionVillageoisX(pasJeu(M, c, numVillageois, arg), i) = positionVillageoisX(M, i)
				&& positionVillageoisY(pasJeu(M, c, numVillageois, arg), i) = positionVillageoisY(M, i)
			
			// les case du terrain sont inchangées, sauf celles correspondants au zones de départ et d'arrivée du Villageois
			∀i ∈ [0...largeur(terrain(M))] \ {positionVillageoisX(M, numVillageois)...IVillageoisService::largeur(getVillageois(M, numVillageois)) && positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois)...IVillageoisService::largeur(getVillageois(pasJeu(M, c, numVillageois, arg), numVillageois))}
				∀j ∈ [0...hauteur(terrain(M))] \ {positionVillageoisY(M, numVillageois)...IVillageoisService::hauteur(getVillageois(M, numVillageois)) && positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois)...IVillageoisService::hauteur(getVillageois(pasJeu(M, c, numVillageois, arg), numVillageois))}
					ITerrainJeuxService::getObjectAt(terrain(pasJeu(M, c, numVillageois, arg)), i, j) = ITerrainJeuxService::getObjectAt(terrain(M), i, j)
			
			// si les cases couverte par le Villageois à l'arrivée avaient pour valeur ROUTE, elles deviennent des VILLAGEOISSURROUTE, sinon, elles sont devenues des VILLAGEOIS
			∀i ∈ [positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois)...positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) + IVillageoisService::largeur(getVillageois(pasJeu(M, c, numVillageois, arg), numVillageois))]
				∀j ∈ [positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois)...positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) + IVillageoisService::hauteur(getVillageois(pasJeu(M, c, numVillageois, arg), numVillageois))]
					si ITerrainJeuxService::getObjectAt(terrain(M), i, j) = ROUTE alors
						ITerrainJeuxService::getObjectAt(terrain(pasJeu(M, c, numVillageois, arg)), i, j) = VILLAGEOISSURROUTE
					sinon ITerrainJeuxService::getObjectAt(terrain(pasJeu(M, c, numVillageois, arg)), i, j) = VILLAGEOIS
			
			// déplacement droite
			si 0 <= arg < 45 || 315 < arg <= 360
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						
						// déplacement de deux unités à cause de la route
						si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 1, positionVillageoisY(M, numVillageois)) = ROUTE || RIEN && ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 2, positionVillageoisY(M, numVillageois)) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) + 2
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois)
							
						// déplacement normal (une seule unité)	
						sinon si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 1, positionVillageoisY(M, numVillageois)) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) + 1
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois)
				
				sinon
					positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) + 1
					positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois)
			
			// déplacement diagonale haut droite
			si arg = 45
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						
						// déplacement de deux unités à cause de la route
						si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 1, positionVillageoisY(M, numVillageois) - 1) = ROUTE || RIEN && ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 2, positionVillageoisY(M, numVillageois) - 2) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) + 2
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) - 2
							
						// déplacement normal (une seule unité)	
						sinon si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 1, positionVillageoisY(M, numVillageois) - 1) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) + 1
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) - 1
							
				sinon
					positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) + 1
					positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) - 1
			
			// déplacement haut
			si 45 < arg < 135
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						
						// déplacement de deux unités à cause de la route
						si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois), positionVillageoisY(M, numVillageois) - 1) = ROUTE || RIEN && ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois), positionVillageoisY(M, numVillageois) - 2) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois)
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) - 2
							
						// déplacement normal (une seule unité)	
						sinon si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois), positionVillageoisY(M, numVillageois) - 1) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois)
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) - 1
							
				sinon
					positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois)
					positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) - 1
			
			// déplacement diagonale haut gauche
			si arg = 135
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						
						// déplacement de deux unités à cause de la route
						si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 1, positionVillageoisY(M, numVillageois) - 1) = ROUTE || RIEN && ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 2, positionVillageoisY(M, numVillageois) - 2) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) - 2
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) - 2
							
						// déplacement normal (une seule unité)	
						sinon si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois), positionVillageoisY(M, numVillageois) - 1) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) - 1
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) - 1
							
				sinon
					positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) - 1
					positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) - 1
			
			// déplacement gauche
			si 135 < arg < 225
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						
						// déplacement de deux unités à cause de la route
						si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 1, positionVillageoisY(M, numVillageois)) = ROUTE || RIEN && ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 2, positionVillageoisY(M, numVillageois)) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) - 2
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois)
							
						// déplacement normal (une seule unité)	
						sinon si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 1, positionVillageoisY(M, numVillageois)) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) - 1
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois)
							
				sinon
					positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) - 1
					positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois)
			
			// déplacement diagonale bas gauche
			si arg = 225
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						
						// déplacement de deux unités à cause de la route
						si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 1, positionVillageoisY(M, numVillageois) + 1) = ROUTE || RIEN && ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 2, positionVillageoisY(M, numVillageois) + 2) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) - 2
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) + 2
							
						// déplacement normal (une seule unité)	
						sinon si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) - 1, positionVillageoisY(M, numVillageois) + 1) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) - 1
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) + 1
							
				sinon
					positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) - 1
					positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) + 1
			
			// déplacement bas
			si 225 < arg < 315
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						
						// déplacement de deux unités à cause de la route
						si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois), positionVillageoisY(M, numVillageois) + 1) = ROUTE || RIEN && ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois), positionVillageoisY(M, numVillageois) + 2) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois)
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) + 2
							
						// déplacement normal (une seule unité)	
						sinon si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois), positionVillageoisY(M, numVillageois) + 1) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois)
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) + 1
							
				sinon
					positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois)
					positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) + 1
			
			// déplacement diagonale bas droite
			si arg = 315
					
					si il exite i  ∈ [positionVillageoisX(M, numVillageois)...positionVillageoisX(M, numVillageois) + IVillageoisService::largeur(getVillageois(M, numVillageois)]
					&& il existe j ∈ [positionVillageoisY(M, numVillageois)...positionVillageoisY(M, numVillageois) + IVillageoisService::hauteur(getVillageois(M, numVillageois)]
					tel que ITerrainJeuService::getObjectAt(terrain(M), i, j) == OBJECTS.VILLAGEOISSURROUTE alors
						
						// déplacement de deux unités à cause de la route
						si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 1, positionVillageoisY(M, numVillageois) + 1) = ROUTE || RIEN && ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 2, positionVillageoisY(M, numVillageois) + 2) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) + 2
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) + 2
							
						// déplacement normal (une seule unité)	
						sinon si ITerrainJeuxService::getObjectAt(terrain(M), positionVillageoisX(M, numVillageois) + 1, positionVillageoisY(M, numVillageois) + 1) = ROUTE || RIEN
							positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) + 1
							positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) + 1
							
				sinon
					positionVillageoisX(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisX(M, numVillageois) + 1
					positionVillageoisY(pasJeu(M, c, numVillageois, arg), numVillageois) = positionVillageoisY(M, numVillageois) + 1
		
		si c = ENTRERMINE
			// les positions des Villageois, sont inchangées
			∀i ∈ numerosVillageois(M)
				positionVillageoisX(pasJeu(M, c, numVillageois, arg), i) = positionVillageoisX(M, i)
				&& positionVillageoisY(pasJeu(M, c, numVillageois, arg), i) = positionVillageoisY(M, i)
			
			// les case du terrain sont inchangées
			∀i ∈ [0...largeur(terrain(M))]
				∀j ∈ [0...hauteur(terrain(M))]
					ITerrainJeuxService::getObjectAt(terrain(pasJeu(M, c, numVillageois, arg)), i, j) = ITerrainJeuxService::getObjectAt(terrain(M), i, j)
			
			∀i ∈ numerosMines(M) \ arg, getMine(pasJeu(M, c, numVillageois, i)) = Mine::abandoned(getMine(M, i))
			getMine(pasJeu(M, c, numVillageois, arg)) = Mine::acceuil(getMine(M, arg))
			
		si c = ENTRERHOTELVILLE
			// les positions des Villageois, sont inchangées
			∀i ∈ numerosVillageois(M)
				positionVillageoisX(pasJeu(M, c, numVillageois, arg), i) = positionVillageoisX(M, i)
				&& positionVillageoisY(pasJeu(M, c, numVillageois, arg), i) = positionVillageoisY(M, i)
			
			// les case du terrain sont inchangées
			∀i ∈ [0...largeur(terrain(M))]
				∀j ∈ [0...hauteur(terrain(M))]
					ITerrainJeuxService::getObjectAt(terrain(pasJeu(M, c, numVillageois, arg)), i, j) = ITerrainJeuxService::getObjectAt(terrain(M), i, j)
			
			IHotelVilleService::quantiteDor(hotelVille(pasJeu(M, c, numVillageois, arg))) = IHotelVilleService::quantiteDor(hotelVille(M)) + IVillageoisService::quantiteOr(getVillageois(M, numVillageois))
			IVillageoisService::quantiteOr(getVillageois(pasJeu(M, c, numVillageois, arg), numVillageois)) = 0
		
		si c = ATTAQUERMURAILLE
			// les positions des Villageois, sont inchangées
			∀i ∈ numerosVillageois(M)
				positionVillageoisX(pasJeu(M, c, numVillageois, arg), i) = positionVillageoisX(M, i)
				&& positionVillageoisY(pasJeu(M, c, numVillageois, arg), i) = positionVillageoisY(M, i)
			
			// les case du terrain sauf celles de la Muraille sont inchangées
			∀i ∈ [0...largeur(terrain(M))] \ {positionMurailleX(M)...IMurailleService::largeur(getMuraille(M, arg))}
				∀j ∈ [0...hauteur(terrain(M))] \ {positionMurailleY(M)...IMurailleService::hauteur(getMuraille(M, arg))}
					ITerrainJeuxService::getObjectAt(terrain(pasJeu(M, c, numVillageois, arg)), i, j) = ITerrainJeuxService::getObjectAt(terrain(M), i, j)
			
			∀i ∈ numerosMurailles(M) \ arg, IMurrailleService::pointDeVie(getMuraille(pasJeu(M, c, numVillageois, arg), arg)) = IMurrailleService::pointDeVie(getMuraille(M, arg))
		
			si IMurrailleService::pointDeVie(getMuraille(M, arg)) <= IVillageoisService::force(getVillageois(M, numVillageois)) alors
				ITerrainJeuService::getObjectAt(terrain(pasJeu(M, c, numVillageois, arg)), positionMurailleX(M, arg), positionMurailleY(M, arg)) = RIEN
				∀i ∈ numerosMuraille(pasJeu(M, c, numVillageois, arg)), i != arg
			sinon IMurrailleService::pointDeVie(getMuraille(pasJeu(M, c, numVillageois, arg), arg)) = IMurrailleService::pointDeVie(getMuraille(M, arg)) - IVillageoisService::force(getVillageois(M, numVillageois))
		 */
	void pasJeu(COMMAND commande, int numVillageois, int argument);
}