package services;

import java.awt.Point;

import enums.RACE;

public interface IMineService extends IMyObjectService{
	/**
	 * Observators
	 */
	/**
	 *  Largeur
	 * @return int 
	 */
	
	/**
	 * la race d'un villageois
	 * @return RACE
	 */
	public RACE appartientA();
	
	
	@Override
	public int largeur();
	/**
	 *	 Hauteur
	 * @return int
	 */
	@Override
	public int hauteur();

	/**
	 * Quantite d'or restant
	 * @return int
	 */
	public int orRestant();
	
	/**
	 * Tester si la mine est abandonner
	 * @return boolean
	 */
	public boolean estAbandonnee();

	/**
	 * Tester si la mine peut etre visiter
	 * @return boolean
	 */
	public boolean peutAcceuil();
	
	/**
	 * Tester si la mine est lamine
	 * @return boolean
	 */
	public boolean estLaminee();

	/**
	 * Compteur de pas jeu pour indiquer s la mine peut etre abandonner <br>
	 * apres 51 pas du jeu.
	 * @return int
	 */
	public int abandonCompteur();

	/**
	 * Constructors
	 */

	/**
	 * pre:<br>
	 * 		pre init(largeur,hauteur,pos,race) require race <> null ^ largeur%2=1 ∧ hauteur%2=1 ^ pos.x >0 ^ pos.y >0<br>
	 *
	 *<br>
	 * post:<br>
	 * 		largeur(init(l,h,pos,race))=l <br>
	 * 		hauteur(init(l,h,pos,race))=h <br>
	 * 		orRestant(init(l,h,pos,race))=51 <br>
	 * 		abandonCompteur(init(l,h,pos,race))=51 <br>
	 * 		getY(init(l,h,pos,race)) = pos.y<br>
	 * 		getX(init(l,h,pos,race)) = pos.x<br>
	 * 		appartientA(init(l,h,pos,race))= race <br>
	 * @param largeur
	 * @param hauteur
	 * @param position TODO
	 * @param race TODO
	 * 
	 */
	
	void init(int largeur, int hauteur, Point position, RACE race);

	/**
	 * Operators
	 */
	
	/**
	 * pre: <br>
	 *		pre retrait(M,s) require ¬estLaminee(M) ∧ 0< s <= orRestant(M)  <br>
	 *<br>
	 * Post: <br>
	 * 		orRestant(retrait(M,s))=orRestant(M)-s <br>
	 * 		abandonCompteur(retrait(M,s))=abandonCompteur(M) <br>
	 * 		getX(retrai(M,s)) = getX(H)<br>
	 * 		getY(retrait(M,s)) = getY(H)<br>
	 * @param s
	 */
	
	void retrait(int s);
	
	/**
	 *pre: <br>
		pre acceuil(M) require abandoned(M) <br>
	 * <br>
	 * Post: <br>
	 * 		orRestant(acceuil(M))=orRestant(M) <br>
	 * 		abandonCompteur(accueil(M))=0 <br>
	 * 		getX(acceuil(M)) = getX(H)<br>
	 * 		getY(acceuil(M)) = getY(H)<br>
	 * 
	 */
	void acceuil();
			
	
	/**
	 * Pre: <br>
	 * 		pre abandoned(M) require ¬estAbandonne(M)<br>	
	 * <br>
	 * Post: <br>
	 * 		orRestant(abandoned(M))=orRestant(M) <br>
	 *		abandonCompteur(abandoned(M))=abandonCompteur(M)+1<br>
	 *		getX(abandoned(M)) = getX(H)<br>
	 *		getY(abandoned(M)) = getY(H) <br>
	 * 
	 */
	void abandoned();
	
	
	/**
	 * pre:<br> 
	 *		pre setX(M,x) require x>0 <br>
	 *<br>
	 * Post:<br>
	 * 		orRestant(setX(M))=orRestant(M) <br>
	 * 		abandonCompteur(setX(M))=abandonCompteur(M)<br>
	 * 		getX(setX(M,x)) = x <br>
	 * 		getY(setX(M,x)) = getY(M) <br>
	 * @param x
	 */
	@Override
	public void setX(int x); 
	
	
	/**
	 * pre:<br> 
	 *		pre setY(M,y) require y>0 <br>
	 *<br>
	 * Post:<br>
	 * 		orRestant(setY(M))=orRestant(M) <br>
	 * 		abandonCompteur(setY(M))=abandonCompteur(M)<br>
	 * 		getX(setY(M,y)) = getX(M) <br>
	 * 		getY(sety(M,y)) = y <br>
	 * @param y
	 */
	@Override
	public void setY(int y); 
	
	
	/**
	 * Invriant: <br>
	 * 	estLaminee(M) =(min) orRestant(M) ≤ 0 <br>
	 *	peutAccueil(M) = (min) abandonCompteur=0 <br>
 	 *	estAbandonnee(M) =(min) abandonCompteur = 51 <br>
	 *	0 ≤abandonCompteur(M)≤ 51 <br>
	 * 
	 */
	
}
