package services;

import java.awt.Point;


//TODO
public interface IRouteService extends IMyObjectService {
	/**
	 * Observators
	 */
	
	/**
	 * hauteur de la route
	 * @return int
	 */
	@Override
	public int hauteur();
	
	/**
	 * largeur d'une route 
	 * @return int
	 */
	@Override
	public int largeur();
	
	/**
	 * Une Route passe par un tel point.<br>
	 * pre:<br>
	 * 		pre passAt(x,y) requires x>0 ^ y>0 <br>
	 * @param x
	 * @param y
	 * @return boolean
	 */
	
	public boolean passAt(int x, int y);
	
	/**
	 * Constructors
	 */
	
	/**
	 * pre:<br>
	 * 		pre init(position, Length, hauteur) requires position <> null ^ position.x > 0^ position.y > 0^ largeur >0 ^ hauteur>0 <br>
	 * post:<br>
	 * 		getX(init(p,l,h)) = p.x<br>
	 * 		getY(init(p,l,h)) = p.y<br>
	 * 		hauteur(init(p,l,h))=h; <br>
	 * 		longuer(init(p,l,h))=l; <br>
	 * @param maxLength
	 */
	public void init (Point position, int largeur, int hauteur);
	
	
	
	/**
	 * Operators
	 */
	
	

	/**
	 * pre:
	 * 	pre setY(R, x) require x > 0 <br>
	 *	
	 *<br>
	 *post:<br>
	 *		getX(setY(R, x)) = x; <br>
	 *		getY(setY(R, x)) = getY(R) <br>
	 */
	@Override
	public void setX(int x);
	
	
	/**
	 * pre:
	 * 	pre setY(R, y) require y > 0 <br>
	 *	
	 *<br>
	 *post:<br>
	 *		getX(setY(R, y)) = getX(R); <br>
	 *		getY(setY(R, y)) = y <br>
	 */
	
	
	@Override
	public void setY(int y);


	/**
	 * Invariants
	 */
}
