package services;

import java.awt.Point;

import enums.RACE;

public interface IHotelVilleService extends IMyObjectService{
	/**
	 * Observators
	 */
	
	/**
	 * la race d'un villageois
	 * @return RACE
	 */
	public RACE appartientA();
	
	/**
	 *  Largeur
	 * @return int 
	 */
	@Override
	public int largeur();
	/**
	 *	 Hauteur
	 * @return int
	 */
	@Override
	public int hauteur();

	int quantityDor();

	/**
	 * Tester si l'hotel est abandonner
	 * @return boolean
	 */
	public boolean estAbandonnee();

	/**
	 * Tester si l'hotel est lamine
	 * @return boolean
	 */
	public boolean estLaminee();

	/**
	 * Compteur de pas jeu pour indiquer si l'hotel peut etre abandonner <br>
	 * apres 51 pas du jeu.
	 * @return int
	 */
	public int abandonCompteur();

	/**
	 * Constructors
	 */

	/**
	 * pre: <br>
	 * 	pre init(largeur,hauteur,pos,race) require pos.x >0 ^ pos.y >0 ^ race <> null ^ largeur%2=1 ∧ hauteur%2=1 <br>
	 * <br>
	 * post: <br>
	 * 		largeur(init(l,h,pos,r))=l <br>
	 * 		hauteur(init(l,h,pos,r))=h <br>
	 * 		quantiteDor(init(l,h,pos,r))=16 <br>
	 * 		estAbandonne(init(l,h,pos,r))=true <br>
	 * 		getX(init(l,h,pos,r)) = pos.x
	 * 		getY(init(l,h,pos,r)) = pos.y
	 * 		appartientA(getX(init(l,h,pos,r)))= r;
	 * @param position TODO
	 * 
	 */
	
	void init(int largeur, int hauteur, Point position,RACE race);

	/**
	 * Operators
	 */
	
	/**
	 * pre:<br> 
	 *		pre depot(H,s) require s>0 <br>
	 *<br>
	 * Post:<br>
	 * 		quantityDor(depot(H,s))=quantityDor(H)+s<br>
	 * 		estAbandonne(depot(H,s))=false <br>
	 * 		getX(depot(H,s)) = getX(H) <br>
	 * 		getY(depot(H,s)) = getY(H) <br>
	 */
	
	void depot(int s);
	
	
	/**
	 * pre:<br> 
	 *		pre setX(H,x) require x>0 <br>
	 *<br>
	 * Post:<br>
	 * 		quantityDor(setX(H,x))=quantityDor(H)<br>
	 * 		estAbandonne(setX(H,x))=estAbandonne(H) <br>
	 * 		getX(setX(H,x)) = x <br>
	 * 		getY(setX(H,x)) = getY(H) <br>
	 */
	@Override
	public void setX(int x); 
	
	
	/**
	 * pre:<br> 
	 *		pre setY(H,y) require y>0 <br>
	 *<br>
	 * Post:<br>
	 * 		quantityDor(setY(H,y))=quantityDor(H)<br>
	 * 		estAbandonne(setY(H,y))=estAbandonne(H) <br>
	 * 		getX(setY(H,y)) = y <br>
	 * 		getY(setY(H,y)) = getY(H) <br>
	 */
	@Override
	public void setY(int y); 
	
	
	/**
	 * Invriant: <br>
	 * 	estLaminee(H) =(min) quantityDor(H) ≤ 0 <br>
	 * 
	 */
	
	
}
