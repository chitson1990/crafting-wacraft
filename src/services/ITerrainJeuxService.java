package services;

import java.util.Set;

import enums.OBJECTS;
import requires.IRequireHotelVilleService;
import requires.IRequireMineService;
import requires.IRequireMuraillesService;
import requires.IRequireRouteService;

public interface ITerrainJeuxService extends IRequireHotelVilleService,
		IRequireMineService, IRequireMuraillesService, IRequireRouteService {
	/**
	 * observateurs de la largeur du terrain
	 * @return int
	 */
	public int largeur();
	/**
	 * 
	 * @param numero
	 * @return IMineService
	 */
	public IMineService getMine(int numero);

	/**
	 * 
	 * @return Set
	 */
	public Set<Integer> numerosMurailles(); 
	
	/**
	 * 
	 * @return Set<Integer>
	 */
	public Set<Integer> numerosMines();

	/**
	 * 
	 * @return int
	 */
	public int nbMurailles(); 
	/**
	 * Observateor
	 * pre:
	 * 		pre getMuraille(T,id) requires id exists in numerosMurailles(T)
	 * @return IMurrailleService
	 */
	public IMurrailleService getMuraille(int id);
	
	
	/**
	 * observateurs de l'hauteur du terrain
	 * @return int
	 */
	public int hauteur();
	
	/**
	 * pre:<br>
	 * 		pre getObjectAt(x,y) requires 0<= x < largeur() ^ 0<= y <hauteur()
	 * <br>
	 * <br>
	 * Savoir quel objet se trouvant a un tel coordonner
	 * @return String
	 */
	
	public OBJECTS getObjectAt(int x, int y);
	//TODO
	/**
	 * pre:<br>
	 * 		pre setObjectAt(x,y,I) requires 0<= x < largeur() ^ 0<= y <hauteur() ^ o <> null ^ OBJECTS::myType(o) in {VILLAGEOIS, MINE,HOTELVILLE, ROUTE, MURAILLE}
	 * <br>
	 * post:
	 * 		numerosMurailles(setObjectAt(x,y,o)) = numerosMurailles(T)<br>
	 * 		pour tous i = 0 et i < OBJECT:largeur(o)<br>
	 * 			pour tous j =0 et j < OBJECT:hauteur(o)<br>
	 * 				getObjectAt(setObjectAt(x,y,o),x+i,y+j)== OBJECT::myType(o)<br>
	 * 		
	 * <br>
	 * <br>
	 * Set quel objet se trouvant a un tel coordonner
	 * @return void
	 */
	
	public void setObjectAt(int x, int y, OBJECTS type);
	
	
	/**
	 * pre:<br>
	 * 		pre setObjectAt(x,y,I) requires 0<= x < largeur() ^ 0<= y <hauteur()
	 * <br>
	 * post:
	 * 		getObjectAt(setObjectAt(x,y,type),x,y)== OBJECT::myType(o)<br>
	 * 		numerosMurailles(setObjectAt(x,y,type)) = numerosMurailles(T)
	 * <br>
	 * <br>
	 * Set quel objet se trouvant a un tel coordonner
	 * @return void
	 */
	
	public void setObjectAt(int x, int y, IMyObjectService object);
	
	
	
	/**
	 * 
	 * @param largeur
	 * @param hauteur
	 */
	
	/** 
	 * pre:<br>
	 * 		pre init(largeur,hauteur) requires largeur > 0 ^ hauteur > 0 <br>   
	 * <br>
	 * post: <br>
	 * 	   numerosMurailles(init(l,h)) = VIDE
	 *     largeur(init(l,h))= l; <br>
	 *     largeur(init(l,h))= h; <br>
	 *     pour tous 0 <=0 i < largeur
	 *     <br>  pour tous 0 <=j < hauteur
	 *     <br>		getObjectAt(i, j)= OBJECTS::RIEN
	 *     <br>
	 */
	void init(int largeur, int hauteur);
	
	
	/**
	 * Operator 
	 */
	
	/**
	 * pre:<br>
	 * 		pre detruireMuraille(T, id) requires id exists in numerosMurailles(T)<br>
	 * post:<br>
	 * 		<br>
	 *		<br> 
	 *		id not in  numerosMurailles()<br>
	 * 		soit o = getMurailles(T,id)<br>
	 * 		et x = o.getX() ^ y = o.getY()<br>
	 * 		pour tous i = 0 et i < OBJECT:largeur(o)<br>
	 * 			pour tous j =0 et j < OBJECT:hauteur(o)<br>
	 * 				getObjectAt(setObjectAt(x,y,o),x+i,y+j)== RIEN<br>
	 * 		
	 * @param id
	 */
	public void detruireMuraille(int id);
	

	/**
	 * Invriants
	 * nbMurailles(T) = (min) |numerosMurailles(T)|
	 */
}
