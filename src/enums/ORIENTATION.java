package enums;

// Orientation of Roads and Walls
public enum ORIENTATION {
	VERTICAL,
	HORIZONTAL
}
