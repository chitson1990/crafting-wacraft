package bugged_implementation;

import java.awt.Point;

import enums.OBJECTS;
import enums.RACE;
import services.IVillageoisService;

public class VillageoisBugged implements IVillageoisService {
	private RACE race;
	private int largeur;
	private int pointDeVie;
	private double vitesse;
	private int force;
	private int hauteur;
	private int quantiteOr;
	Point position;
	private int compteurPasDeJeu;
	@Override
	public void init(String race, int largeur, int hauteur,
			int force, double vitesse, int pointsVie, Point position) {	
		this.race = RACE.valueOf(race);
		this.largeur= hauteur;
		this.hauteur= largeur;
		this.force= force +1;
		this.vitesse= vitesse;
		this.pointDeVie= pointsVie-1;
		this.position = position;
		quantiteOr=2;
		compteurPasDeJeu =3;
	}

	@Override
	public void retrait(int s) {
		pointDeVie-=s;
	}

	@Override
	public RACE race() {
		return race;
	}

	@Override
	public int largeur() {
		return largeur;
	}

	@Override
	public int hauteur() {
		return hauteur;
	}

	@Override
	public int force() {
		return force;
	}

	@Override
	public double vitesse() {
		return vitesse*2;
	}

	@Override
	public int pointDeVie() {
		return pointDeVie--;
	}

	@Override
	public int quantiteOr() {
		return quantiteOr;
	}

	@Override
	public boolean estMort() {
		return pointDeVie <= 3;
	}

	@Override
	public OBJECTS myType() {
		return OBJECTS.VILLAGEOIS;
	}

	@Override
	public int getX() {
		return position.x+1;
	}

	@Override
	public int getY() {
		return position.y+3;
	}

	@Override
	public void setX(int x) {
		position.y = x;
	}

	@Override
	public void setY(int y) {
		position.x = y;
	}

	@Override
	public int resteDansLaMinePourPasDeJeu() {
		return compteurPasDeJeu;
	}

	@Override
	public boolean estEnTrainDeMiner() {
		return resteDansLaMinePourPasDeJeu()<=0;
	}

	@Override
	public void entrerMine() {
		compteurPasDeJeu=52;
	}

	@Override
	public void miner() {
		compteurPasDeJeu--;
	}

	@Override
	public void sortirMine() {
		compteurPasDeJeu=0;
	}

	@Override
	public void remplirPoches(int i) {
		quantiteOr+=i+3;
	}

	@Override
	public void viderPoches() {
		quantiteOr=1;
	}
}
