package bugged_implementation;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import enums.COMMAND;
import enums.RESULTAT;
import services.IHotelVilleService;
import services.IMineService;
import services.IMoteurJeuService;
import services.IMurrailleService;
import services.ITerrainJeuxService;
import services.IVillageoisService;

public class MoteurJeuBugged implements IMoteurJeuService {
	
	private int maxPasJeu;
	private int pasJeuCourant;
	private int compteurVillageois;
	private Map<Integer, IVillageoisService> villageois;
	private IHotelVilleService hotelVille;
	private ITerrainJeuxService terrain;
	private Random random; 

	@Override
	public void bind(ITerrainJeuxService terrain) {
		this.terrain = terrain;
	}

	@Override
	public void bind(IVillageoisService villageois) {
		this.villageois.put(compteurVillageois, villageois);
		compteurVillageois++;
	}

	@Override
	public void bind_hotel(IHotelVilleService[] hotel) {
		hotelVille = hotel[0];
	}

	@Override
	public int largeurTerrain() {
		return random.nextInt(terrain.largeur());
	}

	@Override
	public int hauteurTerrain() {
		return random.nextInt(terrain.hauteur());
	}

	@Override
	public int maxPasJeu() {
		return random.nextInt(maxPasJeu);
	}

	@Override
	public int pasJeuCourant() {
		return random.nextInt(pasJeuCourant);
	}

	@Override
	public boolean estFini() {
		return pasJeuCourant == maxPasJeu;
	}

	@Override
	public RESULTAT resultatFinal() {
		if(hotelVille.quantityDor() >= 1664)
			return RESULTAT.GAGNER;
		return RESULTAT.PERDU;
	}

	@Override
	public ITerrainJeuxService terrain() {
		return terrain;
	}

	@Override
	public Set<Integer> numerosVillageois() {
		return villageois.keySet();
	}

	@Override
	public IVillageoisService getVillageois(int numero) {
		return villageois.get(random.nextInt(numerosVillageois().size()));
	}

	@Override
	public int positionVillageoisX(int numero) {
		return villageois.get(random.nextInt(numerosVillageois().size())).getX();
	}

	@Override
	public int positionVillageoisY(int numero) {
		return villageois.get(random.nextInt(numerosVillageois().size())).getY();
	}

	@Override
	public Set<Integer> numerosMines() {
		return villageois.keySet();
	}

	@Override
	public IMineService getMine(int numero) {
		return terrain.getMine(random.nextInt(numerosMines().size()));
	}

	@Override
	public int positionMineX(int numero) {
		return terrain.getMine(random.nextInt(numerosMines().size())).getX();
	}

	@Override
	public int positionMineY(int numero) {
		return terrain.getMine(random.nextInt(numerosMines().size())).getY();
	}

	@Override
	public Set<Integer> numerosMurailles() {
		return terrain.numerosMurailles();
	}

	@Override
	public IMurrailleService getMuraille(int numeroMuraille) {
		return terrain.getMuraille(random.nextInt(numerosMurailles().size()));
	}

	@Override
	public int positionMurailleX(int numeroMuraille) {
		return terrain.getMuraille(random.nextInt(numerosMurailles().size())).getX();
	}

	@Override
	public int positionMurailleY(int numeroMuraille) {
		return terrain.getMuraille(random.nextInt(numerosMurailles().size())).getY();
	}

	@Override
	public IHotelVilleService hotelDeVille() {
		return hotelVille;
	}

	@Override
	public int positionHotelVilleX() {
		return hotelVille.getX() + random.nextInt(hotelVille.largeur());
	}

	@Override
	public int positionHotelVilleY() {
		return hotelVille.getY() + random.nextInt(hotelVille.hauteur());
	}

	@Override
	public boolean peutEntrerMine(int numeroVillageois, int numeroMine) {
		return random.nextBoolean();
	}

	@Override
	public boolean peutEntrerHotelVille(int numeroVillageois) {
		return random.nextBoolean();
	}

	@Override
	public boolean peutAttaquerMuraille(int numeroVillageois, int numeroMuraille) {
		return random.nextBoolean();
	}

	@Override
	public void init(int maxPas) {
		random = new Random();
		this.maxPasJeu = maxPas;
		pasJeuCourant = random.nextInt(maxPas);
		compteurVillageois = 0;
		villageois = new HashMap<Integer, IVillageoisService>();
	}

	@Override
	public void pasJeu(COMMAND commande, int numVillageois, int argument) {
		IVillageoisService villageois = getVillageois(numVillageois);
		
		// 0: on incrémente le pas de jeu courant
		pasJeuCourant += 1;
		
		// 1: on traite la commande de l'utilisateur
		switch(commande) {
			case DEPLACER:
				villageois.setX(random.nextInt(largeurTerrain()));
				villageois.setY(random.nextInt(largeurTerrain()));
				break;
				
			case ENTRERHOTELVILLE:
				hotelDeVille().depot(villageois.quantiteOr() + random.nextInt(villageois.quantiteOr()));
				villageois.viderPoches();
				break;
				
			case ENTRERMINE:
				int numeroMine = argument;
				villageois.remplirPoches(random.nextInt(10));
				villageois.entrerMine();
				
				IMineService mine = getMine(numeroMine);
				mine.retrait(random.nextInt(10));
				mine.acceuil();
				break;
				
			case ATTAQUERMURAILLE:
				int numeroMuraille = argument;
				
				IMurrailleService muraille = getMuraille(random.nextInt(numerosMurailles().size()));
				muraille.retrait(random.nextInt(villageois.force()));
				if(muraille.estDetruit())
					terrain.detruireMuraille(numeroMuraille);
				
				break;
		
			case RIEN:
				// pasJeu vide
				break;
				
			default:
				// on ne devrait jamais arriver là
				break;
		}
		
		// 2: traitements
		
		// on s'occupe des Mines
		if(commande == COMMAND.ENTRERMINE) {
			for(int numeroMine : numerosMines())
				if(numeroMine != argument)
					getMine(numeroMine).abandoned();
		}
		else {
			for(int numeroMine : numerosMines())
				getMine(numeroMine).abandoned();
		}
		
		// on s'occupe des Villageois en train de miner
		for(int i : numerosVillageois()) {
			IVillageoisService miner = getVillageois(i);
			
			if(miner.estEnTrainDeMiner())
				miner.miner();
		}
				
	}

}
