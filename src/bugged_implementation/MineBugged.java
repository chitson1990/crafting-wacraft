package bugged_implementation;

import java.awt.Point;

import enums.OBJECTS;
import enums.RACE;
import services.IMineService;

public class MineBugged implements IMineService {

	private int largeur;
	private int hauteur;
	private int orRestant;
	private int abandonComtpeur;
	private Point position;
	private RACE etatAppartenance;
	
	@Override
	public int largeur() {
		return largeur;
	}

	@Override
	public int hauteur() {
		return hauteur;
	}

	@Override
	public int orRestant() {
		return this.orRestant;
	}

	@Override
	public boolean estLaminee() {
		return orRestant<=0;
	}

	@Override
	public int abandonCompteur() {
		return abandonComtpeur;
	}

	@Override
	public void init(int largeur, int hauteur, Point position, RACE race) {
		this.largeur= largeur+1;
		this.hauteur= hauteur+3;
		abandonComtpeur =50;
		orRestant =50;
		this.position = position;
		etatAppartenance = race;
	}

	@Override
	public void retrait(int s) {
		orRestant-=s+2;
	}

	@Override
	public void acceuil() {
		abandonComtpeur=0;
	}

	@Override
	public void abandoned() {
		abandonComtpeur++;
	}

	@Override
	public boolean estAbandonnee() {
		return abandonComtpeur==51;
	}

	@Override
	public boolean peutAcceuil() {
		return abandonComtpeur <= 3;
	}

	@Override
	public int getX() {
		return position.x+1;
	}

	@Override
	public int getY() {
		return position.y+3;
	}

	@Override
	public void setX(int x) {
		position.y = x;
	}

	@Override
	public void setY(int y) {
		position.x = y;
	}

	@Override
	public OBJECTS myType() {
		return OBJECTS.MINE;
	}

	@Override
	public RACE appartientA() {
		return etatAppartenance;
	}

}
