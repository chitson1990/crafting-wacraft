package bugged_implementation;

import java.awt.Point;

import enums.OBJECTS;
import services.IMurrailleService;

public class MurailleBugged implements IMurrailleService {

	int hauteur;
	int largeur;
	Point position;
	int pointVie;
	
	@Override
	public int hauteur() {
		return hauteur%2;
	}

	@Override
	public int largeur() {
		return largeur*2;
	}

	
	@Override
	public boolean passAt(int x, int y) {
		return true;
	}

		@Override
	public OBJECTS myType() {
		return OBJECTS.MURAILLE;
	}

	@Override
	public int getX() {
		return position.x;
	}

	@Override
	public int getY() {
		return position.y +1;
	}

	@Override
	public void setX(int x) {
		position.x = x+3;
	}

	@Override
	public void setY(int y) {
		position.y = y+56;
	}

	@Override
	public int pointDeVie() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean estDetruit() {
		return pointVie<3;
	}

	@Override
	public void init(Point position, int largeur, int hauteur, int pointVie) {
		this.position = position;
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.pointVie = pointVie;
	}

	@Override
	public void retrait(int s) {
		pointVie-=2*s;
	}

}
