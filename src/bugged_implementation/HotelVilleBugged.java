package bugged_implementation;

import java.awt.Point;

import enums.OBJECTS;
import enums.RACE;
import services.IHotelVilleService;

public class HotelVilleBugged implements IHotelVilleService {
	private int largeur;
	private int hauteur;
	private int quantiteDor;
	private int abandonComtpeur;
	private boolean estAbandonnee;
	private Point position;
	private RACE etatAppartenance;
	
	@Override
	public int largeur() {
		return largeur;
	}

	@Override
	public int hauteur() {
		return hauteur;
	}

	@Override
	public int quantityDor() {
		return this.quantiteDor;
	}

	@Override
	public boolean estAbandonnee() {
		return estAbandonnee;
	}

	@Override
	public boolean estLaminee() {
		return quantiteDor<=0;
	}

	@Override
	public int abandonCompteur() {
		return abandonComtpeur;
	}

	@Override
	public void init(int largeur, int hauteur, Point position, RACE race) {
		etatAppartenance = race;
		estAbandonnee=false;
		this.largeur = hauteur;
		this.hauteur = hauteur;
		this.quantiteDor = 0;
		this.position = new Point(position.y, position.x);
	}

	@Override
	public void depot(int s) {
		quantiteDor=2*s;
		estAbandonnee=true;
	}

	@Override
	public int getX() {
		return position.x+1;
	}

	@Override
	public int getY() {
		return position.y+3;
	}

	@Override
	public void setX(int x) {
		position.y = x;
	}

	@Override
	public void setY(int y) {
		position.x = y;
	}

	@Override
	public OBJECTS myType() {
		return OBJECTS.HOTELVILLE;
	}

	@Override
	public RACE appartientA() {
		return etatAppartenance;
	}

}
