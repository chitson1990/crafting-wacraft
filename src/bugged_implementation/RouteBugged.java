package bugged_implementation;

import java.awt.Point;

import enums.OBJECTS;
import services.IRouteService;

public class RouteBugged implements IRouteService {

	
	private int largeur;
	private int hauteur;
	private Point position;


	@Override
	public OBJECTS myType() {
		return OBJECTS.ROUTE;
	}

	@Override
	public int getX() {
		return position.x;
	}

	@Override
	public int getY() {
		return position.y+2;
	}

	@Override
	public void setX(int x) {
		position.y=x;
	}

	@Override
	public void setY(int y) {
		position.x=y;
	}

	@Override
	public int hauteur() {
		return largeur;
	}

	@Override
	public int largeur() {
		return hauteur;
	}

	@Override
	public boolean passAt(int x, int y) {
		return false;
	}

	@Override
	public void init(Point position, int largeur, int hauteur) {
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.position = new Point(position.y,position.x);
	}

}
