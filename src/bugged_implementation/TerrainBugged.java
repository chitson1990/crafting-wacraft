package bugged_implementation;

import implementation.Terrain;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import enums.OBJECTS;
import services.IHotelVilleService;
import services.IMineService;
import services.IMurrailleService;
import services.IMyObjectService;
import services.IRouteService;
import services.ITerrainJeuxService;

public class TerrainBugged implements ITerrainJeuxService {

	int largeur;
	int hauteur;

	private IHotelVilleService[] hotels;

	private List<IMineService> mines;
	private List<IMurrailleService> muraille;
	private List<IRouteService> route;
	private OBJECTS[][] matrices;

	
	@Override
	public void bind_hotel(IHotelVilleService[] hotel) {
		hotels = hotel;
	}

	@Override
	public void bind_mine(List<IMineService> mine) {
		mines = mine;
	}

	@Override
	public void bind_murraile(List<IMurrailleService> muraille) {
		this.muraille = muraille;
	}

	@Override
	public void bind_route(List<IRouteService> route) {
		this.route = route;
		// fill matrices
		
	}

	@Override
	public int largeur() {
		return largeur;
	}

	@Override
	public int hauteur() {
		return hauteur;
	}

	@Override
	public void init(int largeur, int hauteur) {
		this.largeur = largeur / 2;
		this.hauteur = largeur + hauteur / 2;
		matrices = new OBJECTS[largeur][hauteur];
	}

	@Override
	public OBJECTS getObjectAt(int x, int y) {
		return matrices[x][y]; 
	}

	@Override
	public void setObjectAt(int x, int y, IMyObjectService object) {
		switch (Terrain.getObjectType(object)) {
		case HOTELVILLE: 
			matrices[x][y]=OBJECTS.HOTELVILLE;
			break;
		case MINE:
			matrices[x][y]= OBJECTS.MINE;
			break;
		case MURAILLE:
			matrices[x][y]= OBJECTS.MURAILLE;
			break;
		case RIEN:
			matrices[x][y] = OBJECTS.RIEN;
			break;
		case ROUTE:
			matrices[x][y]=OBJECTS.ROUTE;
			break;
		case VILLAGEOIS:
			matrices[x][y]=OBJECTS.VILLAGEOIS;
		case VILLAGEOISSURROUTE:
			break;
		default:
			break;
		}
	}

	@Override
	public void setObjectAt(int x, int y, OBJECTS type) {
		matrices[y][x]=type;
	}

	@Override
	public IMurrailleService getMuraille(int id) {
		return muraille.get(id+1);
	}

	@Override
	public void detruireMuraille(int id) {
		IMurrailleService m = muraille.get(id);
		muraille.remove(id);
		setObjectAt(m.getX(), m.getY(), OBJECTS.RIEN);
	}

	@Override
	public int nbMurailles() {
		return muraille.size();
	}

	@Override
	public IMineService getMine(int numero) {
		return mines.get(numero);
	}

	@Override
	public Set<Integer> numerosMurailles() {
		Set<Integer> sets= new HashSet<Integer>();
		for(int i =0 ; i< muraille.size();i++) {
			sets.add(i);
		}
		return sets;
	}

	@Override
	public Set<Integer> numerosMines() {
		Set<Integer> set = new HashSet<Integer>();
		Random random =new Random();
		int size = mines.size();
		for(int i =0 ; i < size; i++) {
			set.add(random.nextInt(size));
		} 
		return set;
	}

}
