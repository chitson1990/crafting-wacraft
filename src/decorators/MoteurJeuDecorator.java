package decorators;

import java.util.Set;

import enums.COMMAND;
import enums.RESULTAT;
import services.IHotelVilleService;
import services.IMineService;
import services.IMoteurJeuService;
import services.IMurrailleService;
import services.ITerrainJeuxService;
import services.IVillageoisService;

public class MoteurJeuDecorator implements IMoteurJeuService {
	
	private IMoteurJeuService delegate;
	
	public MoteurJeuDecorator(IMoteurJeuService delegate) {
		this.delegate = delegate;
	}

	@Override
	public void bind(ITerrainJeuxService terrain) {
		delegate.bind(terrain);
	}

	@Override
	public void bind(IVillageoisService villageois) {
		delegate.bind(villageois);
	}

	@Override
	public void bind_hotel(IHotelVilleService[] hotel) {
		delegate.bind_hotel(hotel);
	}

	@Override
	public int largeurTerrain() {
		return delegate.largeurTerrain();
	}

	@Override
	public int hauteurTerrain() {
		return delegate.hauteurTerrain();
	}

	@Override
	public int maxPasJeu() {
		return delegate.maxPasJeu();
	}

	@Override
	public int pasJeuCourant() {
		return delegate.pasJeuCourant();
	}

	@Override
	public boolean estFini() {
		return delegate.estFini();
	}

	@Override
	public RESULTAT resultatFinal() {
		return delegate.resultatFinal();
	}

	@Override
	public ITerrainJeuxService terrain() {
		return delegate.terrain();
	}

	@Override
	public Set<Integer> numerosVillageois() {
		return delegate.numerosVillageois();
	}

	@Override
	public IVillageoisService getVillageois(int numero) {
		return delegate.getVillageois(numero);
	}

	@Override
	public int positionVillageoisX(int numero) {
		return delegate.positionVillageoisX(numero);
	}

	@Override
	public int positionVillageoisY(int numero) {
		return delegate.positionVillageoisY(numero);
	}

	@Override
	public Set<Integer> numerosMines() {
		return delegate.numerosMines();
	}

	@Override
	public IMineService getMine(int numero) {
		return delegate.getMine(numero);
	}

	@Override
	public int positionMineX(int numero) {
		return delegate.positionMineX(numero);
	}

	@Override
	public int positionMineY(int numero) {
		return delegate.positionMineY(numero);
	}

	@Override
	public Set<Integer> numerosMurailles() {
		return delegate.numerosMurailles();
	}

	@Override
	public IMurrailleService getMuraille(int numeroMuraille) {
		return delegate.getMuraille(numeroMuraille);
	}

	@Override
	public int positionMurailleX(int numeroMuraille) {
		return delegate.positionMurailleX(numeroMuraille);
	}

	@Override
	public int positionMurailleY(int numeroMuraille) {
		return delegate.positionMurailleY(numeroMuraille);
	}

	@Override
	public IHotelVilleService hotelDeVille() {
		return delegate.hotelDeVille();
	}

	@Override
	public int positionHotelVilleX() {
		return delegate.positionHotelVilleX();
	}

	@Override
	public int positionHotelVilleY() {
		return delegate.positionHotelVilleY();
	}

	@Override
	public boolean peutEntrerMine(int numeroVillageois, int numeroMine) {
		return delegate.peutEntrerMine(numeroVillageois, numeroMine);
	}

	@Override
	public boolean peutEntrerHotelVille(int numeroVillageois) {
		return delegate.peutEntrerHotelVille(numeroVillageois);
	}

	@Override
	public void init(int maxPas) {
		delegate.init(maxPas);
	}

	@Override
	public void pasJeu(COMMAND commande, int numVillageois, int argument) {
		delegate.pasJeu(commande, numVillageois, argument);
	}

	@Override
	public boolean peutAttaquerMuraille(int numeroVilageois, int numeroMuraille) {
		return delegate.peutAttaquerMuraille(numeroVilageois, numeroMuraille);
	}

}
