package decorators;

import java.awt.Point;

import enums.OBJECTS;
import enums.RACE;
import services.IMineService;

public class MineDecorator implements IMineService {

	private IMineService delegate;

	public MineDecorator(IMineService delegate) {
		this.delegate = delegate;
	}

	@Override
	public int largeur() {
		return delegate.largeur();
	}

	@Override
	public int hauteur() {
		return delegate.hauteur();
	}

	@Override
	public int orRestant() {
		return delegate.orRestant();
	}

	@Override
	public boolean estLaminee() {
		return delegate.estLaminee();
	}

	@Override
	public int abandonCompteur() {
		return delegate.abandonCompteur();
	}

	@Override
	public void init(int largeur, int hauteur, Point position, RACE race) {
		delegate.init(largeur, hauteur, position, race);
	}

	@Override
	public void acceuil() {
		delegate.acceuil();
	}

	@Override
	public void abandoned() {
		delegate.abandoned();
	}

	@Override
	public boolean estAbandonnee() {
		return delegate.estAbandonnee();
	}

	@Override
	public boolean peutAcceuil() {
		return delegate.peutAcceuil();
	}

	@Override
	public void retrait(int s) {
		delegate.retrait(s);
	}

	@Override
	public OBJECTS myType() {
		return delegate.myType();
	}

	@Override
	public int getX() {
		return delegate.getX();
	}

	@Override
	public int getY() {
		return delegate.getY();
	}

	@Override
	public void setX(int x) {
		delegate.setX(x);
	}

	@Override
	public void setY(int y) {
		delegate.setY(y);
	}

	@Override
	public RACE appartientA() {
		return delegate.appartientA();
	}
}
