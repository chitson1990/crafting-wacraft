package decorators;

import java.awt.Point;
import enums.OBJECTS;
import services.IRouteService;

//TODO
public class RouteDecorator implements IRouteService {

	private IRouteService route;
	
	public RouteDecorator(IRouteService route) {
		this.route = route;
	}

	
	@Override
	public boolean passAt(int x, int y) {
		return route.passAt(x, y);
	}

		@Override
	public OBJECTS myType() {
		return route.myType();
	}

	@Override
	public int getX() {
		return route.getX();
	}

	@Override
	public int getY() {
		return route.getY();
	}

	@Override
	public void setX(int x) {
		route.setX(x);
	}

	@Override
	public void setY(int y) {
		route.setY(y);
	}


	@Override
	public int hauteur() {
		return route.hauteur();
	}


	@Override
	public int largeur() {
		return route.largeur();
	}


	@Override
	public void init(Point position, int largeur, int hauteur) {
		route.init(position, largeur, hauteur);
	}
}
