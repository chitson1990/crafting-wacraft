package decorators;

import java.util.List;
import java.util.Set;

import enums.OBJECTS;
import services.IHotelVilleService;
import services.IMineService;
import services.IMurrailleService;
import services.IMyObjectService;
import services.IRouteService;
import services.ITerrainJeuxService;

public class TerrainJeuDecorator implements ITerrainJeuxService {

	private ITerrainJeuxService terrain;

	public TerrainJeuDecorator(ITerrainJeuxService terrain) {
		this.terrain = terrain;
	}
	
	@Override
	public void bind_hotel(IHotelVilleService[] hotel) {
		terrain.bind_hotel(hotel);
	}

	@Override
	public void bind_mine(List<IMineService> mine) {
		terrain.bind_mine(mine);
	}

	@Override
	public void bind_murraile(List<IMurrailleService> muraille) {
		terrain.bind_murraile(muraille);
	}

	@Override
	public void bind_route(List<IRouteService> route) {
		terrain.bind_route(route);
	}

	@Override
	public int largeur() {
		return terrain.largeur();
	}

	@Override
	public int hauteur() {
		return terrain.hauteur();
	}

	@Override
	public void init(int largeur, int hauteur) {
		terrain.init(largeur, hauteur);
	}

	@Override
	public OBJECTS getObjectAt(int x, int y) {
		return terrain.getObjectAt(x, y);
	}

	@Override
	public void setObjectAt(int x, int y, IMyObjectService object) {
		terrain.setObjectAt(x, y, object);
	}

	@Override
	public void setObjectAt(int x, int y, OBJECTS type) {
		terrain.setObjectAt(x, y, type);
	}

	@Override
	public int nbMurailles() {
		return terrain.nbMurailles();
	}

	@Override
	public IMurrailleService getMuraille(int id) {
		return terrain.getMuraille(id);
	}

	@Override
	public void detruireMuraille(int id) {
		terrain.detruireMuraille(id);
	}

	@Override
	public IMineService getMine(int numero) {
		return terrain.getMine(numero);
	}

	@Override
	public Set<Integer> numerosMurailles() {
		return terrain.numerosMurailles();
	}

	@Override
	public Set<Integer> numerosMines() {
		return terrain.numerosMines();
	}

}
