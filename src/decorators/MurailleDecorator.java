package decorators;

import java.awt.Point;

import enums.OBJECTS;
import services.IMurrailleService;

//TODO
public class MurailleDecorator implements IMurrailleService {
	private IMurrailleService muraille;

	public MurailleDecorator(IMurrailleService muraille) {
		this.muraille = muraille;
	}

	@Override
	public int hauteur() {
		return muraille.hauteur();
	}

	@Override
	public int largeur() {
		return muraille.largeur();
	}

	
	@Override
	public boolean passAt(int x, int y) {
		return muraille.passAt(x, y);
	}

	@Override
	public OBJECTS myType() {
		return muraille.myType();
	}

	@Override
	public int getX() {
		return muraille.getX();
	}

	@Override
	public int getY() {
		return muraille.getY();
	}

	@Override
	public void setX(int x) {
		muraille.setX(x);
	}

	@Override
	public void setY(int y) {
		muraille.setY(y);
	}

	@Override
	public int pointDeVie() {
		return muraille.pointDeVie();
	}

	@Override
	public boolean estDetruit() {
		return muraille.estDetruit();
	}

	@Override
	public void init(Point position, int largeur, int hauteur, int pointVie) {
		muraille.init(position, largeur, hauteur, pointVie);
	}

	
	@Override
	public void retrait(int s) {
		muraille.retrait(s);
	}

}
