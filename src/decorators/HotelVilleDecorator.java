package decorators;

import java.awt.Point;

import enums.OBJECTS;
import enums.RACE;
import services.IHotelVilleService;

public class HotelVilleDecorator implements IHotelVilleService {

	private IHotelVilleService delegate;

	public HotelVilleDecorator(IHotelVilleService delegate) {
		this.delegate = delegate;
	}

	@Override
	public int largeur() {
		return delegate.largeur();
	}

	@Override
	public int hauteur() {
		return delegate.hauteur();
	}

	@Override
	public int quantityDor() {
		return delegate.quantityDor();
	}

	@Override
	public boolean estAbandonnee() {
		return delegate.estAbandonnee();
	}

	@Override
	public boolean estLaminee() {
		return delegate.estLaminee();
	}

	@Override
	public int abandonCompteur() {
		return delegate.abandonCompteur();
	}

	@Override
	public void init(int largeur, int hauteur, Point position, RACE race) {
		delegate.init(largeur, hauteur, position,race);
	}

	@Override
	public void depot(int s) {
		delegate.depot(s);
	}

	@Override
	public OBJECTS myType() {
		return delegate.myType();
	}

	@Override
	public int getX() {
		return delegate.getX();
	}

	@Override
	public int getY() {
		return delegate.getY();
	}

	@Override
	public void setX(int x) {
		delegate.setX(x);
	}

	@Override
	public void setY(int y) {
		delegate.setY(y);
	}

	@Override
	public RACE appartientA() {
		return delegate.appartientA();
	}
}
