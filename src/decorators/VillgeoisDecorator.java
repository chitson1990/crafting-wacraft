package decorators;

import java.awt.Point;

import enums.OBJECTS;
import enums.RACE;
import services.IVillageoisService;

public class VillgeoisDecorator implements IVillageoisService {

	private IVillageoisService delegate;

	public VillgeoisDecorator(IVillageoisService delegate) {
		this.delegate = delegate;
	}

	@Override
	public RACE race() {
		return delegate.race();
	}

	@Override
	public int largeur() {
		return delegate.largeur();
	}

	@Override
	public int hauteur() {
		return delegate.hauteur();
	}

	@Override
	public int force() {
		return delegate.force();
	}

	@Override
	public double vitesse() {
		return delegate.vitesse();
	}

	@Override
	public int pointDeVie() {
		return delegate.pointDeVie();
	}

	@Override
	public int quantiteOr() {
		return delegate.quantiteOr();
	}

	@Override
	public boolean estMort() {
		return delegate.estMort();
	}

	@Override
	public void init(String race, int largeur, int hauteur, int force,
			double vitesse, int pointsVie, Point position) {
		delegate.init(race, largeur, hauteur, force, vitesse, pointsVie,
				position);
	}

	@Override
	public void retrait(int s) {
		delegate.retrait(s);
	}

	@Override
	public OBJECTS myType() {
		return delegate.myType();
	}

	@Override
	public int getX() {
		return delegate.getX();
	}

	@Override
	public int getY() {
		return delegate.getY();
	}

	@Override
	public void setX(int x) {
		delegate.setX(x);
	}

	@Override
	public void setY(int y) {
		delegate.setY(y);
	}

	@Override
	public int resteDansLaMinePourPasDeJeu() {
		return delegate.resteDansLaMinePourPasDeJeu();
	}

	@Override
	public boolean estEnTrainDeMiner() {
		return delegate.estEnTrainDeMiner();
	}

	@Override
	public void entrerMine() {
		delegate.entrerMine();
	}

	@Override
	public void miner() {
		delegate.miner();
	}

	@Override
	public void sortirMine() {
		delegate.sortirMine();
	}

	@Override
	public void remplirPoches(int i) {
		delegate.remplirPoches(i);
	}

	@Override
	public void viderPoches() {
		delegate.viderPoches();
	}

}
