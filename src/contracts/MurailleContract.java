package contracts;

import java.awt.Point;

import services.IMurrailleService;
import decorators.MurailleDecorator;
import errors.InvariantError;
import errors.PostconditionError;
import errors.PreconditionError;

public class MurailleContract extends MurailleDecorator implements
		IMurrailleService {

	public MurailleContract(IMurrailleService muraille) {
		super(muraille);
	}

	public void checkInvariant() {
		if (estDetruit() != (pointDeVie() <= 0)) {
			throw new InvariantError(
					"estMort(M) = (min)pointsDeVie(M) ≤0	incorrecte");
		}

	}

	@Override
	public void init(Point position, int largeur, int hauteur, int pointVie) {
		// pre
		if (position == null) {
			throw new PreconditionError(
					" pre init(p,l,h,vie) require p <> \"\" ");
		} else if (position.x <= 0) {
			throw new PreconditionError(
					" pre init(p,l,h,vie) require p.x >0 incorrect ");
		} else if (position.y <= 0) {
			throw new PreconditionError(
					" pre init(p,l,h,vie) require p.y >0 incorrect ");
		}
		if (largeur <= 0) {
			throw new PreconditionError("pre init(p,l,h,vie) requires l > 0");
		}

		if (hauteur <= 0) {
			throw new PreconditionError("pre init(p,l,h,vie) requires h > 0");
		}

		if (pointVie <= 0) {
			throw new PreconditionError("pre init(p,l,h,vie) requires vie > 0");
		}
		// Operations
		super.init(position, largeur, hauteur, pointVie);

		// Invariant
		checkInvariant();

		// Post

		if (position.x != getX()) {
			throw new PostconditionError(
					"post getX(init(p,l,h,vie)) = p.x incorect");
		}

		if (position.y != getY()) {
			throw new PostconditionError(
					"post getY(init(p,l,h,vie)) = p.y incorect");
		}

		if (largeur() != largeur) {
			throw new PostconditionError(
					"post larguer(init(p,l,h,vie))=largeur incorrect");
		}

		if (hauteur() != hauteur) {
			throw new PostconditionError(
					"post init hauteur(init(p,l,h,vie)) = h incorrect");
		}

		if (pointDeVie() != pointVie) {
			throw new PostconditionError(
					"post init orientation(init(p,l,h,vie)) = vie incorrect");
		}
	}

	@Override
	public void setX(int x) {
		//pre
		if (x <= 0) {
			throw new PreconditionError("pre setX(S, x) require x > 0");
		}

		checkInvariant();

		// Capture

		int y_pre = getY();
		int vie_pre = pointDeVie();
		
		super.setX(x);

		checkInvariant();

		// post

		if (super.getX() != x) {
			throw new PostconditionError(
					"post getX(setX(M, x) = x incorrect");
		}
		
		if(y_pre!=getY()) {
			throw new PostconditionError("post getY(setX(M,x))= getY(M) incorrect");
		}
		
		if(pointDeVie()!=vie_pre) {
			throw new PostconditionError("post pointDeVie(setX(M,x))= pointDeVie(M) incorrect");
		}

	}

	@Override
	public void setY(int y) {
		//pre
		if (y <= 0) {
			throw new PreconditionError("pre setY(M, y) require y > 0");
		}

		checkInvariant();

		// Capture

		int x_pre = getX();
		int vie_pre =pointDeVie();
		
		super.setY(y);

		checkInvariant();

		// post

		if(x_pre!=getX()) {
			throw new PostconditionError("post getX(setY(M,y))= getX(M) incorrect");
		}
		
		if(pointDeVie()!=vie_pre) {
			throw new PostconditionError("post pointDeVie(setY(M,y))= pointDeVie(M) incorrect");
		}
		if (super.getY() != y) {
			throw new PostconditionError(
					"post getY(setY(M, y) = y incorrect");
		}
	}

	
	@Override
	public void retrait(int s) {
		// pre retrait(Villageois,s) require !estMort(Villageois) && s>0
		
		if(estDetruit() || s<=0) {
			throw new PreconditionError("pre retrait(Murailles,s) require !estMort(Murailles) && s>0 incorrecte");
		}
		// checkInvariant
		checkInvariant();
		// Capture
		
		int pointsDeVie_pre = pointDeVie();
		int x_pre = getX();
		int y_pre = getY();
		
		super.retrait(s);
		
		//checkInvariant
		checkInvariant();
		
		//post
		
		if(x_pre!=getX()) {
			throw new PostconditionError("getX(retrait(Muraille,s)) = getX(Muraille) incorrect");
		}
		
		if(y_pre!=getY()) {
			throw new PostconditionError("getY(retrait(Muraille,s)) = getY(Muraille) incorrect");
		}
	
		if(pointDeVie() != (pointsDeVie_pre -s)){
			throw new PostconditionError("post pointsDeVie(retrait(Murailles,s))=pointsDeVie(Murailles) -s incorrecte");
		}
	}
}
