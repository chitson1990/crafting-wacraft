package contracts;

import java.awt.Point;

import services.IMineService;
import decorators.MineDecorator;
import enums.RACE;
import errors.InvariantError;
import errors.PostconditionError;
import errors.PreconditionError;

public class MineContract extends MineDecorator implements IMineService {
	public MineContract(IMineService delegate) {
		super(delegate);
	}

	public void checkInvariant() {
		if (abandonCompteur() < 0 ) {
			throw new InvariantError("0 ≤abandonCompteur(M) incorrecte");
		}

		if(abandonCompteur() > 51) { 
				throw new InvariantError(" abandonCompteur(M)≤ 51 incorrecte");
		}
		
		if (estLaminee() != (orRestant() <= 0)) {
			throw new InvariantError(
					"estLaminee(M) =(min) orRestant(M) ≤ 0 incorrecte");
		}

		if (estAbandonnee()!= (abandonCompteur() == 51)) {
			throw new InvariantError(
					"estAbandonnee(M) =(min) abandonCompteur = 51 incorrecte");
		}
		
		if(peutAcceuil()!=(abandonCompteur()==0)) {
			throw new InvariantError(" peutAcceuil(M)=(min) abandonCompeteur=0 incorrect");
		}
	}

	@Override
	public int largeur() {
		return super.largeur();
	}

	@Override
	public int hauteur() {
		return super.hauteur();
	}

	@Override
	public int orRestant() {
		return super.orRestant();
	}


	@Override
	public boolean estLaminee() {
		return super.estLaminee();
	}

	@Override
	public int abandonCompteur() {
		return super.abandonCompteur();
	}

	@Override
	public boolean peutAcceuil() {
		return super.peutAcceuil();
	}
	
	@Override
	public void init(int largeur, int hauteur, Point position, RACE race) {
		// Pre

		int x = position.x;
		int y = position.y;
		
		if(x <=0 || y <=0) {
			throw new PreconditionError(" pre init(largeur,hauteur,pos) requires pos.x >0 ^ pos.y >0 incorrect");
		}
		
		if(race==null) {
			throw new PreconditionError(" pre init(largeur,hauteur,pos,race) requires race <> null"); 	
		}
		
		if ((largeur % 2 != 1) || (hauteur % 2 != 1)) {
			throw new PreconditionError(
					"pre init(largeur,hauteur,pos) require largeur%2=1 ∧ hauteur%2=1 incorrecte");
		}
		super.init(largeur, hauteur, position, race);

		checkInvariant();

		// post
		if (largeur != largeur()) {
			throw new PostconditionError("largeur(init(largeur,hauteur,pos))=l incorrecte");
		}

		if (hauteur != hauteur()) {
			throw new PostconditionError("hauteur(init(largeur,hauteur,pos))=h incorrecte");
		}

		if (orRestant() != 51) {
			throw new PostconditionError("orRestant(init(largeur,hauteur,pos))=51 incorrecte");
		}

		if(appartientA()!=race) {
			throw new PostconditionError("appartientA(init(largeur,hauteur,pos,race))= race incorrect");
		}
		
		if (abandonCompteur() != 51) {
			throw new PostconditionError(
					"abandonCompteur(init(largeur,hauteur,pos))=51 incorrecte");
		}

		if(position.x != getX()) {
			throw new PostconditionError("post getX(init(largeur,hauteur,pos)) = pos.x incorect");
		}
		
		if(position.y != getY()) {
			throw new PostconditionError("post getY(init(largeur,hauteur,pos)) = pos.y incorect");
		}
	}

	@Override
	public void retrait(int s) {
		// pre

		if (estLaminee() || s <= 0) {
			throw new PreconditionError(
					"pre retrait(M,s) require ¬estLaminee(M) ∧ s>0 incorrect");
		}

		if (s > orRestant()) {
			throw new PreconditionError(
					"pre retrait(M,s) require s <= orRestant() incorrect");
		}
		
		checkInvariant();

		// Capture

		int x_pre = getX();
		int y_pre = getY();
				
		int orRestant_pre = orRestant();
		int abandonComp_pre = abandonCompteur();

		super.retrait(s);

		checkInvariant();

		// post

		if(getY()!=y_pre) {
			throw new PostconditionError(" post getY(retrai(M,s)) = getY(H) incorrect");
		}
		
		if(getX()!=x_pre) {
			throw new PostconditionError("post getX(retrai(M,s)) = getX(H) incorrect ");
		}
		
		if ((orRestant_pre - s) != orRestant()) {
			throw new PostconditionError(
					"orRestant(retrait(M,s))=orRestant(M)-s");
		}

		if (abandonComp_pre != abandonCompteur()) {
			throw new PostconditionError(
					"abandonCompteur(retrait(M,s))=abandonCompteur(M)");
		}	
	}

	@Override
	public void acceuil() {
		if (!estAbandonnee()) {
			throw new PreconditionError(
					"pre acceuil(M) require ¬abandoned(M) incorrecte");
		}

		checkInvariant();

		// Capture

		int x_pre = getX();
		int y_pre= getY();
		
		int orRestant_pre = orRestant();

		super.acceuil();

		checkInvariant();

		// post

		
		if(getY()!=y_pre) {
			throw new PostconditionError(" post getY(acceuil(M)) = getY(H) incorrect");
		}
		
		if(getX()!=x_pre) {
			throw new PostconditionError("post getX(acceuil(M)) = getX(H) incorrect ");
		}
		
		if ((orRestant_pre) != orRestant()) {
			throw new PostconditionError(
					"orRestant(acceuil(M)))=orRestant(M) incorrecte");
		}

		if (abandonCompteur() != 0) {
			throw new PostconditionError(
					"abandonCompteur(acceuil(M))=0 incorrecte");
		}
	}

	@Override
	public void abandoned() {
		if (estAbandonnee()) {
			throw new PreconditionError(
					"pre abandoned(M) require ¬estAbandonnee(M) incorrecte");
		}

		checkInvariant();

		// Capture

		int x_pre = getX();
		int y_pre= getY();
		
		int orRestant_pre = orRestant();
		int abandonComp_pre = abandonCompteur();

		super.abandoned();

		checkInvariant();

		// post

		if(getY()!=y_pre) {
			throw new PostconditionError(" post getY(abandoned(M)) = getY(H) incorrect");
		}
		
		if(getX()!=x_pre) {
			throw new PostconditionError("post getX(abandoned(M)) = getX(H) incorrect ");
		}
		
		if ((orRestant_pre) != orRestant()) {
			throw new PostconditionError(
					"orRestant(abandoned(M)))=orRestant(M) incorrecte");
		}

		if (abandonCompteur() != (abandonComp_pre + 1)) {
			throw new PostconditionError(
					"abandonCompteur(abandoned(M))=abandonCompteur(abandoned(M))+1 incorrecte");
		}
	}

	@Override
	public void setX(int x) {
		if (x <= 0) {
			throw new PreconditionError("pre setX(M, x) require x > 0");
		}

		checkInvariant();

		// Capture

		int y_pre = getY();
		int or_pre = orRestant();
		int compteur_pre =abandonCompteur();
		
		super.setX(x);

		checkInvariant();

		// post

		if (super.getX() != x) {
			throw new PostconditionError(
					"post getX(setX(M, x) = x incorrect");
		}
		
		if (super.getY() != y_pre) {
			throw new PostconditionError(
					"post getY(setX(M, x) = getY(M) incorrect");
		}
		
		if(or_pre!=orRestant()) {
			throw new PostconditionError("post orRestant(setX(M,x))=orRestant(M) incorrect");
		}
		
		if(compteur_pre!=abandonCompteur()) {
			throw new PostconditionError(" post abandonCompteur(setX(M,x))=abandonCompteur(M) incorrect");
		}
	}

	@Override
	public void setY(int y) {
		if (y <= 0) {
			throw new PreconditionError("pre setX(M, y) require y > 0");
		}

		checkInvariant();

		// Capture

		int x_pre = getX();
		int or_pre = orRestant();
		int compteur_pre =abandonCompteur();
		
		
		super.setY(y);

		checkInvariant();

		// post

		if (super.getY() != y) {
			throw new PostconditionError(
					"post getY(setY(Villageois, y) = y incorrect");
		}
		
		if (x_pre!=getX()) {
			throw new PostconditionError("post getX(setY(M,y)) = getX(M) incorrect");
		}
		if(or_pre!=orRestant()) {
			throw new PostconditionError("post orRestant(setY(M,y))=orRestant(M) incorrect");
		}
		
		if(compteur_pre!=abandonCompteur()) {
			throw new PostconditionError(" post abandonCompteur(setY(M))=abandonCompteur(M) incorrect");
		}
	}
}
