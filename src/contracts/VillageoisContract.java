package contracts;

import java.awt.Point;

import services.IVillageoisService;
import decorators.VillgeoisDecorator;
import enums.RACE;
import errors.InvariantError;
import errors.PostconditionError;
import errors.PreconditionError;

public class VillageoisContract extends VillgeoisDecorator implements
		IVillageoisService {

	public VillageoisContract(IVillageoisService delegate) {
		super(delegate);
	}

	public void checkInvariant() {
		if (estMort() != (pointDeVie() <= 0)) {
			throw new InvariantError(
					"estMort(V) = (min)pointsDeVie(V) ≤0	incorrecte");
		}

		if (estEnTrainDeMiner() != (resteDansLaMinePourPasDeJeu() > 0)) {
			throw new InvariantError(
					" estEnTrainDeMiner(V) = (min) resteDansLaMinePourPasDeJeu(V) > 0 incorrect");
		}
	}

	@Override
	public RACE race() {
		return super.race();
	}

	@Override
	public int largeur() {
		return super.largeur();
	}

	@Override
	public int hauteur() {
		return super.hauteur();
	}

	@Override
	public int force() {
		return super.force();
	}

	@Override
	public double vitesse() {
		return super.vitesse();
	}

	@Override
	public int pointDeVie() {
		return super.pointDeVie();
	}

	@Override
	public int quantiteOr() {
		return super.quantiteOr();
	}

	@Override
	public boolean estMort() {
		return super.estMort();
	}

	@Override
	public void init(String race, int largeur, int hauteur, int force,
			double vitesse, int pointsVie, Point position) {

		// pre init(race,largeur,hauteur,force,vitesse,pointsVie) require race
		// <> "" && largeur%2=1 && hauteur%2=1

		if (race == null || race == "" || (largeur % 2 != 1)
				|| (hauteur % 2 != 1)) {
			throw new PreconditionError(
					"init(race,largeur,hauteur,force,vitesse,pointsVie) require race <> \"\" && largeur%2=1 && hauteur%2=1 incorrecte");
		}

		if (position == null) {
			throw new PreconditionError(
					" pre init(s,l,h,f,v,p,pos) require pos <> \"\" ");
		} else if (position.x <= 0) {
			throw new PreconditionError(
					" pre init(s,l,h,f,v,p,pos) require pos.x >0 incorrect ");
		} else if (position.y <= 0) {
			throw new PreconditionError(
					" pre init(s,l,h,f,v,p,pos) require pos.y >0 incorrect ");
		}
		super.init(race, largeur, hauteur, force, vitesse, pointsVie, position);

		this.checkInvariant();

		// Post

		if (hauteur() != hauteur) {
			throw new PostconditionError(
					"hauteur(init(s,l,h,f,v,p,pos))=h incorrecte");
		}

		if (largeur() != largeur) {
			throw new PostconditionError(
					"largeur(init(s,l,h,f,v,p,pos))=l incorrecte");
		}

		if (force() != force) {
			throw new PostconditionError(
					"force(init(s,l,h,f,v,p,pos))=f incorrecte");
		}

		if (race() != RACE.valueOf(race))
			throw new PostconditionError(
					"race (init(s,l,h,f,v,p,pos))=r incorrecte");

		if (vitesse() != vitesse) {
			throw new PostconditionError(
					"vittese (init(s,l,h,f,v,p,pos))=v incorrecte");
		}

		if (quantiteOr() != 0) {
			throw new PostconditionError(
					"quantiteDor(init(s,l,h,f,v,p,pos))=0 incorrecte");
		}

		if (pointDeVie() != pointsVie) {
			throw new PostconditionError(
					"pointDeVie(init(s,l,h,f,v,p,pos))=p incorrecte");
		}

		if (position.x != getX()) {
			throw new PostconditionError(
					"post getX(init(s,l,h,f,v,p,pos)) = pos.x incorect");
		}

		if (position.y != getY()) {
			throw new PostconditionError(
					"post getY(init(s,l,h,f,v,p,pos)) = pos.y incorect");
		}

		if (resteDansLaMinePourPasDeJeu() > 0) {
			throw new PostconditionError(
					"post resteDansLaMinePourPasDeJeu(init(s,l,h,f,v,p,pos)) <=0 incorect");
		}
	}

	@Override
	public void retrait(int s) {
		// pre retrait(Villageois,s) require !estMort(Villageois) && s>0

		if (estMort() || s <= 0) {
			throw new PreconditionError(
					"pre retrait(Villageois,s) require !estMort(Villageois) && s>0 incorrecte");
		}
		// checkInvariant
		checkInvariant();
		// Capture

		int quantiteDor_pre = quantiteOr();
		int pointsDeVie_pre = pointDeVie();
		int x_pre = getX();
		int y_pre = getY();

		int pasJeu_pre = resteDansLaMinePourPasDeJeu();

		super.retrait(s);

		// checkInvariant
		checkInvariant();

		// post

		if (pointDeVie() != (pointsDeVie_pre - s)) {
			throw new PostconditionError(
					"post pointsDeVie(retrait(Villageois,s))=pointsDeVie(Villageois) -s incorrecte");
		}

		if (quantiteDor_pre != quantiteOr()) {
			throw new PostconditionError(
					"post quantiteOr(retrait(Villageois,s))=quantiteOr(Villageois) incorrecte");
		}

		if (pasJeu_pre != resteDansLaMinePourPasDeJeu()) {
			throw new PostconditionError(
					" post resteDansLaMinePourPasDeJeu(retrait(Villageois,s)) = resteDansLaMinePourPasDeJeu(Villageois) incorrect");
		}
		if (x_pre != getX()) {
			throw new PostconditionError(
					" post getX(retrait(Villageois,s)) = getX(Villageois) incorrect");
		}

		if (y_pre != getY()) {
			throw new PostconditionError(
					" post getY(retrait(Villageois,s)) = getY(Villageois) incorrect");
		}
	}

	@Override
	public void setX(int x) {
		if (x <= 0) {
			throw new PreconditionError("pre setX(V, x) require x > 0");
		}

		checkInvariant();

		// Capture

		int quantiteDor_pre = quantiteOr();
		int pointsDeVie_pre = pointDeVie();
		int y_pre = getY();

		int pasJeu_pre = resteDansLaMinePourPasDeJeu();

		super.setX(x);

		checkInvariant();

		// post

		if (getX() != x) {
			throw new PostconditionError(
					"post getX(setX(Villageois, x) = x incorrect");
		}

		if (pasJeu_pre != resteDansLaMinePourPasDeJeu()) {
			throw new PostconditionError(
					" post resteDansLaMinePourPasDeJeu(setX(Villageois,x)) = resteDansLaMinePourPasDeJeu(Villageois) incorrect");
		}

		if (pointDeVie() != pointsDeVie_pre) {
			throw new PostconditionError(
					"post pointsDeVie(setX(Villageois,x))=pointsDeVie(Villageois)incorrecte");
		}

		if (quantiteDor_pre != quantiteOr()) {
			throw new PostconditionError(
					"post quantiteOr(setX(Villageois,x))=quantiteOr(Villageois) incorrecte");
		}

		if (getY() != y_pre) {
			throw new PostconditionError(
					"post getY(setX(Villageois, x) = getY(Villageois) incorrect");
		}

	}

	@Override
	public void setY(int y) {
		if (y <= 0) {
			throw new PreconditionError("pre setX(V, y) require y > 0");
		}

		checkInvariant();

		// Capture

		int quantiteDor_pre = quantiteOr();
		int pointsDeVie_pre = pointDeVie();
		int x_pre = getX();
		int pasJeu_pre = resteDansLaMinePourPasDeJeu();
		// operations
		super.setY(y);

		checkInvariant();

		// post

		if (super.getY() != y) {
			throw new PostconditionError(
					"post getY(setY(Villageois, y) = y incorrect");
		}

		if (pasJeu_pre != resteDansLaMinePourPasDeJeu()) {
			throw new PostconditionError(
					" post resteDansLaMinePourPasDeJeu(setY(Villageois,y)) = resteDansLaMinePourPasDeJeu(Villageois) incorrect");
		}

		if (pointDeVie() != pointsDeVie_pre) {
			throw new PostconditionError(
					"post pointsDeVie(setY(Villageois,x))=pointsDeVie(Villageois)incorrecte");
		}

		if (quantiteDor_pre != quantiteOr()) {
			throw new PostconditionError(
					"post quantiteOr(setY(Villageois,y))=quantiteOr(Villageois) incorrecte");
		}

		if (getX() != x_pre) {
			throw new PostconditionError(
					"post getX(setY(Villageois, y) = getX(Villageois) incorrect");
		}

	}

	@Override
	public void miner() {
		// pre
		if (!estEnTrainDeMiner()) {
			throw new PreconditionError(
					" pre miner(V) requires estEnTrainDeMiner(V) incorrect");
		}

		// capture
		int pointsDeVie_pre = pointDeVie();
		int quantiteDor_pre = quantiteOr();
		int y_pre = getY();
		int x_pre = getX();
		int reste_pre = resteDansLaMinePourPasDeJeu();


		checkInvariant();
		
		super.miner();

		checkInvariant();
		// post

		if(reste_pre!=(resteDansLaMinePourPasDeJeu()-1)) {
			throw new PostconditionError("post resteDansLaMinePourPasDeJeu(miner(V)) = resteDansLaMinePourPasDeJeu(V)-1 incorrect");
		}
		if (pointDeVie() != pointsDeVie_pre) {
			throw new PostconditionError(
					"post pointsDeVie(miner(V))= pointsDeVie(V) incorrecte");
		}

		if (quantiteDor_pre != quantiteOr()) {
			throw new PostconditionError(
					"post quantiteOr (miner(V))=quantiteOr (V) incorrecte");
		}

		if (getX() != x_pre) {
			throw new PostconditionError(
					"post getX(miner(V)) = getX(V) incorrect");
		}

		if (getY() != y_pre) {
			throw new PostconditionError(
					"post getY(miner(V)) = getY(V) incorrect");
		}

	}

	@Override
	public void entrerMine() {
		// pre
		if (estEnTrainDeMiner()) {
			throw new PreconditionError(
					" pre entrerMine(V) requires !estEnTrainDeMiner(V) incorrect");
		}
		
		int pointsDeVie_pre = pointDeVie();
		int quantiteDor_pre = quantiteOr();
		int y_pre = getY();
		int x_pre = getX();
		
		checkInvariant();
		// operations
		super.entrerMine();
		
		checkInvariant();
		// post
		if(resteDansLaMinePourPasDeJeu()!=16) {
			throw new PostconditionError("post resteDansLaMinePourPasDeJeu(entrerMine(V)) = 16 incorrect");
		}
		if (pointDeVie() != pointsDeVie_pre) {
			throw new PostconditionError(
					"post pointsDeVie(entrerMine(V))= pointsDeVie(V) incorrecte");
		}

		if (quantiteDor_pre != quantiteOr()) {
			throw new PostconditionError(
					"post quantiteOr(entrerMine(V))=quantiteOr(V) incorrecte");
		}

		if (getX() != x_pre) {
			throw new PostconditionError(
					"post getX(entrerMine(V)) = getX(V) incorrect");
		}

		if (getY() != y_pre) {
			throw new PostconditionError(
					"post getY(entrerMine(V)) = getY(V) incorrect");
		}		
	}
	
	@Override
	public void sortirMine() {
		// pre
		if (!estEnTrainDeMiner()) {
			throw new PreconditionError(
					" pre sortirMine(V) requires estEnTrainDeMiner(V) incorrect");
		}
		
		int pointsDeVie_pre = pointDeVie();
		int quantiteDor_pre = quantiteOr();
		int y_pre = getY();
		int x_pre = getX();
		
		checkInvariant();
		// operations
		super.sortirMine();
		
		checkInvariant();
		// post
		if(resteDansLaMinePourPasDeJeu()>0) {
			throw new PostconditionError("post resteDansLaMinePourPasDeJeu(sortirMine(V)) = 0 incorrect");
		}
		if (pointDeVie() != pointsDeVie_pre) {
			throw new PostconditionError(
					"post pointsDeVie(sortirMine(V))= pointsDeVie(V) incorrecte");
		}

		if (quantiteDor_pre != quantiteOr()) {
			throw new PostconditionError(
					"post quantiteOr(sortirMine(V))=quantiteOr(V) incorrecte");
		}

		if (getX() != x_pre) {
			throw new PostconditionError(
					"post getX(sortirMine(V)) = getX(V) incorrect");
		}

		if (getY() != y_pre) {
			throw new PostconditionError(
					"post getY(sortirMine(V)) = getY(V) incorrect");
		}		
	}
	
	@Override
	public void remplirPoches(int i) {
		if(i<=0) {
			throw new PreconditionError(" pre remplirPoches(i) requires i >0 incorrect");
		}
		
		//Invariant
		checkInvariant();
		
		// Capture
		int pointsDeVie_pre = pointDeVie();
		int quantiteDor_pre = quantiteOr();
		int y_pre = getY();
		int x_pre = getX();
		int reste_pre = resteDansLaMinePourPasDeJeu();
		
		super.remplirPoches(i);
		
		//post
		checkInvariant();
		
		//post
		
		if(resteDansLaMinePourPasDeJeu()!=reste_pre) {
			throw new PostconditionError("post resteDansLaMinePourPasDeJeu(remplirPoches(V,i)) = resteDansLaMinePourPasDeJeu(V) incorrect");
		}
		if (pointDeVie() != pointsDeVie_pre) {
			throw new PostconditionError(
					"post pointsDeVie(remplirPoches(V,i))= pointsDeVie(V) incorrecte");
		}

		if ((quantiteDor_pre+i) != quantiteOr()) {
			throw new PostconditionError(
					"post quantiteOr (remplirPoches(V,i))=quantiteOr(V)+i incorrecte");
		}

		if (getX() != x_pre) {
			throw new PostconditionError(
					"post getX(remplirPoches(V,i)) = getX(V) incorrect");
		}

		if (getY() != y_pre) {
			throw new PostconditionError(
					"post getY(remplirPoches(V,i)) = getY(V) incorrect");
		}		
	}
	
	@Override
	public void viderPoches() {
		
		//
		if(quantiteOr()<=0) {
			throw new PreconditionError(" pre viderPoches(V) requires quantiteOr(V)>0 incorrect");
		}
		//Invariant
		checkInvariant();
		
		// Capture
		int pointsDeVie_pre = pointDeVie();
		int y_pre = getY();
		int x_pre = getX();
		int reste_pre = resteDansLaMinePourPasDeJeu();
		
		super.viderPoches();
		
		//post
		checkInvariant();
		
		//post
		
		if(resteDansLaMinePourPasDeJeu()!=reste_pre) {
			throw new PostconditionError("post resteDansLaMinePourPasDeJeu(viderPoches(V)) = resteDansLaMinePourPasDeJeu(V) incorrect");
		}
		if (pointDeVie() != pointsDeVie_pre) {
			throw new PostconditionError(
					"post pointsDeVie(viderPoches(V))= pointsDeVie(V) incorrecte");
		}

		if (quantiteOr()!=0) {
			throw new PostconditionError(
					"post quantiteOr (viderPoches(V))=quantiteOr(V)+i incorrecte");
		}

		if (getX() != x_pre) {
			throw new PostconditionError(
					"post getX(viderPoches(V)) = getX(V) incorrect");
		}

		if (getY() != y_pre) {
			throw new PostconditionError(
					"post getY(viderPoches(V)) = getY(V) incorrect");
		}		
	}
}
