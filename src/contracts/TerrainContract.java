package contracts;

import java.util.Set;

import implementation.Terrain;
import services.IMurrailleService;
import services.IMyObjectService;
import services.ITerrainJeuxService;
import decorators.TerrainJeuDecorator;
import enums.OBJECTS;
import errors.InvariantError;
import errors.PostconditionError;
import errors.PreconditionError;

//TODO
public class TerrainContract extends TerrainJeuDecorator implements
		ITerrainJeuxService {

	public TerrainContract(ITerrainJeuxService terrain) {
		super(terrain);
	}

	public void checkInvariant() {
		// No invariant
		if(numerosMurailles().size()!=nbMurailles()) {
			throw new InvariantError(" Invariant nbMurailles(T) = (min) |numerosMurailles(T)| incorrect ");
		}
	}

	@Override
	public void init(int largeur, int hauteur) {
		// pre
		if (largeur <= 0 || hauteur <= 0) {
			throw new PreconditionError(
					"pre init(l,h) requires l >0 ^ h > 0 incorrect");
		}

		super.init(largeur, hauteur);

		checkInvariant();

		// post
		if (largeur() != largeur) {
			throw new PostconditionError(
					"post largeur(init(l,h))== l incorrect");
		}

		if (hauteur != hauteur()) {
			throw new PostconditionError(
					"post hauteur(init(l,h))== h incorrect");
		}

		if(numerosMurailles().size()>0) {
			throw new PostconditionError("post numerosMurailles(init(l,h)) = VIDE incorrect");
		}
		
		if(numerosMines().size()>0) {
			throw new PostconditionError("post numeroMines(init(l,h)) = VIDE incorrect");
		}
		
		for (int i = 0; i < largeur(); i++) {
			for (int j = 0; j < hauteur(); j++) {
				if (super.getObjectAt(i, j) != OBJECTS.RIEN) {
					throw new PostconditionError(
							" post getObjectAt(init(largeur,hauteur),i,j)==RIEN incorrect");
				}
			}
		}
	}

	@Override
	public OBJECTS getObjectAt(int x, int y) {
		// pre

		if (x < 0 || x > largeur()) {
			throw new PreconditionError(
					" pre getObjectAt(x,y) requires 0<=x<largeur() incorrect");
		}

		if (y < 0 || y > hauteur()) {
			throw new PreconditionError(
					"pre getObjectAt(x,y) requires 0<=y<hauteur() incorrect");
		}
		// aucun Invariant
		// post aucun
		return super.getObjectAt(x, y);
	}

	@Override
	public void setObjectAt(int x, int y, IMyObjectService object) {
		int x_test = x + object.largeur();
		int y_test = y + object.hauteur();
		
		if (object == null) {
			throw new PreconditionError(
					"pre setObjectAt(x,y,o) requires o <> null incorrect");
		}
		
		if (x_test < 0 || x_test > largeur()) {
			throw new PreconditionError(
					" pre setObjectAt(x,y,o) requires 0<=x + object.largeur() <largeur() incorrect");
		}

		if (y_test < 0 || y_test > hauteur()) {
			throw new PreconditionError(
					"pre setObjectAt(x,y,o) requires 0<=y + object.hauteur() <hauteur() incorrect");
		}
		// aucun Invariant

		

		if (Terrain.getObjectType(object) == OBJECTS.RIEN) {
			throw new PreconditionError(
					"pre setObjectAt(x,y,o) requires o in {VILLAGEOIS, MINE,HOTELVILLE, ROUTE, MURAILLE} incorrect");
		}

		// Capture
		Set<Integer> nmb_pre = numerosMurailles();
		Set<Integer> nb_mines_pre = numerosMines();
		super.setObjectAt(x, y, object);

		checkInvariant();
		// Post

		if (!nmb_pre.equals(numerosMurailles()) ) {
			throw new PreconditionError(
					" post numerosMurailles(setObjectAt(x,y,o)) = numerosMurailles(T) incorrect");
		}

		if (!nb_mines_pre.equals(numerosMines()) ) {
			throw new PreconditionError(
					" post numerosMines(setObjectAt(x,y,o)) = numerosMines(T) incorrect");
		}
		for (int i = 0; i < object.largeur(); i++) {
			for (int j = 0; j < object.hauteur(); j++) {
				if (super.getObjectAt(x + i, y + j) != Terrain
						.getObjectType(object)) {
					throw new PostconditionError(
							"post getObjectAt(setObjectAt(x,y,o),x+i,y+j)== OBJECT::myType(o) incorrect");
				}
			}
		}
	}

	@Override
	public void setObjectAt(int x, int y, OBJECTS type) {
		// pre aucun
		if (x < 0 || x > largeur()) {
			throw new PreconditionError(
					" pre setObjectAt(x,y,o) requires 0<=x<largeur() incorrect");
		}

		if (y < 0 || y > hauteur()) {
			throw new PreconditionError(
					"pre setObjectAt(x,y,o) requires 0<=y<hauteur() incorrect");
		}

		if(type == null) {
			throw new PreconditionError(" pre setObjectAt(x,y,type) requires type <> null incorrect");
		}
		
		// aucun Invariant

		checkInvariant();

		// Capture
		Set<Integer> nmb_pre = numerosMurailles();
		Set<Integer> nb_mines_pre = numerosMines();
		
		super.setObjectAt(x, y, type);

		checkInvariant();
		// Post
		if (nmb_pre != numerosMurailles()) {
			throw new PreconditionError(
					" post numerosMurailles(setObjectAt(x,y,type)) = numerosMurailles(T) incorrect");
		}

		if (!nb_mines_pre.equals(numerosMines()) ) {
			throw new PreconditionError(
					" post numerosMines(setObjectAt(x,y,type)) = numerosMines(T) incorrect");
		}
		
		if (super.getObjectAt(x, y) != type) {
			throw new PostconditionError(
					"post getObjectAt(setObjectAt(x,y,o),x,y)== OBJECT::myType(o) incorrect");
		}
	}

	@Override
	public IMurrailleService getMuraille(int id) {
		// pre
		if (!numerosMurailles().contains(id)) {
			throw new PreconditionError(
					" pre getMuraille(T,id) requires id exists in numerosMurailles(T) incorrect ");
		}

		return super.getMuraille(id);
	}

	@Override
	public void detruireMuraille(int id) {
		// pre
		if (!numerosMurailles().contains(id)) {
			throw new PreconditionError(
					" pre detruireMuraille(T, id) requires id exists in numerosMurailles(T) incorrect ");
		}

		checkInvariant();
		
		// capture
		Set<Integer> nb_mines_pre = numerosMines();
		
		IMurrailleService murals = super.getMuraille(id);
		
		int x = murals.getX();
		int y = murals.getY();
		
		super.detruireMuraille(id);
		
		checkInvariant();
		
		if (!nb_mines_pre.equals(numerosMines()) ) {
			throw new PreconditionError(
					" post numerosMines(detruireMuraille(id)) = numerosMines(T) incorrect");
		}
		
		if(numerosMurailles().contains(id)) {
			throw new PostconditionError(" post id not in  numerosMurailles() incorrect");
		}
		
		for (int i = 0; i < murals.largeur(); i++) {
			for (int j = 0; j < murals.hauteur(); j++) {
				if (super.getObjectAt(x + i, y + j) != OBJECTS.RIEN) {
					throw new PostconditionError(
							"post getObjectAt(detruireMuraille(id),x+i,y+j)== RIEN incorrect");
				}
			}
		}
		
	}
}
