package contracts;

import java.awt.Point;

import services.IRouteService;
import decorators.RouteDecorator;
import errors.PostconditionError;
import errors.PreconditionError;

public class RouteContract extends RouteDecorator implements IRouteService {
	public RouteContract(IRouteService route) {
		super(route);
	}
	
	public void checkInvariant() {
		
	}
	
	@Override
	public void init(Point position, int largeur, int hauteur) {
		//pre
		
		if(position ==null) {
			throw new PreconditionError(" pre init(pos,l,h) requires pos<> null incorrect");
		}
		
		int x = position.x;
		int y = position.y;
		
		if(x <=0 || y <=0) {
			throw new PreconditionError(" pre init(pos,l,h) requires pos.x >0 ^ pos.y >0 incorrect");
		}
		 
		if(largeur<=0) {
			throw new PreconditionError("pre init(pos,l,h) requires largeur > 0 incorrect");
		}
		
		
		if(hauteur<=0) {
			throw new PreconditionError("pre init(pos,l,h) requires hauteur > 0 incorrect");
		}
		
		// Operations
		super.init(position,largeur,hauteur);
		
		//Invariant
		checkInvariant();
		
		// Post
		
		if(position.x != getX()) {
			throw new PostconditionError("post getX(init orientation(init(o,m,pos)) = pos.x incorect");
		}
		
		if(position.y != getY()) {
			throw new PostconditionError("post getY(init orientation(init(o,m,pos)) = pos.y incorect");
		}
	}
	
	@Override
	public void setX(int x) {
		if (x <= 0) {
			throw new PreconditionError("pre setX(V, x) require x > 0");
		}

		checkInvariant();

		// Capture

		int y_pre= getY();
		
		super.setX(x);

		checkInvariant();

		// post

		if (super.getX() != x) {
			throw new PostconditionError(
					"post getX(setX(Villageois, x) = x incorrect");
		}
		
		if(y_pre!=getY()) {
			throw new PostconditionError("post getY(setY(R, x)) = getY(R) incorrect");
		}
	}

	@Override
	public void setY(int y) {
		if (y <= 0) {
			throw new PreconditionError("pre setX(V, y) require y > 0");
		}

		checkInvariant();

		// Capture

		int x_pre= getX();
		
		super.setY(y);

		checkInvariant();

		// post

		if (super.getY() != y) {
			throw new PostconditionError(
					"post getY(setY(Villageois, y) = y incorrect");
		}
		
		if(x_pre!=getX()) {
			throw new PostconditionError("post getX(setY(R, y)) = getX(R) incorrect");
		}
	}
}
