package contracts;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import services.IHotelVilleService;
import services.IMineService;
import services.IMoteurJeuService;
import services.IMurrailleService;
import services.ITerrainJeuxService;
import services.IVillageoisService;
import decorators.MoteurJeuDecorator;
import enums.COMMAND;
import enums.OBJECTS;
import enums.RESULTAT;
import errors.InvariantError;
import errors.PostconditionError;
import errors.PreconditionError;

public class MoteurJeuContract extends MoteurJeuDecorator {

	// Constructors
	
	public MoteurJeuContract(IMoteurJeuService delegate) {
		super(delegate);
	}

	// Observators
	
	@Override
	public int largeurTerrain() {
		checkInvariants();
		final int largeur =  super.largeurTerrain();
		checkInvariants();
		
		return largeur;
	}

	@Override
	public int hauteurTerrain() {
		checkInvariants();
		final int hauteur = super.hauteurTerrain();
		checkInvariants();
		
		return hauteur;
	}

	@Override
	public int maxPasJeu() {
		checkInvariants();
		final int maxPasJeu = super.maxPasJeu();
		checkInvariants();
		
		return maxPasJeu;
	}

	@Override
	public int pasJeuCourant() {
		checkInvariants();
		
		final int pasJeuCourant = super.pasJeuCourant();
		
		checkInvariants();
		
		return pasJeuCourant;
	}

	@Override
	public boolean estFini() {
		checkInvariants();
		
		final boolean estFini = super.estFini();
		
		checkInvariants();
		
		return estFini;
	}

	@Override
	public RESULTAT resultatFinal() {
		checkInvariants();
		
		if(!super.estFini())
			throw new PreconditionError("La partie n'est pas terminée.");
		
		final RESULTAT resultatFinal = super.resultatFinal();
		
		checkInvariants();
		
		return resultatFinal;
	}

	@Override
	public Set<Integer> numerosVillageois() {
		checkInvariants();
		final Set<Integer> numerosVillageois = super.numerosVillageois();
		checkInvariants();
		
		return numerosVillageois;
	}

	@Override
	public IVillageoisService getVillageois(int numeroVillageois) {
		checkInvariants();
		
		checkNumeroVillageois(numeroVillageois);
		
		final IVillageoisService villageois = super.getVillageois(numeroVillageois);
		
		checkInvariants();
		
		return villageois;
	}

	@Override
	public int positionVillageoisX(int numeroVillageois) {
		checkInvariants();
		
		checkNumeroVillageois(numeroVillageois);
		
		final int positionVillageoisX = super.positionVillageoisX(numeroVillageois);
		
		checkInvariants();
		
		return positionVillageoisX;
	}

	@Override
	public int positionVillageoisY(int numeroVillageois) {
		checkInvariants();
		
		checkNumeroVillageois(numeroVillageois);
		
		final int positionVillageoisY = super.positionVillageoisY(numeroVillageois);
		
		checkInvariants();
		
		return positionVillageoisY;
	}

	@Override
	public Set<Integer> numerosMines() {
		checkInvariants();
		
		final Set<Integer> numerosMines = super.numerosMines();
		
		checkInvariants();
		
		return numerosMines;
	}

	@Override
	public IMineService getMine(int numeroMine) {
		checkInvariants();
		
		checkNumeroMine(numeroMine);
		
		final IMineService mine = super.getMine(numeroMine);
		
		checkInvariants();
		
		return mine;
	}

	@Override
	public int positionMineX(int numeroMine) {
		checkInvariants();
		
		checkNumeroMine(numeroMine);
		
		final int positionMineX = super.positionMineX(numeroMine);
		
		checkInvariants();
		
		return positionMineX;
	}

	@Override
	public int positionMineY(int numeroMine) {
		checkInvariants();
		
		checkNumeroMine(numeroMine);
		
		final int positionMineY = super.positionMineY(numeroMine);
		
		checkInvariants();
		
		return positionMineY;
	}
	
	@Override
	public Set<Integer> numerosMurailles() {
		checkInvariants();
		
		final Set<Integer> numerosMurailles = super.numerosMurailles();
		
		checkInvariants();
		
		return numerosMurailles;
	}

	@Override
	public IMurrailleService getMuraille(int numeroMuraille) {
		checkInvariants();
		
		checkNumeroMuraille(numeroMuraille);
		
		final IMurrailleService muraille = super.getMuraille(numeroMuraille);
		
		checkInvariants();
		
		return muraille;
	}

	@Override
	public int positionMurailleX(int numeroMuraille) {
		checkInvariants();
		
		checkNumeroMuraille(numeroMuraille);
		
		final int positionMurailleX = super.positionMurailleX(numeroMuraille);
		
		checkInvariants();
		
		return positionMurailleX;
	}

	@Override
	public int positionMurailleY(int numeroMuraille) {
		checkInvariants();
		
		checkNumeroMuraille(numeroMuraille);
		
		final int positionMurailleY = super.positionMurailleY(numeroMuraille);
		
		checkInvariants();
		
		return positionMurailleY;
	}

	@Override
	public IHotelVilleService hotelDeVille() {
		checkInvariants();
		
		final IHotelVilleService hotelDeVille = super.hotelDeVille();
		
		checkInvariants();
		
		return hotelDeVille;
	}

	@Override
	public int positionHotelVilleX() {
		checkInvariants();
		final int positionHotelVilleX = super.positionHotelVilleX();
		checkInvariants();
		
		return positionHotelVilleX;
	}

	@Override
	public int positionHotelVilleY() {
		checkInvariants();
		final int positionHotelVilleY = super.positionHotelVilleY();
		checkInvariants();
		
		return positionHotelVilleY;
	}

	@Override
	public boolean peutEntrerMine(int numeroVillageois, int numeroMine) {
		checkInvariants();
		
		checkNumeroVillageois(numeroVillageois);
		checkNumeroMine(numeroMine);
		
		final boolean peutEntrer = super.peutEntrerMine(numeroVillageois, numeroMine);
		
		checkInvariants();
		
		return peutEntrer;
	}

	@Override
	public boolean peutEntrerHotelVille(int numeroVillageois) {
		checkInvariants();
		
		checkNumeroVillageois(numeroVillageois);
		
		final boolean peutEntrerHotelVille = super.peutEntrerHotelVille(numeroVillageois);
		
		checkInvariants();
		
		return peutEntrerHotelVille;
	}

	@Override
	public void init(int maxPasJeu) {
		if(maxPasJeu < 0)
			throw new PreconditionError("\"maxPas\" doit être supérieur à 0");
		
		super.init(maxPasJeu);
		
		checkInvariants();
		
		if(super.maxPasJeu() != maxPasJeu)
			throw new PostconditionError("\"maxPasJeu()\" est différent de \"maxPasJeu\"");
		
		if(super.pasJeuCourant() != 0)
			throw new PostconditionError("\"pasJeuCourant()\" est différent de 0");
		
		if(maxPasJeu == 0) {
			if(!super.estFini())
				throw new PostconditionError("\"maxPasJeu\" est à zéro mais le jeu n'est pas finit");
		}
		else {
			if(super.estFini())
				throw new PostconditionError("\"maxPasJeu\" n'est pas à zéro mais le jeu est finit");
		}
	}

	// Operators
	
	@Override
	public void pasJeu(COMMAND commande, int numeroVillageois, int argument) {
		checkInvariants();
		
		if(super.estFini())
			throw new PreconditionError("Opération \"pasJeu(COMMAND, int, int)\" impossible, le jeu est déjà finit.");
		
		if(!super.numerosVillageois().contains(numeroVillageois))
			throw new PreconditionError("Le numéro de Villageois n'est aps valide !");
		
		int prePasjeuCourant = super.pasJeuCourant();
		Map<Integer, Integer[]> prePositions = new HashMap<Integer, Integer[]>();
		for(int i : numerosVillageois())
			prePositions.put(i, new Integer[]{super.positionVillageoisX(i), super.positionVillageoisY(i)});
		
		OBJECTS[][] preMatrice = new OBJECTS[super.terrain().largeur()][super.terrain().hauteur()];
		for(int i = 0; i < super.terrain().largeur(); i++)
			for(int j = 0; j < super.terrain().hauteur(); j++)
				preMatrice[i][j] = super.terrain().getObjectAt(i, j);
		
		switch(commande) {
			case ATTAQUERMURAILLE:
				int numeroMuraille = argument;
				
				if(!super.numerosMurailles().contains(numeroMuraille))
					throw new PreconditionError("Le numéro de Muraille est invalide");
				
				if(!super.peutAttaquerMuraille(numeroVillageois, numeroMuraille))
					throw new PreconditionError("Le Villageois n°" + numeroVillageois + " ne peut pas attaquer la Muraille n°" + numeroMuraille);
				
				int prePointVie = super.getMuraille(numeroMuraille).pointDeVie();
				int preLargeur = super.getMuraille(numeroMuraille).largeur();
				int preHauteur = super.getMuraille(numeroMuraille).hauteur();
				Integer[] prePositionMuraille = new Integer[]{super.terrain().getMuraille(numeroMuraille).getX(), super.terrain().getMuraille(numeroMuraille).getY()};
				
				super.pasJeu(commande, numeroVillageois, numeroMuraille);
				
				checkInvariants();
				
				if(prePointVie <= super.getVillageois(numeroVillageois).force()) {
					if(super.terrain().getObjectAt(prePositionMuraille[0], prePositionMuraille[1]) != OBJECTS.RIEN)
						throw new PostconditionError("La Muraille d'indentifiant " + numeroMuraille + " n'a pas été correctement détruite.");
					
					if(super.numerosMurailles().contains(numeroMuraille))
						throw new PostconditionError("La Muraille d'indentifiant " + numeroMuraille + " n'a pas été correctement détruite.");
				}
				else {
					if(super.terrain().getMuraille(numeroMuraille).pointDeVie() != prePointVie - super.getVillageois(numeroVillageois).force())
						throw new PostconditionError("La Muraille d'indentifiant " + numeroMuraille + " n'a pas été correctement endommagée.");
				}
				
				checkIfPositionsAreSame(prePositions);
				
				for(int i = 0; i < super.terrain().largeur(); i++) {
					for(int j = 0; j < super.terrain().hauteur(); j++) {
						if(!(prePositionMuraille[0] <= i && i <= preLargeur) && !(prePositionMuraille[1] <= j && j <= preHauteur))
							if(super.terrain().getObjectAt(i, j) != preMatrice[i][j])
								throw new PostconditionError("L'objet à l'indice [" + i + "][" + j + "] a été modifié");
					}
				}
				
				checkPasJeu(prePasjeuCourant);
				
				break;
				
			case DEPLACER:
				int angle = argument;
				
				if(!((0 <= angle) && (angle <= 360)))
					throw new PreconditionError("L'angle n'est pas compris entre 0 et 360 degrés");
				
				IVillageoisService villageois = super.getVillageois(numeroVillageois);
				ITerrainJeuxService terrain = super.terrain();
				int largeurVillageois = villageois.largeur();
				int hauteurVillageois = villageois.hauteur();
				int largeurTerrain = terrain.largeur();
				int hauteurTerrain = terrain.hauteur();
				int preXVillageois = villageois.getX();
				int preYVillageois = villageois.getY();
				
				// déplacement droite
				if((0 <= angle && angle < 45) || (315 < angle && angle <= 360)) {
					if(!(preXVillageois + largeurVillageois < largeurTerrain))
						throw new PreconditionError("Déplacement à droite impossible, le Villageois va sortir du Terrain");
					
					if(estSurRoute(numeroVillageois)) {
						if(preXVillageois + 2 > largeurTerrain) {
							if(!estAtteignable(numeroVillageois, angle, 1, 0))
								throw new PreconditionError("Déplacement à droite impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois + 1, preYVillageois);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 1, 0);
						}
						else {
							if(!estAtteignable(numeroVillageois, angle, 1, 0) || !estAtteignable(numeroVillageois, angle, 2, 0))
								throw new PreconditionError("Déplacement à droite impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois + 2, preYVillageois);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 2, 0);
						}
						
					}
					else {
						if(!estAtteignable(numeroVillageois, angle, 1, 0))
							throw new PreconditionError("Déplacement à droite impossible, la case n'est pas libre");
						
						terrain.getObjectAt(preXVillageois + 1, preYVillageois);
						
						super.pasJeu(commande, numeroVillageois, angle);
						
						checkInvariants();
						
						checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 1, 0);
					}
				}
				// déplacement diagonale haut droite
				else if(angle == 45) {
					if(!((preXVillageois + largeurVillageois < largeurTerrain)&&(preYVillageois > 0)))
						throw new PreconditionError("Déplacement en diagonale haute droite impossible, le Villageois va sortir du Terrain");
					
					if(estSurRoute(numeroVillageois)) {
						if(preXVillageois + 2 > largeurTerrain || preYVillageois - 2 < 0) {
							if(!estAtteignable(numeroVillageois, angle, 1, -1))
								throw new PreconditionError("Déplacement en diagonale haute droite impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois + 1, preYVillageois - 1);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 1, -1);
						}
						else {
							if(!estAtteignable(numeroVillageois, angle, 1,  -1) || !estAtteignable(numeroVillageois, angle, 2,  -2))
								throw new PreconditionError("Déplacement en diagonale haute droite impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois + 2, preYVillageois - 2);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 2, -2);
						}
						
					}
					else {
						if(!estAtteignable(numeroVillageois, angle, 1, -1))
							throw new PreconditionError("Déplacement en diagonale haute droite impossible, la case n'est pas libre");
						
						terrain.getObjectAt(preXVillageois + 1, preYVillageois - 1);
						
						super.pasJeu(commande, numeroVillageois, angle);
						
						checkInvariants();
						
						checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 1, -1);
					}
				}
				// déplacement haut
				else if(45 < angle && angle < 135) {
					if(!(preYVillageois > 0))
						throw new PreconditionError("Déplacement en haut impossible, le Villageois va sortir du terrain");
					
					if(estSurRoute(numeroVillageois)) {
						if(preYVillageois - 2 < 0) {
							if(!estAtteignable(numeroVillageois, angle, 0, -1))
								throw new PreconditionError("Déplacement en haut impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois, preYVillageois - 1);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 0, -1);
						}
						else {
							if(!estAtteignable(numeroVillageois, angle, 0,  -1) || !estAtteignable(numeroVillageois, angle, 0,  -2))
								throw new PreconditionError("Déplacement en haut impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois, preYVillageois - 2);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 0, -2);
						}
						
					}
					else {
						if(!estAtteignable(numeroVillageois, angle, 0, -1))
							throw new PreconditionError("Déplacement en haut impossible, la case n'est pas libre");
						
						terrain.getObjectAt(preXVillageois, preYVillageois - 1);
						
						super.pasJeu(commande, numeroVillageois, angle);
						
						checkInvariants();
						
						checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 0, -1);
					}
				}
				// déplacement diagonale haut gauche
				else if(angle == 135) {
					if(!((preYVillageois > 0) && (preXVillageois > 0)))
						throw new PreconditionError("Déplacement diagonale haute gauche impossible, le Villageosi va sortir du terrain");
					
					if(estSurRoute(numeroVillageois)) {
						if(preYVillageois - 2 < 0 || preXVillageois - 2 < 0) {
							if(!estAtteignable(numeroVillageois, angle, -1, -1))
								throw new PreconditionError("Déplacement diagonale haute gauche impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois - 1, preYVillageois - 1);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, -1, -1);
						}
						else {
							if(!estAtteignable(numeroVillageois, angle, -1,  -1) || !estAtteignable(numeroVillageois, angle, -2,  -2))
								throw new PreconditionError("Déplacement diagonale haute gauche impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois - 2, preYVillageois - 2);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, -2, -2);
						}
						
					}
					else {
						if(!estAtteignable(numeroVillageois, angle, -1, -1))
							throw new PreconditionError("Déplacement diagonale haute gauche impossible, la case n'est pas libre");
						
						terrain.getObjectAt(preXVillageois - 1, preYVillageois - 1);
						
						super.pasJeu(commande, numeroVillageois, angle);
						
						checkInvariants();
						
						checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, -1, -1);
					}
					
				}
				// déplacement gauche
				else if(135 < angle && angle < 225) {
					if(!(preXVillageois > 0))
						throw new PreconditionError("Déplacement gauche impossible, le Villageois va sortir du terrain");
					
					if(estSurRoute(numeroVillageois)) {
						if(preXVillageois - 2 < 0) {
							if(!estAtteignable(numeroVillageois, angle, -1, 0))
								throw new PreconditionError("Déplacement gauche impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois - 1, preYVillageois);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, -1, 0);
						}
						else {
							if(!estAtteignable(numeroVillageois, angle, -1,  0) || !estAtteignable(numeroVillageois, angle, -2,  0))
								throw new PreconditionError("Déplacement gauche impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois - 2, preYVillageois);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, -2, 0);
						}
						
					}
					else {
						if(!estAtteignable(numeroVillageois, angle, -1, 0))
							throw new PreconditionError("Déplacement gauche impossible, la case n'est pas libre");
						
						terrain.getObjectAt(preXVillageois - 1, preYVillageois);
						
						super.pasJeu(commande, numeroVillageois, angle);
						
						checkInvariants();
						
						checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, -1, 0);
					}
					
				}
				// déplacement diagonale bas gauche
				else if(angle == 225) {
					if(!((preYVillageois + hauteurVillageois < hauteurTerrain) && (preXVillageois > 0)))
						throw new PreconditionError("Déplacement diagonale bas gauche impossible, le Villageois va sortir du Terrain");
					
					if(estSurRoute(numeroVillageois)) {
						if(preXVillageois - 2 < 0 || preYVillageois + 2 > hauteurTerrain) {
							if(!estAtteignable(numeroVillageois, angle, -1, 1))
								throw new PreconditionError("Déplacement diagonale bas gauche impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois - 1, preYVillageois + 1);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, -1, 1);
						}
						else {
							if(!estAtteignable(numeroVillageois, angle, -1,  1) || !estAtteignable(numeroVillageois, angle, -2,  2))
								throw new PreconditionError("Déplacement diagonale bas gauche impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois - 2, preYVillageois + 2);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, -2, 2);
						}
						
					}
					else {
						if(!estAtteignable(numeroVillageois, angle, -1, 1))
							throw new PreconditionError("Déplacement diagonale bas gauche impossible, la case n'est pas libre");
						
						terrain.getObjectAt(preXVillageois - 1, preYVillageois + 1);
						
						super.pasJeu(commande, numeroVillageois, angle);
						
						checkInvariants();
						
						checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, -1, 1);
					}
				}
				// déplacement bas
				else if(225 < angle && angle < 315) {
					if(preYVillageois + hauteurVillageois < hauteurTerrain)
						throw new PreconditionError("Déplacement bas impossible, le Villageois va sortir du Terrain");
					
					if(estSurRoute(numeroVillageois)) {
						if(preYVillageois + 2 > hauteurTerrain) {
							if(!estAtteignable(numeroVillageois, angle, 0, 1))
								throw new PreconditionError("Déplacement bas impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois, preYVillageois + 1);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 0, 1);
						}
						else {
							if(!estAtteignable(numeroVillageois, angle, 0,  1) || !estAtteignable(numeroVillageois, angle, 0,  2))
								throw new PreconditionError("Déplacement bas impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois, preYVillageois + 2);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 0, 2);
						}
						
					}
					else {
						if(!estAtteignable(numeroVillageois, angle, 0, 1))
							throw new PreconditionError("Déplacement bas impossible, la case n'est pas libre");
						
						terrain.getObjectAt(preXVillageois, preYVillageois + 1);
						
						super.pasJeu(commande, numeroVillageois, angle);
						
						checkInvariants();
						
						checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 0, 1);
					}
				}
				// déplacement diagonale bas droite
				else if(angle == 315) {
					if(!((preYVillageois + hauteurVillageois < hauteurTerrain) && (preXVillageois + largeurVillageois < largeurTerrain)))
						throw new PreconditionError("Déplacement diagonale bas droite impossible, le Villageois va sortir du Terrain");
					
					if(estSurRoute(numeroVillageois)) {
						if(preYVillageois + 2 > hauteurTerrain || preXVillageois + 2 < largeurTerrain) {
							if(!estAtteignable(numeroVillageois, angle, 1, 1))
								throw new PreconditionError("Déplacement diagonale bas droite impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois + 1, preYVillageois + 1);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 1, 1);
						}
						else {
							if(!estAtteignable(numeroVillageois, angle, 1,  1) || !estAtteignable(numeroVillageois, angle, 2,  2))
								throw new PreconditionError("Déplacement diagonale bas droite impossible, la case n'est pas libre");
							
							terrain.getObjectAt(preXVillageois + 2, preYVillageois + 2);
							
							super.pasJeu(commande, numeroVillageois, angle);
							
							checkInvariants();
							
							checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 2, 2);
						}
						
					}
					else {
						if(!estAtteignable(numeroVillageois, angle, 1, 1))
							throw new PreconditionError("Déplacement diagonale bas droite impossible, la case n'est pas libre");
						
						terrain.getObjectAt(preXVillageois + 1, preYVillageois + 1);
						
						super.pasJeu(commande, numeroVillageois, angle);
						
						checkInvariants();
						
						checkPostPositions(numeroVillageois, preXVillageois, preYVillageois, 1, 1);
					}
					
				}
				checkIfPositionsAreSameExcept(prePositions, numeroVillageois);
				checkIfObjectsAreSameExcept(preMatrice, numeroVillageois, preXVillageois, preYVillageois);
				checkIfLeavedCasesHaveChange(preMatrice, numeroVillageois, preXVillageois, preYVillageois);
				
				break;
				
			case ENTRERHOTELVILLE:
				if(!super.peutEntrerHotelVille(numeroVillageois))
					throw new PreconditionError("Le Villageois n°" + numeroVillageois + " ne peut pas entrer dans l'Hotel de Ville");
				
				int preQuantityDorHV = super.hotelDeVille().quantityDor();
				int preQuantityDorV = super.getVillageois(numeroVillageois).quantiteOr();
				
				super.pasJeu(commande, numeroVillageois, argument);
				
				checkInvariants();
				
				if(super.hotelDeVille().quantityDor() != preQuantityDorHV + preQuantityDorV)
					throw new PostconditionError("Le Villageois n°" + numeroVillageois + " n'a pas correctement vidé ses poches");
				
				if(super.getVillageois(numeroVillageois).quantiteOr() != 0)
					throw new PostconditionError("Le Villageois n°" + numeroVillageois + " n'a pas correctement vidé ses poches");
				
				checkPasJeu(prePasjeuCourant);
				checkIfObjectsAreSame(preMatrice);
				checkIfPositionsAreSame(prePositions);
				
				break;
				
			case ENTRERMINE:
				int numeroMine = argument;
				
				if(!super.numerosMines().contains(numeroMine))
					throw new PreconditionError("Le numero de Mine est invalide");
				
				if(!super.peutEntrerMine(numeroVillageois, numeroMine))
					throw new PreconditionError("Le Villageois n°" + numeroVillageois + " ne peut pas entrer dans la Mine n°" + numeroMine);
				
				if(super.getMine(numeroMine).estLaminee())
					throw new PreconditionError("La Mine est laminée");
				
				if(super.getMine(numeroMine).appartientA() != super.getVillageois(numeroVillageois).race())
					throw new PreconditionError("La mine n'appartient pas à la race du Villageois");
				
				int preorRestant = super.getMine(numeroMine).orRestant();
				
				super.pasJeu(commande, numeroVillageois, numeroMine);
				
				checkInvariants();
				
				if(super.getMine(numeroMine).orRestant() != preorRestant - 1)
					throw new PostconditionError("L'unité d'or n'a pas été correctement retirée de la Mine n°" + numeroMine);
				
				checkPasJeu(prePasjeuCourant);
				checkIfObjectsAreSame(preMatrice);
				checkIfPositionsAreSame(prePositions);
				checkIfCasesHaveChanged(preMatrice, numeroVillageois);
				
				break;
				
			case RIEN:
				super.pasJeu(commande, numeroVillageois, argument);
				
				checkInvariants();
				
				checkPasJeu(prePasjeuCourant);
				checkIfObjectsAreSame(preMatrice);
				checkIfPositionsAreSame(prePositions);
				break;
				
			default:
				// on ne devrait jamais arriver ici
				break;
			
		}
	}
	
	// Members

	@Override
	public void bind(ITerrainJeuxService terrain) {
		checkInvariants();
		
		super.bind(terrain);
		
		checkInvariants();
	}

	@Override
	public void bind(IVillageoisService villageois) {
		checkInvariants();
		
		super.bind(villageois);
		
		checkInvariants();
	}
	
	private void checkInvariants() throws InvariantError {
		int pasJeuCourant = super.pasJeuCourant();
		
		if(!((0 <= pasJeuCourant) && (pasJeuCourant <= super.maxPasJeu())))
			throw new InvariantError("pasJeuCourant = \"" + pasJeuCourant +"\", doit être 0 <= pasJeuCourant() <= maxPasJeu()");
	}
	
	private void checkNumeroVillageois(int numeroVillageois) throws PreconditionError {
		if(!super.numerosVillageois().contains(numeroVillageois))
			throw new PreconditionError("\"numerosVillageois()\" ne contient pas " + numeroVillageois);
	}
	
	private void checkNumeroMine(int numeroMine) throws PreconditionError {
		if(!super.numerosMines().contains(numeroMine))
			throw new PreconditionError("\"numerosMines()\" ne contient pas " + numeroMine);
	}
	
	private void checkNumeroMuraille(int numeroMuraille) throws PreconditionError {
		if(!super.numerosMurailles().contains(numeroMuraille))
			throw new PreconditionError("\"numerosMuraille()\" ne contient pas " + numeroMuraille);
	}
	
	private void checkPasJeu(int prePasjeuCourant) throws PostconditionError {
		if(super.pasJeuCourant() != prePasjeuCourant + 1)
			throw new PostconditionError("Le pasJeu n'a pas été correctement augmenté");
	}
	
	private void checkPostPositions(int numeroVillageois, int preXVillageois, int preYVillageois, int xMove, int yMove) throws PostconditionError {
		int postX = super.positionVillageoisX(numeroVillageois);
		int postY = super.positionVillageoisY(numeroVillageois);
		
		if(preXVillageois + xMove != postX)
			throw new PostconditionError("La coordonnée X du Villageois n°" + numeroVillageois + " n'a pas été correctement mise à jour");
		
		if(preYVillageois + yMove != postY)
			throw new PostconditionError("La coordonnée Y du Villageois n°" + numeroVillageois + " n'a pas été correctement mise à jour");
	}
	
	private void checkIfPositionsAreSame(Map<Integer, Integer[]> prePositions) throws PostconditionError {
		for(int numeroVillageois : super.numerosVillageois()) {
			if(positionVillageoisX(numeroVillageois) != prePositions.get(numeroVillageois)[0])
				throw new PostconditionError("La position X du Villageois n°" + numeroVillageois + " a changer, elle est passée de " + prePositions.get(numeroVillageois)[0] + " à " + positionVillageoisX(numeroVillageois));
			
			if(positionVillageoisY(numeroVillageois) != prePositions.get(numeroVillageois)[1])
				throw new PostconditionError("La position Y du Villageois n°" + numeroVillageois + " a changer, elle est passée de " + prePositions.get(numeroVillageois)[1] + " à " + positionVillageoisY(numeroVillageois));
		}
	}
	
	private void checkIfPositionsAreSameExcept(Map<Integer, Integer[]> prePositions, int exception) throws PostconditionError {
		for(int numeroVillageois : super.numerosVillageois()) {
			if(numeroVillageois != exception) {
				if(positionVillageoisX(numeroVillageois) != prePositions.get(numeroVillageois)[0])
					throw new PostconditionError("La position X du Villageois n°" + numeroVillageois + " a changer, elle est passée de " + prePositions.get(numeroVillageois)[0] + " à " + positionVillageoisX(numeroVillageois));
				
				if(positionVillageoisY(numeroVillageois) != prePositions.get(numeroVillageois)[1])
					throw new PostconditionError("La position Y du Villageois n°" + numeroVillageois + " a changer, elle est passée de " + prePositions.get(numeroVillageois)[1] + " à " + positionVillageoisY(numeroVillageois));
			}
		}
	}
	
	private void checkIfObjectsAreSame(OBJECTS[][] preMatrice) throws PostconditionError {
		for(int i = 0; i < terrain().largeur(); i++) {
			for(int j = 0; j < terrain().hauteur(); j++) {
				if(terrain().getObjectAt(i, j) != preMatrice[i][j])
					throw new PostconditionError("La case [" + i + "][" + j + "] est différente");
			}
		}
	}
	
	/**
	 * check si les cases du terrain sont inchangées, sauf celles correspondants aux zones de départ et d'arrivée du Villageois
	 * 
	 * @param preMatrice
	 * @param numeroVillageois
	 * @param preX
	 * @param preY
	 * @param postX
	 * @param postY
	 */
	private void checkIfObjectsAreSameExcept(OBJECTS[][] preMatrice, int numeroVillageois, int preX, int preY) throws PostconditionError {
		int largeurVillageois = super.getVillageois(numeroVillageois).largeur();
		int hauteurVillageois = super.getVillageois(numeroVillageois).hauteur();
		int preXBound = preX + largeurVillageois;
		int preYBound = preY + hauteurVillageois;
		int postX = super.positionVillageoisX(numeroVillageois);
		int postY = super.positionVillageoisY(numeroVillageois);
		int postXBound = postX + largeurVillageois;
		int postYBound = postY + hauteurVillageois;
		
		for(int i = 0; i < super.largeurTerrain(); i++) {
			if(!((preX <= i)&&(i <= preXBound)) && !((postX <= i)&&(i <= postXBound))) {
				for(int j = 0; j < super.hauteurTerrain(); j++) {
					if(!((preY <= j)&&(j <= preYBound)) && !((postY <= j)&&(j <= postYBound))) {
						if(preMatrice[i][j] != super.terrain().getObjectAt(i, j)) {
							throw new PostconditionError("La case [" + i + "][" + j + "] est différente");
						}
					}
				}
			}
		}
	}
	
	/**
	 * Les cases couvertes par le Villageois au départ et qui ne sont pas couvertes par le Villageois à l'arrivée qui vallaient VILLAGEOISSURROUTE vallent ROUTE, sinon elle vallent RIEN
	 * 
	 * @param preMatrice
	 * @param numeroVillageois
	 * @param preX
	 * @param preY
	 * @param postX
	 * @param postY
	 */
	private void checkIfLeavedCasesHaveChange(OBJECTS[][] preMatrice, int numeroVillageois, int preX, int preY) throws PostconditionError {
		int largeurVillageois = super.getVillageois(numeroVillageois).largeur();
		int hauteurVillageois = super.getVillageois(numeroVillageois).hauteur();
		int preXBound = preX + largeurVillageois;
		int preYBound = preY + hauteurVillageois;
		int postX = super.positionVillageoisX(numeroVillageois);
		int postY = super.positionVillageoisY(numeroVillageois);
		int postXBound = postX + largeurVillageois;
		int postYBound = postY + hauteurVillageois;
		
		for(int i = preX; i < preXBound; i++) {
			if(!((postX <= i)&&(i <= postXBound))) {
				for(int j = preY; j < preYBound; j++) {
					if(!((postY <= j)&&(j <= postYBound))) {
						OBJECTS preObject = preMatrice[i][j];
						OBJECTS postObject = super.terrain().getObjectAt(i, j);
						
						if(preObject == OBJECTS.VILLAGEOISSURROUTE && postObject != OBJECTS.ROUTE) {
							throw new PostconditionError("La case [" + i + "][" + j + "] n'a pas été correctement mise à jour");
						}
						else if(postObject != OBJECTS.RIEN)
							throw new PostconditionError("La case [" + i + "][" + j + "] n'a pas été correctement mise à jour");
					}
				}
			}
		}
	}
	
	/**
	 * check si les cases couvertes par le Villageois à l'arrivée avaient pour valeur ROUTE, elles deviennent des VILLAGEOISSURROUTE, sinon, elles sont devenues des VILLAGEOIS
	 * 
	 * @param preMatrice
	 * @param numeroVillageois
	 */
	private void checkIfCasesHaveChanged(OBJECTS[][] preMatrice, int numeroVillageois) throws PostconditionError {
		IVillageoisService villageois = super.getVillageois(numeroVillageois);
		int postX = villageois.getX();
		int postY = villageois.getY();
		
		for(int i = postX; i < postX + villageois.largeur(); i++) {
			for(int j = postY; j < postY + villageois.hauteur(); j++) {
				OBJECTS preObject = preMatrice[i][j];
				OBJECTS postObject = super.terrain().getObjectAt(i, j);
				
				if(preObject == OBJECTS.ROUTE && postObject != OBJECTS.VILLAGEOISSURROUTE)
					throw new PostconditionError("La case [" + i + "][" + j + "] n'a pas été correctement mise à jour");
				
				else if(postObject != OBJECTS.VILLAGEOIS)
					throw new PostconditionError("La case [" + i + "][" + j + "] n'a pas été correctement mise à jour");
			}
		}
	}
	
	/**
	 * Renvoit true si le Villageois associé au numéro passé en paramètre est sur une Route et sinon false.
	 * 
	 * @param numVillageois
	 * @return
	 */
	private boolean estSurRoute(int numeroVillageois) {
		int positionX = super.positionVillageoisX(numeroVillageois);
		int positionY = super.positionVillageoisY(numeroVillageois);
		for(int i = positionX; i < positionX + super.getVillageois(numeroVillageois).largeur(); i++) {
			for(int j = positionY; j < positionY + super.getVillageois(numeroVillageois).hauteur(); j++) {
				if(super.terrain().getObjectAt(i, j) == OBJECTS.VILLAGEOISSURROUTE)
					return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Renvoit true si la case située à la position (positionVillageoisX(numeroVillageois) + xOption, positionVillageoisY(numeroVillageois)) est atteignable.<br>
	 * Càd si il s'agit d'une ROUTE ou de RIEN.
	 * 
	 * @param numeroVillageois
	 * @param xOption
	 * @param yOption
	 * @return
	 */
	private boolean estAtteignable(int numeroVillageois, int angle, int xOption, int yOption) {
		int currentX = positionVillageoisX(numeroVillageois);
		int currentY = positionVillageoisY(numeroVillageois);
		int nextX = positionVillageoisX(numeroVillageois) + xOption;
		int nextY = positionVillageoisY(numeroVillageois) + yOption;
		int largeur = getVillageois(numeroVillageois).largeur();
		int hauteur = getVillageois(numeroVillageois).hauteur();

		// déplacement droite
		if((0 <= angle && angle < 45) || (315 < angle && angle <= 360)) {
			for(int i = currentY; i < currentY + hauteur; i ++) {
				OBJECTS object = terrain().getObjectAt(nextX + largeur, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement diagonale haut droite
		else if(angle == 45) {
			for(int i = nextX; i < nextX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}

			for(int i = nextY; i < nextY + hauteur; i ++) {
				OBJECTS object = terrain().getObjectAt(nextX + largeur, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement haut
		else if(45 < angle && angle < 135) {
			for(int i = currentX; i < currentX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement diagonale haut gauche
		else if(angle == 135) {
			for(int i = nextX; i < nextX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}

			for(int i = nextY; i < nextY + hauteur; i++) {
				OBJECTS object = terrain().getObjectAt(nextX, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement gauche
		else if(135 < angle && angle < 225) {
			for(int i = currentY; i < currentY + hauteur; i++) {
				OBJECTS object = terrain().getObjectAt(nextX, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement diagonale bas gauche
		else if(angle == 225) {
			for(int i = nextY; i < nextY + hauteur; i++) {
				OBJECTS object = terrain().getObjectAt(nextX, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}

			for(int i = nextX; i < nextX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY + hauteur);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement bas
		else if(225 < angle && angle < 315) {
			for(int i = currentX; i < currentX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}
		// déplacement diagonale bas droite
		else if(angle == 315) {
			for(int i = nextX; i < nextX + largeur; i++) {
				OBJECTS object = terrain().getObjectAt(i, nextY + hauteur);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}

			for(int i = nextY; i < nextY + hauteur; i++) {
				OBJECTS object = terrain().getObjectAt(nextX + largeur, i);
				if(object != OBJECTS.RIEN && object != OBJECTS.ROUTE)
					return false;
			}
		}

		return true;
	}
}
