package contracts;

import java.awt.Point;

import decorators.HotelVilleDecorator;
import enums.RACE;
import errors.InvariantError;
import errors.PostconditionError;
import errors.PreconditionError;
import services.IHotelVilleService;

public class HotelVilleContract extends HotelVilleDecorator implements
		IHotelVilleService {

	public HotelVilleContract(IHotelVilleService delegate) {
		super(delegate);
	}

	public void checkInvariant() {
		if (estLaminee() != (quantityDor() <= 0)) {
			throw new InvariantError(
					"estLaminee(H) =(min) orQuantity(H) ≤ 0 incorrecte");
		}
	}

	@Override
	public int largeur() {
		return super.largeur();
	}

	@Override
	public int hauteur() {
		return super.hauteur();
	}

	@Override
	public int quantityDor() {
		return super.quantityDor();
	}

	@Override
	public boolean estAbandonnee() {
		return super.estAbandonnee();
	}

	@Override
	public boolean estLaminee() {
		return super.estLaminee();
	}

	@Override
	public int abandonCompteur() {
		return super.abandonCompteur();
	}

	@Override
	public void init(int largeur, int hauteur, Point position, RACE race) {
		// Pre
		int x = position.x;
		int y = position.y;
		
		if(race==null) {
			throw new PreconditionError(" pre init(largeur,hauteur,pos,race) requires race <> null"); 	
		}
		
		if(x <=0 || y <=0) {
			throw new PreconditionError(" pre init(largeur,hauteur,pos,race) requires pos.x >0 ^ pos.y >0");
		}
		
		if ((largeur % 2 != 1) || (hauteur % 2 != 1)) {
			throw new PreconditionError(
					"pre init(largeur,hauteur,pos,race) require largeur%2=1 ∧ hauteur%2=1 incorrecte");
		}

		super.init(largeur, hauteur, position,race);

		checkInvariant();

		// post
		
		if(appartientA()!=race) {
			throw new PostconditionError("appartientA(init(largeur,hauteur,pos,race))= race incorrect");
		}
		
		if (largeur != largeur()) {
			throw new PostconditionError("largeur(init(largeur,hauteur,pos,race))=l incorrecte");
		}

		if (hauteur != hauteur()) {
			throw new PostconditionError("hauteur(init(largeur,hauteur,pos,race))=h incorrecte");
		}

		if (quantityDor() != 16) {
			throw new PostconditionError(
					"quantityDor()(init(largeur,hauteur,pos,race))=16 incorrecte");
		}

		if(position.x != getX()) {
			throw new PostconditionError("post getX(init(largeur,hauteur,pos.race)) = pos.x incorect");
		}
		
		if(position.y != getY()) {
			throw new PostconditionError("post getY(init(largeur,hauteur,pos,race)) = pos.y incorect");
		}
	}

	@Override
	public void depot(int s) {
		// pre

		if (s <= 0) {
			throw new PreconditionError("pre depot(H,s) require s>0");
		}

		checkInvariant();

		// Capture

		int x_pre = getX();
		int y_pre = getY();
		
		int orRestant_pre = quantityDor();
		int abandonComp_pre = abandonCompteur();

		super.depot(s);

		checkInvariant();

		// post

		if ((orRestant_pre + s) != quantityDor()) {
			throw new PostconditionError(
					"quantityDor(depot(H,s))=quantityDor(H)+s");
		}

		if (abandonComp_pre != abandonCompteur()) {
			throw new PostconditionError(
					"abandonCompteur(depot(H,s))=abandonCompteur(H)");
		}
		
		if(x_pre != getX()) {
			throw new PostconditionError("getX(depot(H,s)) = getX(H) incorrect");
		}
		
		if(y_pre!=getY()) {
			throw new PostconditionError("getY(depot(H,s)) = getY(H) incorrect");
		}
	}
	
	@Override
	public void setX(int x) {
		if (x <= 0) {
			throw new PreconditionError("pre setX(H, x) require x > 0");
		}

		checkInvariant();

		// Capture

		int y_pre = getY();
		
		int orRestant_pre = quantityDor();
		int abandonComp_pre = abandonCompteur();


		
		super.setX(x);

		checkInvariant();

		// post

		if (orRestant_pre != quantityDor()) {
			throw new PostconditionError(
					"quantityDor(setX(H,x))=quantityDor(H) incorrect");
		}

		if (abandonComp_pre != abandonCompteur()) {
			throw new PostconditionError(
					"abandonCompteur(setX(H,x))=abandonCompteur(H)");
		}
		if(y_pre!=getY()) {
			throw new PostconditionError("getY(setX(H,x)) = getY(H) incorrect");
		}
		
		if (super.getX() != x) {
			throw new PostconditionError(
					"post getX(setX(H, x) = x incorrect");
		}
		
		
	}

	@Override
	public void setY(int y) {
		if (y <= 0) {
			throw new PreconditionError("pre setY(H, y) require y > 0");
		}

		checkInvariant();

		// Capture

		int x_pre = getX();
		
		int orRestant_pre = quantityDor();
		int abandonComp_pre = abandonCompteur();


		super.setY(y);

		checkInvariant();

		// post

		if (super.getY() != y) {
			throw new PostconditionError(
					"post getY(setY(H, y) = y incorrect");
		}
		if(x_pre!=getX()) {
			throw new PostconditionError("getX(setY(H,x)) = getX(H) incorrect");
		}
		
		if (orRestant_pre != quantityDor()) {
			throw new PostconditionError(
					"quantityDor(setY(H,y))=quantityDor(H) incorrect");
		}

		if (abandonComp_pre != abandonCompteur()) {
			throw new PostconditionError(
					"abandonCompteur(setY(H,y))=abandonCompteur(H)");
		}

	}
}
